/*
 * lbSprite.h
 *
 *  Created on: 10-10-2012
 *      Author: Spliter
 */

#pragma once
#include <vector>
#include <math/util/lbRect.h>

#define CENTER_MIDDLE 0x7fffffff
#define CENTER_LEFT 0x7ffffffe
#define CENTER_RIGHT 0x7ffffffd
#define CENTER_TOP CENTER_LEFT
#define CENTER_BOTTOM CENTER_RIGHT

class lbTexture;
class lbTextureResourceManager;

class lbSprite
{
public:
	//note: the texture for the sprite will be loaded automatically (if it's not yet loaded)
	static lbSprite* loadSprite(std::string fname, lbTextureResourceManager* textureManager);
	//note: we assume that the spacing is applied to the first row/column as well
	//if spacing or cellSize is -1 then it will be calculated as it's seen fit.
	//if nColumns=0 then we assume that each frame is in its own column
	lbSprite(lbTexture* texture, int nFrames, int nColumns=0, int centerX=CENTER_MIDDLE, int centerY=CENTER_MIDDLE, int startXOffset=0, int startYOffset=0, int hSpacing=-1, int vSpacing=-1, int frameWidth=-1, int frameHeight=-1);
	virtual ~lbSprite();

	lbTexture* getBoundTexture();
	int getFrameNum();
	lbRectI getFrameRect(int frame);
	lbRectI getSpriteBbox(){return _bbox;}

	int getFrameWidth(){return _bbox.x2-_bbox.x1;}
	int getFrameHeight(){return _bbox.y2-_bbox.y1;}

	void renderFrame(int frame, int x, int y, float xScale=1.0f, float yScale=1.0f);
	void renderFrame(int frame, lbRectF rect);
private:
	std::vector<lbRectI> _frames;
	lbTexture* _boundTexture;
	lbRectI _bbox;
};
