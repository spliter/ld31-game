/*
 * GameLevel.cpp
 *
 *  Created on: 3 gru 2014
 *      Author: Miko Kuta
 */

#include <game/GameEntity.h>
#include <game/GameEntityFactoryManager.h>
#include <game/level/GameLevel.h>
#include <game/level/levelUtils.h>
#include <global.h>
#include <math/util/lbRect.h>
#include <utils/debug/debug.h>
#include <utils/entities/lbEntityManager.h>
#include <utils/file/TinyXml/tinyxml.h>
#include <utils/resources/lbDefaultResourceManagers.h>
#include <utils/resources/lbResourceManager.h>
#include <utils/resources/lbTextureResourceManager.h>
#include <iostream>

//#define LogMessage( message )

GameLevel::GameLevel(GameContext context) :
		entManager(2048)
{
	LogEntryPoint();
	gameContext = context;
	entManager.reset();
}

GameLevel::~GameLevel()
{
	LogEntryPoint();
	entManager.deleteAllEntities();
}

void GameLevel::onStart()
{
	LogEntryPoint();
	backgroundCamera.setViewport(lbRectI(0, 0, getScreenWidth(), getScreenHeight()), -1, 1);
	characterCamera.setViewport(lbRectI(0, 0, getScreenWidth(), getScreenHeight()), -1, 1);
	foregroundCamera.setViewport(lbRectI(0, 0, getScreenWidth(), getScreenHeight()), -1, 1);

	GameEntity* ent = entManager.getHeadEntity();
	while (ent)
	{
		ent->onSpawned();
		ent = ent->getNext();
	}
}

void GameLevel::onUpdate(float frameDif)
{
	LogEntryPoint();
	GameEntity* curEnt = entManager.getHeadEntity();
	while (curEnt)
	{
		curEnt->onUpdate(frameDif);
		curEnt = curEnt->getNext();
	}
	entManager.deleteKilledEntities();
}

void GameLevel::onFrameRender()
{
	LogEntryPoint();
	backgroundCamera.pushCamera();
	backgroundLayer.draw(&backgroundCamera);
	backgroundCamera.popCamera();

	characterCamera.pushCamera();
	characterLayer.draw(&characterCamera);
	characterCamera.popCamera();

	foregroundCamera.pushCamera();
	foregroundLayer.draw(&foregroundCamera);
	foregroundCamera.popCamera();
}

void GameLevel::onEnd()
{
	LogEntryPoint();
	entManager.deleteAllEntities();
}

GameEntity* GameLevel::createEntity(const std::string& entName)
{
	LogEntryPoint();
	if (entManager.hasFreeIds())
	{
		GameEntityFactory* factory = GameEntityFactories::findFactory(entName);
		if (factory)
		{
			GameEntity* entity = factory->createEntity(&gameContext);
			if (entity)
			{
				entManager.addEntity(entity);
				return entity;
			}
		}
		else
		{
			LogMessage("Failed to create entity of type: " << entName);
		}
	}
	return NULL;
}

////////////////////////map load Helper Functions

//

GameLevel* GameLevel::loadLevel(GameContext context, std::string levelName)
{
	LogEntryPoint();
	GameLevel* level = new GameLevel(context);

	TilesetList tileSets;

	std::string fname = buildLevelFilename(levelName);
	std::string logicFname = "";

	TiXmlDocument doc(fname);
	if (doc.LoadFile())
	{
		TiXmlElement* mapElement = doc.FirstChildElement("map");
		if (mapElement)
		{
			std::string levelDir = retrieveDirectory(fname);
			TiXmlElement* tileSetElement = mapElement->FirstChildElement("tileset");
			while (tileSetElement)
			{
				tileSets.set[tileSets.setNumber] = getTileSetFromXmlElement(tileSetElement, levelDir);

				std::string name = "error:NO NAME";
				tileSetElement->QueryStringAttribute("name", &name);
				tileSets.set[tileSets.setNumber].texture = lbDefaultResourceManagers::instance().getTextureManager()->getResource(tileSets.set[tileSets.setNumber].textureFname.c_str());

				tileSetElement = tileSetElement->NextSiblingElement("tileset");
				if (tileSets.set[tileSets.setNumber].firstId >= 0)
				{
					tileSets.setNumber++;
					if(tileSets.setNumber>MAX_TILE_SET_NUM)
					{
						std::cerr << "ERROR: Attempting to load too many Tile Sets, current max is "<<MAX_TILE_SET_NUM<<"\n";
						tileSets.setNumber = MAX_TILE_SET_NUM;
						break;
					}
				}
			}

			TiXmlElement* layerElement = mapElement->FirstChildElement("layer");
			while (layerElement)
			{
				std::string name;
				int result;
				result = layerElement->QueryStringAttribute("name", &name);
				if (result == TIXML_SUCCESS)
				{
					if (name.substr(0, std::string("Background").size()) == "Background")
					{
						lbSceneTileMap* tileMap = createTileMapFromXmlElement(layerElement, tileSets);
						if (tileMap)
						{
							level->tileMaps.push_back(tileMap);
							level->backgroundLayer.addNode(tileMap);
						}
					}
					else if (name.substr(0, std::string("Foreground").size()) == "Foreground")
					{
						lbSceneTileMap* tileMap = createTileMapFromXmlElement(layerElement, tileSets);
						if (tileMap)
						{
							level->tileMaps.push_back(tileMap);
							level->foregroundLayer.addNode(tileMap);
						}
					}
					else if (name == "Collision")
					{
						Map* collMap =  createCollisionMapFromXmlElement(layerElement, tileSets);
						if(collMap)
						{
							level->collisionMaps.push_back(collMap);
						}
					}
				}

				layerElement = layerElement->NextSiblingElement("layer");
			}

			TiXmlElement* objectGroupElement = mapElement->FirstChildElement("objectgroup");
			while (objectGroupElement)
			{
				TiXmlElement* objectElement = objectGroupElement->FirstChildElement("object");
				while (objectElement)
				{
					std::string type;
					int result = objectElement->QueryStringAttribute("type", &type);
					if (result == TIXML_SUCCESS)
					{
						GameEntity* entity = level->createEntity(type);
						if (entity)
						{
							entity->onSetupFromXmlElement(objectElement);
						}
					}
					objectElement = objectElement->NextSiblingElement("object");
				}
				objectGroupElement = objectGroupElement->NextSiblingElement("objectgroup");
			}
		}
	}
	else
	{
		LogMessage("Failed to find file "<<fname);
	}
	doc.Clear();

	if(level->entManager.getHeadEntity()==NULL)
	{
		level->createEntity("debug_camera");
	}
	return level;
}
