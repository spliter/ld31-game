/*
 * lbSpriteResourceManager.cpp
 *
 *  Created on: 25-05-2013
 *      Author: Spliter
 */

#include "lbSpriteResourceManager.h"
#include "../graphics/lbSprite.h"
#include <utils/graphics/lbTexture.h>
#include <utils/debug/debug.h>

lbSpriteResourceManager::lbSpriteResourceManager(lbTextureResourceManager* textureManager)
{
	LogEntryPoint();
	_defaultSprite = NULL;
	_defaultTexture = NULL;
	_textureManager = textureManager;
	createDefaultSprite();
}

lbSpriteResourceManager::~lbSpriteResourceManager()
{
	LogEntryPoint();
	delete _defaultSprite;
	delete _defaultTexture;
}

lbSprite* lbSpriteResourceManager::loadResourceFromFile(const std::string &fname)
{
	LogEntryPoint();
	return lbSprite::loadSprite(fname,_textureManager);
}

void lbSpriteResourceManager::createDefaultSprite()
{
	LogEntryPoint();
	int texWidth = 8;
	int texHeight = 8;
	int texBpp = 4;
	unsigned char data[texWidth * texHeight * texBpp];
	for(int x = 0; x < texWidth; x++)
		for(int y = 0; y < texHeight; y++)
		{
			int i = (y * texWidth + x);

			int* datai = (int*) data;
			if((y + x) % 2)
			{
				datai[i] = 0xffbf5680;
			}
			else
			{
				datai[i] = 0xff000000;
			}
		}
	_defaultTexture = new lbTexture(texWidth, texHeight, data);

	_defaultSprite = new lbSprite(_defaultTexture, 1, 1);
}
