/*
 * lbSceneGridNode2D.cpp
 *
 *  Created on: 2 mar 2014
 *      Author: spliter
 */

#include <utils/graphics/scene/lbSceneGridNode2D.h>

lbSceneGridNode2D::lbSceneGridNode2D(int columns, int rows)
{
	setGrid(columns,rows);
}

lbSceneGridNode2D::~lbSceneGridNode2D()
{
}


void lbSceneGridNode2D::draw(lbCameraSceneNode2D* camera)
{
		lbTexture::unbind();
		glPushMatrix();
		glMultMatrixf(getGLWorldTransform().me);

		glBegin(GL_LINES);

		float xScale = 1.0f/_columns;
		for(int x=0;x<_columns;x++)
		{
			glColor3f(0.0f, 0.1f, 0.0f);
			glVertex2f(x*xScale, 0);
			glColor3f(0.0f, 1.0f, 0.0f);
			glVertex2f(x*xScale, 1);
		}

		float yScale = 1.0f/_rows;
		for(int y=0;y<_rows;y++)
		{
			glColor3f(0.1f, 0.0f, 0.0f);
			glVertex2f(0, y*yScale);
			glColor3f(1.0f, 0.0f, 0.0f);
			glVertex2f(1, y*yScale);
		}
		glEnd();
		glPopMatrix();
	}
