/*
 * GameEntity.h
 *
 *  Created on: 23-08-2013
 *      Author: Spliter
 */

#pragma once

#include <events/lbGameEvent.h>
#include <math/lbVec2.h>
#include <utils/entities/lbEntityManager.h>
#include <utils/graphics/scene/lbSceneNode2D.h>
#include <utils/Object.h>
#include <string>

struct lbRectF;

struct GameContext;
struct GameLevel;
struct Map;

class TiXmlElement;

class GameEntity: public lbSceneNode2D,public lbGameEventListener
{
	DEFINE_CLASS(GameEntity,lbSceneNode2D);

public:
	static bool parseStringProperty(TiXmlElement* element, std::string propertyName, std::string* retValue);
	static bool parseFloatProperty(TiXmlElement* element, std::string propertyName, float* retValue);
	static bool parseIntProperty(TiXmlElement* element, std::string propertyName, int* retValue);
	static bool parseBoolProperty(TiXmlElement* element, std::string propertyName, bool* retValue);
public:
	GameEntity(GameContext* gameContext);
	virtual ~GameEntity();

	virtual void onUpdate(float dt){}

	virtual void onUse(GameEntity* other){};

	std::string getName(){return _name;}
	void setName(std::string newName){_name = newName;}

	GameContext* getGameContext(){return _gameContext;}
	GameLevel* getLevel();

	void kill(){if(!_isKilled){_isKilled = true;callKill();}}

	GameEntity* getPrevious(){return _prev;}
	GameEntity* getNext(){return _next;}

	int getIndexId(){return _id;}
	int getUniqueId(){return _uniqueId;}


	static int castBlockHorizontal(float dx, lbRectF rect, Map* map, int flags,int neededFlags, float &movedDistance);
	static int castBlockVertical(float dy, lbRectF rect, Map* map, int flags,int neededFlags , float &movedDistance);
	static bool checkCollisionAgainstMap(lbRectF rect, Map* map, int flags,int neededFlags);
	static bool checkCollisionLineAgainstMap(lbVec2f src, lbVec2f dst, Map* map, int flags, int neededFlags);
protected:
	virtual void onCreated();//right after creating the instance and adding it to the level, before the onSetupFromXmlElement
	virtual void onSetupFromXmlElement(TiXmlElement* element);
	virtual void onSpawned();//when the level is starting (if created on level load) or right after onCreated if created after level start
	virtual void onKilled();//when the kill(); is called for the first time
	virtual void onDestroy();//right before being deleted

private:
	enum ParentFunc
	{
		Create,Setup,Spawn,Kill,Destroy,None
	};
	ParentFunc _parentFuncCalled;
	ParentFunc _parentFuncRequested;
	void callCreate();
	void callSetup(TiXmlElement* element);
	void callSpawn();
	void callKill();
	void callDestroy();

	friend class lbEntityManager<GameEntity>;
	friend class GameState;
	friend class GameLevel;
	int _id;
	int _uniqueId;
	bool _isKilled;
	GameEntity* _prev;
	GameEntity* _next;
	lbVec2f _pos;
	std::string _name;
	GameContext* _gameContext;
};

class GameEntityFactory
{
public:
	virtual ~GameEntityFactory(){};
	virtual GameEntity* createEntity(GameContext* context)=0;//creates a new entity but doesn't spawn it nor calls any initialization functions.
};


#define REGISTER_GAME_ENTITY(className,editorEntName)\
	class className##GameEntityFactory:public GameEntityFactory\
	{\
	public:\
		virtual GameEntity* createEntity(GameContext* context){return new className(context);}\
	};\
	static className##GameEntityFactory className##__FactoryInstance;\
	static bool className##__FactoryRegistered=GameEntityFactories::registerFactory(&className##__FactoryInstance,editorEntName);
