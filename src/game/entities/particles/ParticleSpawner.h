/*
 * DebugCameraEntity.h
 *
 *  Created on: 4 gru 2014
 *      Author: Miko Kuta
 */

#pragma once

#include <game/GameEntity.h>
#include <utils/Object.h>
#include <iostream>
#include <vector>

struct GameContext;
class lbSprite;

template<class T> class ParticleSpawner: public GameEntity
{
public:
DEFINE_CLASS(ParticleSpawner,GameEntity)
	;

	ParticleSpawner(GameContext* context);
	virtual ~ParticleSpawner();
	void setParticleCount(int particleCount)
	{
		_particles.resize(particleCount);
	}
protected:
	void resetParticles();
	int reserveParticle();
	void freeParticle(int id);
	std::vector<T> _particles;
	int _freeIdHead;
	int _aliveParticleCount;
};

template<class T> ParticleSpawner<T>::ParticleSpawner(GameContext* context) :
		GameEntity(context), _freeIdHead(0),_aliveParticleCount(0)
{
	resetParticles();
}

template<class T> ParticleSpawner<T>::~ParticleSpawner()
{
}

template<class T> int ParticleSpawner<T>::reserveParticle()
{
	if (_freeIdHead < _particles.size())
	{
		int usedId = _freeIdHead;
		_freeIdHead = _particles[_freeIdHead].nextFreeId;
		_particles[usedId].isAlive = true;
		_aliveParticleCount++;
		return usedId;
	}
	return -1;
}

template<class T> void ParticleSpawner<T>::resetParticles()
{
	_freeIdHead = 0;
	_aliveParticleCount = 0;
	for (int i = 0; i < (int) _particles.size(); i++)
	{
		_particles[i].isAlive = false;
		_particles[i].nextFreeId = i + 1;
	}
}

template<class T> void ParticleSpawner<T>::freeParticle(int id)
{
	if (id < (int) _particles.size() && _particles[id].isAlive)
	{
		_particles[id].isAlive = false;
		_particles[id].nextFreeId = _freeIdHead;
		_freeIdHead = id;
		_aliveParticleCount--;
	}
	else
	{
		std::cout << "Warning! Tried to free a particle either outside of range, or one that is already dead\n";
	}
}
