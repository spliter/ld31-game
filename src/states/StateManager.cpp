/*
 * StateManager.cpp
 *
 *  Created on: 13-09-2012
 *      Author: Spliter
 */

#include "StateManager.h"
#include "State.h"
#include <iostream>
using namespace std;

StateManager& StateManager::getInstance()
{
	static StateManager mgr;
	return mgr;
}

StateManager::StateManager()
{

}

StateManager::~StateManager()
{
	cleanUp();
}

void StateManager::update()
{
	bool changed = true;
	while(changed)
	{
		cleanFinishedStates();
		changed = performStackCleanup();
		if(changed)
		{
			std::cout<<"Changed!\n";
			std::cout.flush();
		}
	}

	if (!_currentStack.empty())
	{
		_currentStack.back()->callOnUpdate();
	}
}

void StateManager::startFrame()
{
	if (!_currentStack.empty())
	{
		_currentStack.back()->callOnFrameBegin();
	}
}

void StateManager::renderFrame()
{
	if (!_currentStack.empty())
	{
		_currentStack.back()->callOnFrameRender();
	}
}

void StateManager::endFrame()
{
	if (!_currentStack.empty())
	{
		_currentStack.back()->callOnFrameEnd();
	}
}

void StateManager::cleanUp()
{
	_desiredStack.clear();
	performStackCleanup();
}

void StateManager::onKeyboardEvent(lbKeyboardEvent *event)
{
	if (!_currentStack.empty())
	{
		_currentStack.back()->callOnKeyboardEvent(event);
	}
}

void StateManager::onMouseMotionEvent(lbMouseMotionEvent *event)
{
	if (!_currentStack.empty())
	{
		_currentStack.back()->callOnMouseMotionEvent(event);
	}
}

void StateManager::onMouseButtonEvent(lbMouseButtonEvent *event)
{
	if (!_currentStack.empty())
	{
		_currentStack.back()->callOnMouseButtonEvent(event);
	}
}

void StateManager::onMouseWheelEvent(lbMouseWheelEvent *event)
{
	if (!_currentStack.empty())
	{
		_currentStack.back()->callOnMouseWheelEvent(event);
	}
}

void StateManager::startState(State* state)
{
	state->_isFinished = false;
	_desiredStack.remove(state);
	_desiredStack.push_back(state);
}

void StateManager::cleanFinishedStates()
{
	StateIter iter = _currentStack.begin();

	while (iter != _currentStack.end())
	{
		if ((*iter)->_isFinished)
		{
			_desiredStack.remove((*iter));
		}
		iter++;
	}
}

bool StateManager::performStackCleanup()
{
	/*
	 * Cleaning up the stack is done in several phases
	 * 1-we remove any states from current stack that don't exist in the desired stack
	 * 2-if any of those is running we call on Pause on them.
	 * 3-we go through the removed states and call on End on them
	 * 4-we add in order all states from desired stack, calling onStart() on them if they haven't been started
	 * 5-we go through all states and check if they're running, if so, and they're not the last state, we pause them
	 * 6-we resume the top state if it's not already running
	 */

	bool changed = false;

	std::list<State*> tmpDesiredStack(_desiredStack);
	std::list<State*> tmpCurrentStack(_currentStack);
	std::list<State*> removedStates;

	StateIter iter = tmpCurrentStack.begin();
	while (iter != tmpCurrentStack.end())
	{
		if (!hasState(tmpDesiredStack, (*iter)))
		{
			changed = true;
			removedStates.push_front((*iter));
			iter = tmpCurrentStack.erase(iter);
		}
		else
		{
			iter++;
		}
	}

	iter = removedStates.begin();
	while (iter != removedStates.end())
	{
		State* state = (*iter);

		if (state->isResumed())
		{
			changed = true;
			state->callOnPause();
		}
		iter++;
	}

	iter = removedStates.begin();
	while (iter != removedStates.end())
	{
		State* state = (*iter);
		state->callOnEnd();
		iter++;
	}

	tmpCurrentStack=tmpDesiredStack;

	if(!tmpCurrentStack.empty())
	{
		State* top = tmpCurrentStack.back();
		iter = tmpCurrentStack.begin();
		while(iter!=tmpCurrentStack.end())
		{
			State* state = *iter;
			if(!state->isStarted())
			{
				changed = true;
				state->callOnStart();
			}
			if(state!=top)
			{
				if(state->isResumed())
				{
					changed = true;
					state->callOnPause();
				}
			}
			else
			{
				if(!state->isResumed())
				{
					changed = true;
					state->callOnResume();
				}
			}
			iter++;
		}
	}
	_currentStack = tmpCurrentStack;
	return changed;
}

StateManager::StateIter StateManager::findState(std::list<State*> stateList, State* state)
{
	StateIter iter = stateList.begin();
	while (iter != stateList.end())
	{
		if ((*iter) == state)
		{
			return iter;
		}
		iter++;
	}
	return stateList.end();
}

bool StateManager::hasState(std::list<State*> stateList, State* state)
{
	return findState(stateList, state) != stateList.end();
}

