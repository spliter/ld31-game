/*
 * lbResourceManager.h
 *
 *  Created on: 09-10-2012
 *      Author: Spliter
 */

#pragma once

#include <iostream>
#include <map>
#include <string>
#include <utility>

template <class T>
class lbResourceManager
{
public:
	lbResourceManager();
	virtual ~lbResourceManager();//Note: resource must be cleared manually.
	virtual bool hasResource(const std::string & fname);
	virtual T* getResource(const std::string & fname);
	virtual void clearResources();


	virtual T* getDefaultResource()=0;
	virtual T* loadResourceFromFile(const std::string & fname)=0;
	virtual void freeResource(T* resource)=0;
protected:
	std::map<std::string,T*> _resources;
};

template <class T>
lbResourceManager<T>::lbResourceManager()
{

}

template <class T>
lbResourceManager<T>::~lbResourceManager()
{
}

template <class T>
bool lbResourceManager<T>::hasResource(const std::string & fname)
{
	typename std::map<std::string,T*>::iterator iter = _resources.find(fname);
	return iter != _resources.end();
}

template <class T>
T* lbResourceManager<T>::getResource(const std::string & fname)
{
	typename std::map<std::string,T*>::iterator iter = _resources.find(fname);
	if(iter == _resources.end())
	{
		T* res = loadResourceFromFile(fname);
		if(!res)
		{
			res = getDefaultResource();
		}
		_resources.insert(std::pair<std::string, T*>(fname, res));
		return res;
		return NULL;
	}
	return iter->second;
}

template <class T>
void lbResourceManager<T>::clearResources()
{
	while(!_resources.empty())
	{
		typename std::map<std::string,T*>::iterator iter = _resources.begin();
		if(iter->second != getDefaultResource())
		{
			freeResource(iter->second);
		}
		_resources.erase(iter);
	}
}
