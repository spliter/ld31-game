/*
 * lbDrawableLayer.h
 *
 *  Created on: 17-10-2012
 *      Author: Spliter
 */

#pragma once
#include <math/util/lbRect.h>
#include <list>
class lbDrawable;

class lbDrawableLayer
{
public:
	lbDrawableLayer();
	virtual ~lbDrawableLayer();

	void addDrawable(lbDrawable* drawable);
	void removeDrawable(lbDrawable* drawable);
	void clearDrawables();

	void draw(lbRectF camera);//sorts all drawables, checks if they're visible on screen and draws them
private:
	std::list<lbDrawable*> _drawables;
};
