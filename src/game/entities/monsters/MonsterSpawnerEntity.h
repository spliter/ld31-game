/*
 * DebugCameraEntity.h
 *
 *  Created on: 4 gru 2014
 *      Author: Miko Kuta
 */

#pragma once

#include <game/GameEntity.h>
#include <utils/Object.h>

struct GameContext;

class MonsterSpawnerEntity: public GameEntity
{
public:
	DEFINE_CLASS(MonsterSpawnerEntity,GameEntity);

	MonsterSpawnerEntity(GameContext* context);
	virtual ~MonsterSpawnerEntity();
	virtual void onSetupFromXmlElement(TiXmlElement* element);
	virtual void onSpawned();
	virtual void onUpdate(float dt);

private:
	float _timeSinceLastMonster;
};

