/*
 * lbTileSet.h
 *
 *  Created on: 12 lut 2014
 *      Author: spliter
 */

#pragma once
class lbTexture;
class lbTextureResourceManager;

class lbTileSet
{
public:
	static lbTileSet* loadFromFile(const std::string &fname,lbTextureResourceManager* textureManager);

	lbTileSet(lbTexture* texture, int xOffset, int yOffset, int width, int height, int numRows, int numColumns);
	lbTileSet();
	~lbTileSet();

	lbTexture* getTexture(){return _texture;}
	lbTexture* getOriginalTexture(){return _texture;}

	int getNumColumns(){return _numColumns;}
	int getNumRows(){return _numRows;}

private:
	int _numColumns;
	int _numRows;
	lbTexture* _originalTexture;
	lbTexture* _texture;
};

