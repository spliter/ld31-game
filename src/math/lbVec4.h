/*
 * lbVec4f.h
 *
 *  Created on: 12-04-2011
 *      Author: Spliter
 */

#pragma once

#define LB_CLASS_NAME __internal_lbVec4f_t
#define LB_PARAM_TYPE float
#include "internal/lbVec4_internal" // IWYU pragma: export
typedef LB_CLASS_NAME lbVec4f;
#undef LB_CLASS_NAME
#undef LB_PARAM_TYPE

#define LB_CLASS_NAME __internal_lbVec4d_t
#define LB_PARAM_TYPE double
#include "internal/lbVec4_internal" // IWYU pragma: export
typedef LB_CLASS_NAME lbVec4d;
#undef LB_CLASS_NAME
#undef LB_PARAM_TYPE
