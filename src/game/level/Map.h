/*
 * Map.h
 *
 *  Created on: 12-09-2012
 *      Author: Spliter
 */

#pragma once

#include <game/Constants.h>
#include <lbTypes.h>
#include <utils/debug/debug.h>
#include <cstring>

struct Tile
{
	uint32 tileFlags;
};

struct Map
{
public:
	Tile* tiles;
	int width;
	int height;

	Map()
	{
		tiles = NULL;
		width = 0;
		height = 0;
	}

	Map(int width, int height)
	{
		this->width = width, this->height = height;
		tiles = new Tile[width*height];
	}

	void build(int width, int height)
	{
		LogEntryPoint();
		if(tiles!=NULL)
		{
			delete[] tiles;
		}
		this->width = width;
		this->height = height;
		tiles = new Tile[width*height];
	}

	~Map()
	{
		delete tiles;
	}

	inline Tile getTile(int idX, int idY) const
	{
		LogEntryPoint();
		if(idX<0||idX>=width||idY<0||idY>=height)
		{
			return Tile();
		}
		return tiles[idX+idY*width];
	}

	inline Tile* getTileRef(int idX, int idY) const
	{
		LogEntryPoint();
		if(idX<0||idX>=width||idY<0||idY>=height)
		{
			return NULL;
		}
		return &tiles[idX+idY*width];
	}

	inline Tile getTileAtPos(float x, float y) const
	{
		LogEntryPoint();
		return getTile((int) (x/TILE_SIZE), (int) (y/TILE_SIZE));
	}

	inline int getTileId(float x, float y) const
	{
		LogEntryPoint();
		int idX = (int) (x/TILE_SIZE);
		int idY = (int) (y/TILE_SIZE);
		return idX+idY*width;
	}
};
