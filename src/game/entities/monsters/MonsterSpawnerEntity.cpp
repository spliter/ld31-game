/*
 * MonsterSpawnerEntity.cpp
 *
 *  Created on: 4 gru 2014
 *      Author: Miko Kuta
 */

#include <game/entities/monsters/MonsterSpawnerEntity.h>
#include <game/GameContext.h>
#include <game/GameEntityFactoryManager.h>
#include <game/states/GameState.h>


REGISTER_GAME_ENTITY(MonsterSpawnerEntity,"monster_spawner");

static float monsterSpawnInterval = 1.0f;

MonsterSpawnerEntity::MonsterSpawnerEntity(GameContext* context):
	GameEntity(context),
	_timeSinceLastMonster(0.0f)
{

}


MonsterSpawnerEntity::~MonsterSpawnerEntity()
{
}

void MonsterSpawnerEntity::onSetupFromXmlElement(TiXmlElement* element)
{
	super::onSetupFromXmlElement(element);
}

void MonsterSpawnerEntity::onSpawned()
{
	super::onSpawned();
}

void MonsterSpawnerEntity::onUpdate(float dt)
{
	_timeSinceLastMonster+=dt;

	if(_timeSinceLastMonster>monsterSpawnInterval)
	{
		_timeSinceLastMonster = 0;
		GameEntity* monster = getGameContext()->gameState->spawnEntity("zombie_monster");
		if(monster)
		{
			monster->setPosition(getPosition());
		}
	}
}


