/*
 * debug.h
 *
 *  Created on: 09-10-2012
 *      Author: Spliter
 */

#pragma once

#include <iostream>
#include <list>
#include <string>
#include <stdio.h>

#define lbAssert(condition)\
		if(!(condition)){printf("condition \""#condition"\" failed on line %d in file %s\n",__LINE__,__FILE__);exit(1);}

#define lbAssertMsg(condition, message)\
		if(!(condition)){printf("condition \""#condition"\" failed on line %d in file %s with message: \"%s\"\n",__LINE__,__FILE__,message);exit(1);}

#define lbAssertMsgArgs(condition,message, args...)\
		if(!)condition)){printf("condition \""#condition"\" failed on line %d in file %s with message: "message,__LINE__,__FILE__,args);exit(1);}

#define LogMessage( message ) std::cout<<__FILE__<<"@"<<__LINE__<<" : "<<message<<std::endl

#define LOG_ENTRY_POINT
#define LOG_IMPORTING


#define STRINGIZE_DETAIL(x) #x
#define STRINGIZE(x) STRINGIZE_DETAIL(x)

#ifdef LOG_IMPORTING
	inline void LogEntryPoint(){}
#else
	#ifdef LOG_ENTRY_POINT
			#define LogEntryPoint() FuncEntryPoint __entryPoint##__LINE__(__FILE__,STRINGIZE(__LINE__),__PRETTY_FUNCTION__)
			class FuncEntryPoint
			{
			public:
				FuncEntryPoint(std::string file, std::string line, std::string function);
				~FuncEntryPoint();
				static void flushCurrentEntryPoint();
				static void flushStack();
			};
	#else
		#define  LogEntryPoint()
	#endif
#endif

