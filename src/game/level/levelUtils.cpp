/*
 * levelUtils.cpp
 *
 *  Created on: 3 gru 2014
 *      Author: Miko Kuta
 */

//#include <game/Constants.h>
#include <game/level/levelUtils.h>
#include <game/level/Map.h>
#include <utils/debug/debug.h>
#include <utils/file/TinyXml/tinyxml.h>
#include <utils/graphics/lbTileMap.h>
#include <utils/graphics/scene/lbSceneTileMap.h>
#include <cctype>
#include <cstring>
#include <iostream>


static bool equalsIgnoreCase(const char* a, const char* b)
{
	if (!a || !b)
		return false;

	int i = 0;
	while (a[i] && b[i])
	{
		if (tolower(a[i]) != tolower(b[i]))
			return false;
		i++;
	}
	if (!a[i] && !a[i])
	{
		return true;
	}
	return false;
}

int getTilesetUsed(TilesetList& tileSets, int sampleTileId)
{
	LogEntryPoint();
	int highestFirstId = -1;
	int usedId = -1;
	for (int i = 0; i<tileSets.setNumber; i++)
	{
		if (tileSets.set[i].firstId <= sampleTileId && tileSets.set[i].firstId > highestFirstId)
		{
			highestFirstId = tileSets.set[i].firstId;
			usedId = i;
		}
	}
	return usedId;
}

TileSet getTileSetFromXmlElement(TiXmlElement* element, std::string levelDir)
{
	LogEntryPoint();
	TileSet tileset;
	int tileWidth, tileHeight, firstId;
	int imageWidth, imageHeight;
	std::string texture;
	std::vector<int> flags;

	if (element->QueryIntAttribute("tilewidth", &tileWidth) != TIXML_SUCCESS)
	{
		tileWidth = 8;
		LogMessage("Failed to query tileWidth");
	}

	if (element->QueryIntAttribute("tileheight", &tileHeight) != TIXML_SUCCESS)
	{
		tileHeight = 8;
		LogMessage("Failed to query tileHeight");
	}

	if (element->QueryIntAttribute("firstgid", &firstId) != TIXML_SUCCESS)
	{
		LogMessage("Failed to query firstId");
		firstId = 0;
	}

	TiXmlElement* imageElement = element->FirstChildElement("image");
	if (imageElement)
	{
		std::string fname;

		if (imageElement->QueryIntAttribute("width", &imageWidth) != TIXML_SUCCESS)
		{
			imageWidth = 8;
			LogMessage("Failed to query imageWidth");
		}
		if (imageElement->QueryIntAttribute("height", &imageHeight) != TIXML_SUCCESS)
		{
			imageHeight = 8;
			LogMessage("Failed to query imageheight");
		}
		if (imageElement->QueryStringAttribute("source", &fname) != TIXML_SUCCESS)
		{
			fname = "";
			LogMessage("Failed to query image filename");
		}
		else
		{
			std::string texFname = fname;
			fname = levelDir + "/";
			fname.append(texFname);
		}

		texture = fname;
	}
	else
	{
		imageWidth = 8;
		imageHeight = 8;
		texture = "";
	}

	flags.resize(tileWidth * tileHeight, GameConst::TFEmpty);

	TiXmlElement* tileInfoElement = element->FirstChildElement("tile");
	while (tileInfoElement)
	{
		int id = -1;

		tileInfoElement->Attribute("id", &id);
		if (id >= 0)
		{
			TiXmlElement* propertiesElement = tileInfoElement->FirstChildElement("properties");
			if (propertiesElement)
			{
				TiXmlElement* tilePropertyElement = propertiesElement->FirstChildElement("property");
				while (tilePropertyElement)
				{
					const char* name = tilePropertyElement->Attribute("name");
					if (name)
					{
						if (strcmp(name, "solid") == 0)
						{
							std::cout << "found solid\n";
							const char* value = tilePropertyElement->Attribute("value");
							if (value && equalsIgnoreCase(value, "true"))
							{
								flags[id] = flags[id] | GameConst::TFSolid;
								std::cout << "solid is true, id: " << id << " - " << flags[id] << "\n";
							}
						}
					}
					tilePropertyElement = tilePropertyElement->NextSiblingElement("property");
				}
			}
		}
		tileInfoElement = tileInfoElement->NextSiblingElement("tile");
	}

	tileset.setup(firstId, tileWidth, tileHeight, imageWidth, imageHeight, texture, flags);

	return tileset;
}

lbSceneTileMap* createTileMapFromXmlElement(TiXmlElement* element, TilesetList& tilesets)
{
	LogEntryPoint();
	int collumns, rows;
	bool failed = false;

	if (element->QueryIntAttribute("height", &rows) != TIXML_SUCCESS)
	{
		failed = true;
		LogMessage("Failed to query map height");
	}

	if (element->QueryIntAttribute("width", &collumns) != TIXML_SUCCESS)
	{
		failed = true;
		LogMessage("Failed to query map width");
	}
	TiXmlElement* dataElement = element->FirstChildElement("data");

	lbSceneTileMap* tileMap = NULL;

	if (dataElement && !failed)
	{

		const char* tileData;
		tileData = dataElement->GetText();
		std::string encoding;
		dataElement->QueryStringAttribute("encoding", &encoding);
		if (encoding == "csv")
		{
			int tileId = 0;
			std::string str(tileData);
			std::stringstream ss(str);

			int curTileset = -1;
			char peeked;
			int curTile = 0;

			while (!ss.fail() && curTile < collumns * rows)
			{
				peeked = ss.peek();
				while ((peeked < '0' || peeked > '9') && peeked != '-' && peeked != '+')
				{
					ss.ignore();
					peeked = ss.peek();
				}
				ss >> tileId;
				if (tileId != 0)
				{
					curTileset = getTilesetUsed(tilesets, tileId);
					break;
				}
				curTile++;
			}

			tileMap = new lbSceneTileMap();
			tileMap->getTileMap().resize(collumns, rows);
			tileMap->getTileMap().setTileSize(TILE_SIZE, TILE_SIZE);
			lbTileMap::Tile *tiles = tileMap->getTileMap().getTiles();
			if (curTileset != -1)
			{
				int tcx, tcy;
				curTile = 0;

				std::stringstream ss(str);

				while (!ss.fail() && curTile < collumns * rows)
				{
					peeked = ss.peek();
					while ((peeked < '0' || peeked > '9') && peeked != '-' && peeked != '+')
					{
						ss.ignore();
						peeked = ss.peek();
					}
					ss >> tileId;

					tilesets.set[curTileset].getTileCoord(tileId, tcx, tcy);
					if (tileId == 0 || tcx < 0 || tcx > tilesets.set[curTileset].columns || tcy < 0 || tcy > tilesets.set[curTileset].rows)
					{
						tcx = 255;
						tcy = 255;
					}
					tiles[curTile].renderInfo = lbTileMap::getTileTextureCoord(tcx, tcy);

					curTile++;
				}
				tileMap->getTileMap().setTexture(tilesets.set[curTileset].texture, tilesets.set[curTileset].columns, tilesets.set[curTileset].rows);
			}
			else
			{
				LogMessage("No tileset is used for tileid "<<tileId);
				int invisibleTileRenderInfo = lbTileMap::getTileTextureCoord(255, 255);
				for (int i = 0; i < collumns * rows; i++)
				{
					tiles[i].renderInfo = invisibleTileRenderInfo;
				}
				//failed = true;
			}
		}
		else
		{
			LogMessage("Encoding is wrong");
			failed = true;
		}
	}

	if (tileMap && failed)
	{
		LogMessage("Loading map failed");
		delete tileMap;
		tileMap = NULL;
	}

	return tileMap;
}

Map* createCollisionMapFromXmlElement(TiXmlElement* element, TilesetList& tilesets)
{
	LogEntryPoint();
	int collumns, rows;
	bool failed = false;

	if (element->QueryIntAttribute("height", &rows) != TIXML_SUCCESS)
	{
		failed = true;
		LogMessage("Loading collision map height failed");
	}

	if (element->QueryIntAttribute("width", &collumns) != TIXML_SUCCESS)
	{
		failed = true;
		LogMessage("Loading collision map width failed");
	}
	TiXmlElement* dataElement = element->FirstChildElement("data");

	Map* map = NULL;

	if (dataElement && !failed)
	{
		const char* tileData;
		tileData = dataElement->GetText();
		std::string encoding;
		dataElement->QueryStringAttribute("encoding", &encoding);
		if (encoding == "csv")
		{

			int tileId = 0;
			std::string str(tileData);
			std::stringstream ss(str);

			int curTileset = -1;
			char peeked;
			int curTile = 0;

			while (!ss.fail() && curTile < collumns * rows)
			{
				peeked = ss.peek();
				while ((peeked < '0' || peeked > '9') && peeked != '-' && peeked != '+')
				{
					ss.ignore();
					peeked = ss.peek();
				}
				ss >> tileId;
				if (tileId != 0)
				{
					curTileset = getTilesetUsed(tilesets, tileId);
					std::cout << "Determining tileset for tileId: " << tileId << " = " << curTileset << std::endl;
					break;
				}
				curTile++;
			}
			map = new Map(collumns, rows);

			if (curTileset != -1)
			{
				std::stringstream ss(str);
				while (!ss.fail())
				{
					ss >> tileId;
					if (tileId > 0)
					{
						std::cout << "tileId: " << tileId << " - " << tilesets.set[curTileset].tileFlags[tileId - tilesets.set[curTileset].firstId] << " | ";
						map->tiles[curTile].tileFlags = tilesets.set[curTileset].tileFlags[tileId - tilesets.set[curTileset].firstId]; //(tileId ? GameConst::TFSolid : GameConst::TFEmpty);
					}
					else
					{
						map->tiles[curTile].tileFlags = GameConst::TFEmpty;
					}
					curTile++;
					if (ss.peek() == ',')
						ss.ignore();
				}
				std::cout<<std::endl;
			}
			else
			{
				std::cout << "Failed to determine tileset for collision map\n";
			}
		}
		else
		{
			LogMessage("Wrong encoding used");
			failed = true;
		}
	}

	if (map && failed)
	{
		LogMessage("Loading collision map failed");
		delete map;
		map = NULL;
	}

	return map;
}
