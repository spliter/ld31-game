/*
 * Class.h
 *
 *  Created on: 15 lut 2014
 *      Author: spliter
 */

#pragma once

#include <string>


struct t_ClassDef
{
	t_ClassDef(std::string _className, t_ClassDef* _parent){className=_className;parent=_parent;}
	std::string getClassName(){return className;}
	t_ClassDef* getParent(){return parent;}
	bool inheritsFrom(t_ClassDef* entDef){return (entDef==this) || (parent&&parent->inheritsFrom(entDef));}
	bool inheritsFromName(std::string _className){return (_className==className) || (parent&&parent->inheritsFromName(className));}
private:
	t_ClassDef *parent;
	std::string className;
};

typedef t_ClassDef *ClassDef;


class Object
{
public:
	static ClassDef getClassDef(){static t_ClassDef entDef("Class",NULL); return &entDef;}
	static ClassDef getParentClass(){return Object::getClassDef()->getParent();}
	static std::string getClassName(){return Object::getClassDef()->getClassName();}
	static std::string getParentClassName(){return std::string("");}
	static Object* cast(Object* ent, ClassDef classDef){ if(ent && ent->instanceOf(classDef))return ent;return NULL;}//returns an entity only if it inherits from this class
	template <typename T> static T* cast(Object* ent){ if(ent && ent->instanceOf(T::getClassDef()))return (T*)ent;return NULL;}
	static bool classInheritsFrom(ClassDef classDef){return (classDef==Object::getClassDef());}
	virtual ClassDef getClass(){return Object::getClassDef();}
	virtual bool instanceOf(ClassDef classDef){return getClass()->inheritsFrom(classDef);}

	template <typename T> bool instanceOf(){return getClass()->inheritsFrom(T::getClassDef());}

	virtual ~Object(){};
};


template <typename T> T* cast(Object* obj){return obj?(obj->instanceOf(T::getClassDef())?(T*)obj:NULL):NULL;}

#define DEFINE_CLASS(className, parentClassName)\
	public:\
		static ClassDef getClassDef(){static t_ClassDef entDef(#className,parentClassName::getClassDef()); return &entDef;}\
		static ClassDef getParentClass(){return className::getClassDef()->getParent();}\
		static std::string getClassName(){return className::getClassDef()->getClassName();}\
		static std::string getParentClassName(){return parentClassName::getClassName();}\
		static className* castEntity(Object* ent){ return ent?(ent->instanceOf(className::getClassDef())?(className*)ent:NULL):NULL;}\
		static bool classInheritsFrom(ClassDef classDef){return className::getClassDef()->inheritsFrom(classDef);}\
		virtual ClassDef getClass(){return className::getClassDef();}\
		typedef parentClassName super;
