/*
 * drawUtils.h
 *
 *  Created on: 16-09-2012
 *      Author: Spliter
 */

#pragma once

void drawCircle(float x, float y, float radius, int segments = 20, bool drawFilled=true);
void drawRect(float x1, float y1, float x2,float y2, bool drawFilled=true);
void drawLine(float x1, float y1, float x2,float y2);
void drawAxis(float size);
