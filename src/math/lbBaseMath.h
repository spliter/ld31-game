/*
 * lbBaseMath.h
 *
 *  Created on: 11-04-2011
 *      Author: Spliter
 */

#ifndef LBBASEMATH_H_
#define LBBASEMATH_H_

#include <math.h>
#include <stdlib.h>

#define DEG_TO_RAD_CONST 0.0174532925199432957692369076848
#define RAD_TO_DEG_CONST 57.295779513082320876798154814105
#define ERROR_MARGIN 0.0000000001

inline float degToRad(float deg){return deg*DEG_TO_RAD_CONST;}
inline float radToDeg(float rad){return rad*RAD_TO_DEG_CONST;}
inline float sqr(float x){return x*x;}
inline float slowdown(float x){return (2-x)*x;}
inline float speedup(float x){return x*x;}
inline float smoothstep(float x){return x*x*(3-2*x);}
inline float smoothstep(float edge1, float edge2, float ratio){float x=(ratio-edge1)/(edge2-edge1);return smoothstep(x)*(edge2-edge1)+edge1;}
inline float smoothstepN(float edge1, float edge2, float ratio){float x=(ratio-edge1)/(edge2-edge1);return smoothstep(x);}
inline float lerp(float start,float end,float amt){return start+(end-start)*amt;}
inline float trilerp(float a,float b,float c,float u,float v){return (a-c)*u+(b-c)*v+c;}
inline bool inRange(float start,float end,float x){return x>start && x<end;}
inline float clamp(float min,float max,float x){float aux=(x>min?x:min);return (aux<max?aux:max);}
inline int clampi(int min,int max,int x){float aux=(x>min?x:min);return (aux<max?aux:max);}
inline float normalize(float min,float max,float x){return clamp(0,1,(x-min)/(max-min));}
inline float distance(float x1,float y1,float x2,float y2){return sqrt(sqr(x2-x1)+sqr(y2-y1));}
inline int sign(float x){return (x > 0) ? 1 : ((x < 0) ? -1 : 0);}
inline float maxf(float a,float b) {return (a)>(b)?(a):(b);}
inline float minf(float a,float b) {return (a)<(b)?(a):(b);}
inline int maxi(int a,int b) {return (a)>(b)?(a):(b);}
inline int mini(int a,int b) {return (a)<(b)?(a):(b);}

float lerpDeg(float start,float end,float amt);
float lerpRad(float start,float end,float amt);

float direction(float x,float y);
float directionDeg(float x,float y);

float lengthDirX(float deg,float length);
float lengthDirY(float deg,float length);

float angleDifRad(float start, float end);//[-PI,+PI]
float angleDifDeg(float start, float end);//[-180,+180]

float random(float max);//open max
float random(float min,float max);//closed min and max
int randomi(int max);//open max
int randomi(int min, int max);//closed min and max


#endif /* LBBASEMATH_H_ */
