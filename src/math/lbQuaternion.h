/*
 * lbQuaternion.h
 *
 *  Created on: 17-05-2011
 *      Author: Spliter
 */
#pragma once

#include <math.h>
#include "lbVec3.h"
#include "lbMatrix4x4.h"

#define LB_CLASS_NAME __internal_lbQuaternion_t
#define LB_AANGLE_CLASS_NAME __internal_lbAxisAngle_t
#define LB_VECTOR_CLASS_NAME lbVec3f
#define LB_4x4MATRIX_CLASS_NAME lbMatrix4x4f
#define LB_PARAM_TYPE float
#include "internal/lbQuaternion_internal" // IWYU pragma: export
typedef LB_CLASS_NAME lbQuaternion;
typedef LB_AANGLE_CLASS_NAME lbAxisAngle;
#undef LB_CLASS_NAME
#undef LB_AANGLE_CLASS_NAME
#undef LB_VECTOR_CLASS_NAME
#undef LB_4x4MATRIX_CLASS_NAME
#undef LB_PARAM_TYPE

#define LB_CLASS_NAME __internal_lbQuaterniond_t
#define LB_AANGLE_CLASS_NAME __internal_lbAxisAngled_t
#define LB_VECTOR_CLASS_NAME lbVec3d
#define LB_4x4MATRIX_CLASS_NAME lbMatrix4x4d
#define LB_PARAM_TYPE double
#include "internal/lbQuaternion_internal" // IWYU pragma: export
typedef LB_CLASS_NAME lbQuaterniond;
typedef LB_AANGLE_CLASS_NAME lbAxisAngled;
#undef LB_CLASS_NAME
#undef LB_AANGLE_CLASS_NAME
#undef LB_VECTOR_CLASS_NAME
#undef LB_4x4MATRIX_CLASS_NAME
#undef LB_PARAM_TYPE
