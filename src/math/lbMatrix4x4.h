/*
 * lbMatrix4x4f.h
 *
 *  Created on: 23-04-2011
 *      Author: Spliter
 */
#pragma once

#include "lbVec3.h"
#include "lbMatrix3x3.h"

#define LB_CLASS_NAME __internal_lbMatrix4x4f_t
#define LB_VECTOR_CLASS_NAME lbVec3f
#define LB_3x3_MATRIX_CLASS_NAME lbMatrix3x3f
#define LB_PARAM_TYPE float
#include "internal/lbMatrix4x4_internal" // IWYU pragma: export
typedef __internal_lbMatrix4x4f_t lbMatrix4x4f;
#undef LB_CLASS_NAME
#undef LB_VECTOR_CLASS_NAME
#undef LB_3x3_MATRIX_CLASS_NAME
#undef LB_PARAM_TYPE

#define LB_CLASS_NAME __internal_lbMatrix4x4d_t
#define LB_VECTOR_CLASS_NAME lbVec3d
#define LB_3x3_MATRIX_CLASS_NAME lbMatrix3x3d
#define LB_PARAM_TYPE double
#include "internal/lbMatrix4x4_internal" // IWYU pragma: export
typedef __internal_lbMatrix4x4d_t lbMatrix4x4d;
#undef LB_CLASS_NAME
#undef LB_VECTOR_CLASS_NAME
#undef LB_3x3_MATRIX_CLASS_NAME
#undef LB_PARAM_TYPE
