/*
 * ZombieMonsterEntity.h
 *
 *  Created on: 4 gru 2014
 *      Author: Miko Kuta
 */

#pragma once

#include <game/entities/monsters/MonsterEntity.h>
#include <math/lbVec2.h>

class PlayerEntity;

struct GameContext;
class lbCameraSceneNode2D;
class lbSprite;

class ZombieMonsterEntity: public MonsterEntity
{
public:
	DEFINE_CLASS(ZombieMonsterEntity,MonsterEntity);

	ZombieMonsterEntity(GameContext* context);
	virtual ~ZombieMonsterEntity();

	virtual void onCreated();
	virtual void onSpawned();
	virtual void onUpdate(float dt);
	virtual void draw(lbCameraSceneNode2D* camera);

	void onHitByBullet(lbVec2f position, lbVec2f speed, int damage);
protected:
};

