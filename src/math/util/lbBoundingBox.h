/*
 * lbBoundingBox.h
 *
 *  Created on: 26-03-2013
 *      Author: Spliter
 */

#pragma once

#include <float.h>
#include <math/lbBaseMath.h>
#include <math/lbMatrix4x4.h>
#include <math/lbVec3.h>
#include <math.h>

struct lbBoundingBox
{
	lbVec3f min;
	lbVec3f max;

	lbBoundingBox()
	{
		min.set(0,0,0);
		max.set(0,0,0);
	}

	lbBoundingBox(const lbBoundingBox& bbox)
	{
		min=bbox.min;
		max=bbox.max;
	}

	lbBoundingBox(lbVec3f _min, lbVec3f _max)
	{
		min=_min;
		max=_max;
	}

	lbBoundingBox(float minx, float miny, float minz, float maxx, float maxy, float maxz)
	{
		min.set(minx,miny,minz);
		max.set(maxx,maxy,maxz);
	}

	void set(lbBoundingBox& bbox)
	{
		min=bbox.min;
		max=bbox.max;
	}

	void set(lbVec3f _min, lbVec3f _max)
	{
		min=_min;
		max=_max;
	}

	void set(float minx, float miny, float minz, float maxx, float maxy, float maxz)
	{
		min.set(minx,miny,minz);
		max.set(maxx,maxy,maxz);
	}

	void prepareForVertices()
	{
		min.set(FLT_MAX, FLT_MAX, FLT_MAX);
		max.set(-FLT_MAX, -FLT_MAX, -FLT_MAX);
	}

	void addVertice(lbVec3f vert)
	{
		min.x = minf(vert.x,min.x);
		min.y = minf(vert.y,min.y);
		min.z = minf(vert.z,min.z);

		max.x = maxf(vert.x,max.x);
		max.y = maxf(vert.y,max.y);
		max.z = maxf(vert.z,max.z);
	}

	lbBoundingBox transformed(lbMatrix4x4f &mat)
	{
		lbVec3f corner[8];
		corner[0].set(min.x,min.y,min.z);
		corner[1].set(max.x,min.y,min.z);
		corner[2].set(min.x,max.y,min.z);
		corner[3].set(max.x,max.y,min.z);

		corner[4].set(min.x,min.y,max.z);
		corner[5].set(max.x,min.y,max.z);
		corner[6].set(min.x,max.y,max.z);
		corner[7].set(max.x,max.y,max.z);
		for(int i=0;i<8;i++)
		{
			corner[i] = mat*corner[i];
		}

		lbBoundingBox bbox;
		bbox.prepareForVertices();
		for(int i=0;i<8;i++)
		{
			bbox.addVertice(corner[i]);
		}
		return bbox;
	}

	void merge(lbBoundingBox &bbox)
	{
		min.x = minf(bbox.min.x,min.x);
		min.y = minf(bbox.min.y,min.y);
		min.z = minf(bbox.min.z,min.z);

		max.x = maxf(bbox.max.x,max.x);
		max.y = maxf(bbox.max.y,max.y);
		max.z = maxf(bbox.max.z,max.z);
	}

	void translate(lbVec3f translation)
	{
		min+=translation;
		max+=translation;
	}

	bool isDegenerated()
	{
		return min.x>max.x || min.y>max.y || min.z>max.z;
	}

	//note: returns true only if neither is degenerated and they intersect
	bool intersects(lbBoundingBox& bbox)
	{
		if(bbox.isDegenerated() || isDegenerated())
		{
			return false;
		}
		return 	(min.x<bbox.max.x && bbox.min.x<max.x) &&
				(min.y<bbox.max.y && bbox.min.y<max.y) &&
				(min.z<bbox.max.z && bbox.min.z<max.z);
	}

	bool hasInfiniteValue()
	{
		return isinf(min.x) || isinf(min.y) || isinf(min.z) || isinf(max.x) || isinf(max.y) || isinf(max.z);
	}

	void getProjection(lbVec3f normal, float& _min, float& _max)
	{
		if(isDegenerated())
		{
			_min = FLT_MAX;
			_max = FLT_MAX;
			return;
		}
		if(hasInfiniteValue())
		{
			_min = -FLT_MAX;
			_max = FLT_MAX;
			return;
		}

		_min = FLT_MAX;
		_max = -FLT_MAX;

		lbVec3f corner[8];
		corner[0].set(min.x,min.y,min.z);
		corner[1].set(max.x,min.y,min.z);
		corner[2].set(min.x,max.y,min.z);
		corner[3].set(max.x,max.y,min.z);

		corner[4].set(min.x,min.y,max.z);
		corner[5].set(max.x,min.y,max.z);
		corner[6].set(min.x,max.y,max.z);
		corner[7].set(max.x,max.y,max.z);
		for(int i=0;i<8;i++)
		{
			float d = normal.dot(corner[i]);
			_min = minf(_min,d);
			_max = maxf(_max,d);
		}
	}
};
