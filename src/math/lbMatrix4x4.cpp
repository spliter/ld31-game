/*
 * lbMatrix4x4.cpp
 *
 *  Created on: 3 gru 2014
 *      Author: Miko Kuta
 */



#include "lbMatrix4x4.h"

#define LB_CLASS_NAME __internal_lbMatrix4x4f_t
#define LB_VECTOR_CLASS_NAME lbVec3f
#define LB_3x3_MATRIX_CLASS_NAME lbMatrix3x3f
#define LB_PARAM_TYPE float
#include "internal/lbMatrix4x4_impl_internal"
#undef LB_CLASS_NAME
#undef LB_VECTOR_CLASS_NAME
#undef LB_3x3_MATRIX_CLASS_NAME
#undef LB_PARAM_TYPE

#define LB_CLASS_NAME __internal_lbMatrix4x4d_t
#define LB_VECTOR_CLASS_NAME lbVec3d
#define LB_3x3_MATRIX_CLASS_NAME lbMatrix3x3d
#define LB_PARAM_TYPE double
#include "internal/lbMatrix4x4_impl_internal"
#undef LB_CLASS_NAME
#undef LB_VECTOR_CLASS_NAME
#undef LB_3x3_MATRIX_CLASS_NAME
#undef LB_PARAM_TYPE
