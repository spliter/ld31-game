/*
 * CollisionUtility.cpp
 *
 *  Created on: 19 sty 2014
 *      Author: spliter
 */

//#include <math/lbMath.h>
#include <math/lbVec2.h>
#include <utils/debug/debug.h>
#include "CollisionUtility.h"
#include <iostream>
#include <iterator>
#include <utility>
#include <vector>

typedef std::pair<lbVec2f, int> vertPair;
/*
 * Checks which order are the vertices (if clockwise or counter clockwise) and copies them so they are counter clockwise
 */
void copyVerticesWithCorrectOrientation(std::vector<lbVec2f>& dst, std::vector<lbVec2f>& src)
{
	float sum = 0;
	for (unsigned int i = 0; i < src.size(); i++)
	{
		int id2 = (i + 1) % src.size();
		float xx = (src[id2].x - src[i].x);
		float yy = (src[id2].y + src[i].y);
		sum += xx * yy;
	}
	if (sum > 0)
	{
		//it's all right, source is counter clockwise
		dst.clear();
		dst = src;
	}
	else
	{
		dst.clear();
		for (int i = src.size() - 1; i >= 0; i--)
		{
			dst.push_back(src[i]);
		}
	}
}

bool isInsideTriangle(lbVec2f C, lbVec2f B, lbVec2f A, lbVec2f P)
{
	LogEntryPoint();
	// Compute vectors
	lbVec2f v0 = C - A;
	lbVec2f v1 = B - A;
	lbVec2f v2 = P - A;

	// Compute dot products
	float dot00 = v0.dot(v0);
	float dot01 = v0.dot(v1);
	float dot02 = v0.dot(v2);
	float dot11 = v1.dot(v1);
	float dot12 = v1.dot(v2);

	// Compute barycentric coordinates
	float invDenom = 1 / (dot00 * dot11 - dot01 * dot01);
	float u = (dot11 * dot02 - dot01 * dot12) * invDenom;
	float v = (dot00 * dot12 - dot01 * dot02) * invDenom;

	// Check if point is in triangle
	return (u >= 0) && (v >= 0) && (u + v < 1);
}

bool isEar(int index, std::vector<vertPair>& verts)
{
	LogEntryPoint();
	int i0 = (index + verts.size() - 1) % verts.size();
	int i1 = index;
	int i2 = (index + 1) % verts.size();

	lbVec2f v0 = verts[i0].first;
	lbVec2f v1 = verts[i1].first;
	lbVec2f v2 = verts[i2].first;

	lbVec2f v1to0 = v0 - v1;
	lbVec2f v1to2 = v2 - v1;

//1 check if we're facing the right direction;
	if (v1to0.cross(v1to2) < 0)
	{
		return false;
	}

//2 check if there's a vertice inside us
	for (int i = i2 + 1; i != i0; i = (i + 1) % verts.size())
	{
		if (isInsideTriangle(v0, v1, v2, verts[i].first))
		{
			return false;
		}
	}

	return true;
}

bool runEarClippingAlgorithm(std::vector<vertPair>& verts, std::vector<int> & retTriangles)
{
	LogEntryPoint();
//note: we assume counter clockwise polygons
	while (verts.size() > 3)
	{
		bool foundEar = false;
		for (unsigned int i = 0; i < verts.size(); i++)
		{
			if (isEar(i, verts))
			{
				int i0 = (i + verts.size() - 1) % verts.size();
				int i1 = i;
				int i2 = (i + 1) % verts.size();

				std::cout << "vertex " << verts[(i)].second << " is ear " << verts[(i0)].first << " , " << verts[(i1)].first << " , " << verts[(i2)].first << std::endl;
				retTriangles.push_back(verts[i0].second);
				retTriangles.push_back(verts[i1].second);
				retTriangles.push_back(verts[i2].second);
				verts.erase(verts.begin() + i);
				foundEar = true;
				break;
			}
		}

		if (!foundEar)
		{
			//something has gone terribly wrong. most likely a self intersecting polygon. We stop here
//			Logger::logText("Could not find an ear in polygon, it's probably self intersecting");
			std::cout << "Could not find an ear in polygon, it's probably self intersecting\n";
			return false;
		}
	}
	if (verts.size() == 3)
	{
		retTriangles.push_back(verts[0].second);
		retTriangles.push_back(verts[1].second);
		retTriangles.push_back(verts[2].second);
	}
	return true;
}
