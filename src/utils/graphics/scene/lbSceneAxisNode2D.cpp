/*
 * lbSceneAxisNode2D.cpp
 *
 *  Created on: 2 mar 2014
 *      Author: spliter
 */

#include <utils/graphics/scene/lbSceneAxisNode2D.h>

lbSceneAxisNode2D::lbSceneAxisNode2D()
{

}

lbSceneAxisNode2D::~lbSceneAxisNode2D()
{
}


void lbSceneAxisNode2D::draw(lbCameraSceneNode2D* camera)
{
		lbTexture::unbind();
		glPushMatrix();
		glMultMatrixf(getGLWorldTransform().me);

		glBegin(GL_LINES);
		glColor3f(0.1f, 0.0f, 0.0f);
		glVertex2f(0, 0);
		glColor3f(1.0f, 0.0f, 0.0f);
		glVertex2f(1, 0);

		glColor3f(0.1f, 0.0f, 0.0f);
		glVertex2f(0.8, 0.1);
		glColor3f(1.0f, 0.0f, 0.0f);
		glVertex2f(1, 0);

		glColor3f(0.1f, 0.0f, 0.0f);
		glVertex2f(0.8, -0.1);
		glColor3f(1.0f, 0.0f, 0.0f);
		glVertex2f(1, 0);




		glColor3f(0.0f, 0.1f, 0.0f);
		glVertex2f(0, 0);
		glColor3f(0.0f, 1.0f, 0.0f);
		glVertex2f(0, 1);

		glColor3f(0.0f, 0.1f, 0.0f);
		glVertex2f(0.1f, 0.8f);
		glColor3f(0.0f, 1.0f, 0.0f);
		glVertex2f(0, 1);

		glColor3f(0.0f, 0.1f, 0.0f);
		glVertex2f(-0.1f, 0.8f);
		glColor3f(0.0f, 1.0f, 0.0f);
		glVertex2f(0, 1);
		glEnd();

		glPopMatrix();
	}
