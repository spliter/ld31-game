/*
 * SolidEntity.cpp
 *
 *  Created on: 4 gru 2014
 *      Author: Miko Kuta
 */

#include <events/input/lbInputSystem.h>
#include <events/input/lbKeys.h>
#include <game/entities/SolidEntity.h>
#include <game/GameEntityFactoryManager.h>
#include <game/level/GameLevel.h>
#include <game/states/GameState.h>
#include <math/lbVec2.h>
#include <utils/graphics/scene/lbCameraSceneNode2D.h>

SolidEntity::SolidEntity(GameContext* context):
	GameEntity(context)
{

}


SolidEntity::~SolidEntity()
{
}
//
//void SolidEntity::onCreated()
//{
//	super::onCreated();
//}
//
//void SolidEntity::onDestroy()
//{
//	super::onDestroy();
//}
