/*
 * debug.cpp
 *
 *  Created on: 6 gru 2014
 *      Author: Miko Kuta
 */

#include <utils/debug/debug.h>

#ifndef LOG_IMPORTING
#ifdef LOG_ENTRY_POINT

#define FLUSH_ENTRY_POINTS
static std::list<std::string>& getFuncStack()
{
	static std::list<std::string> funcStack;
	return funcStack;
}

FuncEntryPoint::FuncEntryPoint(std::string file, std::string line, std::string function)
{
	getFuncStack().push_back(function + " @ " + file + ":" + line);
#ifdef FLUSH_ENTRY_POINTS
	flushCurrentEntryPoint();
#endif

}
FuncEntryPoint::~FuncEntryPoint()
{
	getFuncStack().pop_back();
}

void FuncEntryPoint::flushCurrentEntryPoint()
{
	if (!getFuncStack().empty())
	{
		std::cout << getFuncStack().back() << std::endl;
		std::cout.flush();
	}
	else
	{
		std::cout << "stack empty\n";
		std::cout.flush();
	}
}

void FuncEntryPoint::flushStack()
{
	if (!getFuncStack().empty())
	{
		std::list<std::string>::iterator iter = getFuncStack().begin();
		int i = 0;
		std::cout << "DEBUG printing stack:\n";
		while (iter != getFuncStack().end())
		{
			std::cout << "\t" << i << "-" << (*iter) << std::endl;
		}
		std::cout.flush();
	}
	else
	{
		std::cout << "stack empty\n";
		std::cout.flush();
	}
}
#endif
#endif
