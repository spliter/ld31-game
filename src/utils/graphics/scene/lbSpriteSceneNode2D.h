/*
 * lbSpriteSceneNode2D.h
 *
 *  Created on: 1 mar 2014
 *      Author: spliter
 */

#pragma once

#include <utils/graphics/lbSprite.h>
#include <utils/graphics/scene/lbSceneNode2D.h>
#include <utils/Object.h>

class lbCameraSceneNode2D;
class lbSceneLayer2D;

class lbSpriteSceneNode2D: public lbSceneNode2D
{
	DEFINE_CLASS(lbSpriteSceneNode2D, Object)
public:
	lbSpriteSceneNode2D();
	virtual ~lbSpriteSceneNode2D();

	void setSprite(lbSprite* sprite);
	lbSprite* getSprite(){return _sprite;}
	void setFrame(int frame){_curFrame=frame;}
	int getFrame(){return _curFrame;}

	void setPosition(float x, float y);
	void setColour(float r,float g,float b,float a){_r=r;_g=b;_b=b;_a=a;}
	void setColour(float r,float g,float b){_r=r;_g=b;_b=b;_a=1;}

	virtual void draw(lbCameraSceneNode2D* camera);
private:
	float _x,_y;
	float _r,_g,_b,_a;
	int _curFrame;
	lbSprite* _sprite;
};
