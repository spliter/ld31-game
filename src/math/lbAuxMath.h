/*
 * lbAuxMath.h
 *
 *  Created on: 21 sty 2014
 *      Author: spliter
 */

#pragma once


//distances and velocities
inline float convertKilometersPerHourToMetersPerSecond(float kph){return kph*0.27777777777;}
inline float convertMetersPerSecondToKilometersPerHour(float mps){return mps*3.6f;}
inline float convertMilesToKilometers(float Mph){return Mph*1.60934f;}
inline float convertKilometersToMiles(float Mph){return Mph*0.621371f;}

//temperatures
inline float convertKalvinToCelsius(float kalvin){return kalvin-273.15f;}
inline float convertKalvinToFarenheit(float kalvin){return 1.8f*(kalvin-273.15f)+32;}

inline float convertCelsiusToKalvin(float celsius){return celsius+273.15f;}
inline float convertCelsiusToFarenheit(float celsius){return 1.8f*(celsius)+32;}

inline float convertFarenheitToKalvin(float farenheit){return (farenheit-32)*0.55555555f;}
inline float convertFarenheitToCelsius(float farenheit){return (farenheit-32)*0.55555555f+273.15f;}
