/*
 * lbRect.h
 *
 *  Created on: 13-09-2012
 *      Author: Spliter
 */

#pragma once

#include <math/lbBaseMath.h>
#include <math/lbMatrix3x3.h>
#include <math/lbVec2.h>
#include <cfloat>

struct lbRectF
{
	float x1, y1, x2,y2;

	lbRectF()
	{
		x1=0;y1=0;x2=0;y2=0;
	}

	lbRectF(float _x1, float _y1, float _x2, float _y2)
	{
		set(_x1,_y1,_x2,_y2);
	}

	lbRectF(const lbRectF& rect)
	{
		set(rect);
	}

	lbRectF& set(float _x1, float _y1, float _x2, float _y2)
	{
		x1=_x1;y1=_y1;x2=_x2;y2=_y2;
		return *this;
	}

	lbRectF& set(const lbRectF& rect)
	{
		x1=rect.x1;y1=rect.y1;x2=rect.x2;y2=rect.y2;
		return *this;
	}

	int getWidth(){return x2-x1;}
	int getHeight(){return y2-y1;}

	lbRectF& scale(float sx, float sy)
	{
		x1*=sx;y1*=sy;x2*=sx;y2*=sy;
		return *this;
	}

	lbRectF scaled(float sx, float sy) const
	{
		return lbRectF(*this).scale(sx,sy);
	}

	lbRectF& move(float dx, float dy)
	{
		x1+=dx;y1+=dy;x2+=dx;y2+=dy;
		return *this;
	}

	lbRectF moved(float dx, float dy) const
	{
		return lbRectF(*this).move(dx,dy);
	}

	lbRectF& crop(float minx, float miny, float maxx, float maxy)
	{
		x1=x1>minx?(x1<maxx?x1:maxx):minx;
		x2=x2>minx?(x2<maxx?x2:maxx):minx;
		y1=y1>miny?(y1<maxy?y1:maxy):miny;
		y2=y2>miny?(y2<maxy?y2:maxy):miny;
		return *this;
	}

	lbRectF cropped(float minx, float miny, float maxx, float maxy) const
	{
		return lbRectF(*this).crop(minx,miny,maxx,maxy);
	}

	static lbRectF getTotalRange(const lbRectF &rect1, const lbRectF &rect2)
	{
		lbRectF rect;
		rect.x1 = rect1.x1<rect2.x1?rect1.x1:rect2.x1;
		rect.y1 = rect1.y1<rect2.y1?rect1.y1:rect2.y1;
		rect.x2 = rect1.x2>rect2.x2?rect1.x2:rect2.x2;
		rect.y2 = rect1.y2>rect2.y2?rect1.y2:rect2.y2;
		return rect;
	}

	bool isInside(float x, float y) const
	{
		return (x>=x1 && x<x2) && (y>y1 && y<y2);
	}

	bool isIntersecting(const lbRectF& rect) const
	{
		return (x1<=rect.x2 && x2>=rect.x1)&&(y1<=rect.y2 && y2>=rect.y1);
	}

	void prepareForVertices()
	{
		x1=FLT_MAX;
		y1=FLT_MAX;
		x2=-FLT_MAX;
		y2=-FLT_MAX;
	}

	void addVertice(lbVec2f vert)
	{
		x1 = minf(vert.x,x1);
		y1 = minf(vert.y,y1);

		x2 = maxf(vert.x,x2);
		y2 = maxf(vert.y,y2);
	}

	lbRectF &transform(const lbMatrix3x3f &mat)
	{
		lbVec2f corner[4];
		corner[0].set(x1,y1);
		corner[1].set(x1,y2);
		corner[2].set(x2,y2);
		corner[3].set(x2,y2);

		corner[0] = mat*corner[0];
		corner[1] = mat*corner[1];
		corner[2] = mat*corner[2];
		corner[3] = mat*corner[3];
		prepareForVertices();
		addVertice(corner[0]);
		addVertice(corner[1]);
		addVertice(corner[2]);
		addVertice(corner[3]);
		return *this;
	}

	lbRectF transformed(const lbMatrix3x3f &mat)
	{
		lbRectF bbox(*this);
		bbox.transform(mat);
		return bbox;
	}
};

struct lbRectI
{
	int x1, y1, x2,y2;

	lbRectI()
	{
		x1=0;y1=0;x2=0;y2=0;
	}

	lbRectI(int _x1, int _y1, int _x2, int _y2)
	{
		set(_x1,_y1,_x2,_y2);
	}

	lbRectI(const lbRectI& rect)
	{
		set(rect);
	}

	lbRectI& set(int _x1, int _y1, int _x2, int _y2)
	{
		x1=_x1;y1=_y1;x2=_x2;y2=_y2;
		return *this;
	}

	lbRectI& set(const lbRectI& rect)
	{
		x1=rect.x1;y1=rect.y1;x2=rect.x2;y2=rect.y2;
		return *this;
	}

	int getWidth(){return x2-x1;}
	int getHeight(){return y2-y1;}

	lbRectI& scale(float sx, float sy)
	{
		x1*=sx;y1*=sy;x2*=sx;y2*=sy;
		return *this;
	}

	lbRectI scaled(float sx, float sy) const
	{
		return lbRectI(*this).scale(sx,sy);
	}

	lbRectI& move(int dx, int dy)
	{
		x1+=dx;y1+=dy;x2+=dx;y2+=dy;
		return *this;
	}

	lbRectI moved(float dx, float dy) const
	{
		return lbRectI(*this).move(dx,dy);
	}

	lbRectI& crop(int minx, int miny, int maxx, int maxy)
	{
		x1=x1>minx?(x1<maxx?x1:maxx):minx;
		x2=x2>minx?(x2<maxx?x2:maxx):minx;
		y1=y1>miny?(y1<maxy?y1:maxy):miny;
		y2=y2>miny?(y2<maxy?y2:maxy):miny;
		return *this;
	}

	lbRectI cropped(int minx, int miny, int maxx, int maxy) const
	{
		return lbRectI(*this).crop(minx,miny,maxx,maxy);
	}

	static lbRectI getTotalRange(const lbRectI &rect1, const lbRectI &rect2)
	{
		lbRectI rect;
		rect.x1 = rect1.x1<rect2.x1?rect1.x1:rect2.x1;
		rect.y1 = rect1.y1<rect2.y1?rect1.y1:rect2.y1;
		rect.x2 = rect1.x2>rect2.x2?rect1.x2:rect2.x2;
		rect.y2 = rect1.y2>rect2.y2?rect1.y2:rect2.y2;
		return rect;
	}

	bool isInside(float x, float y) const
	{
		return (x>=x1 && x<x2) && (y>y1 && y<y2);
	}

	bool isIntersecting(const lbRectI& rect) const
	{
		return (x1<=rect.x2 && x2>=rect.x1)&&(y1<=rect.y2 && y2>=rect.y1);
	}
};

inline lbRectF toRectF(lbRectI rect)
{
	return lbRectF(rect.x1,rect.y1,rect.x2,rect.y2);
}

inline lbRectI toRectI(lbRectF rect)
{
	return lbRectI(rect.x1,rect.y1,rect.x2,rect.y2);
}
