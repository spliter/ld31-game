/*
 * GameConstants.h
 *
 *  Created on: 01-12-2012
 *      Author: Spliter
 */

#pragma once

extern int TILE_SIZE;
extern int TILE_IMPORT_SIZE;

namespace GameConst
{
	enum Direction
	{
		DirUnknown = 0x00000000,
		DirLeft =  0x01000000,
		DirUp =    0x02000000,
		DirRight = 0x04000000,
		DirDown =  0x08000000,
	};
	enum TileFlags
	{
		TFMaskTileX=0xf00000ff,
		TFMaskTileY=0xf000ff00,
		TFMaskTilePos=TFMaskTileX|TFMaskTileY,

		TFMaskType=	0x00ff0000,
		TFEmpty=	0x00000000,
		TFSolid=	0x00010000,
		TFGround=	0x00020000,

		TFMaskWalls= 0x0f000000,
		TFoffsetWalls=24,
		TFWallNone=  DirUnknown,
		TFWallLeft=	 DirLeft,
		TFWallTop=	 DirUp,
		TFWallRight= DirRight,
		TFWallBottom= DirDown,

		//flag to be used while processing the map but erased afterwards
		TFMaskUtil=	0xf0000000,
		TFUtilTemp0=0x80000000,
		TFUtilTemp1=0x40000000,
		TFUtilTemp2=0x20000000,
		TFUtilTemp3=0x10000000,
	};



	inline int getTileX(int tileFlags){return (tileFlags&TFMaskTileX);}
	inline int getTileY(int tileFlags){return (tileFlags&TFMaskTileY)>>8;}
	inline int getTileType(int tileFlags){return tileFlags&TFMaskType;}
	inline int getTileUtil(int tileFlags){return tileFlags&TFMaskUtil;}
	inline int getTileWalls(int tileFlags){return tileFlags&TFMaskWalls;}
	inline int setTilePosition(int x, int y){return (x&0xff)|((y&0xff)<<8);}


	inline void getPositionFromDirection(Direction direction, int& x, int& y)
	{
		x=0;
		y=0;
		switch(direction)
		{
			case DirLeft:x=-1;y=0;break;
			case DirRight:x=1;y=0;break;
			case DirUp:x=0;y=-1;break;
			case DirDown:x=0;y=1;break;
			default:break;
		}
	}

	inline Direction getDirectionFromPosition(float x, float y)
	{
		bool xby = x>y;
		bool mxby = x>-y;
		if(xby)
		{
			if(mxby)
			{
				return DirRight;
			}
			else
			{
				return DirUp;
			}
		}
		else
		{
			if(mxby)
			{
				return DirDown;
			}
			else
			{
				return DirLeft;
			}
		}
	}

	inline bool canMoveToPosition(int tileFlags, int x, int y)
	{
		int mask=0;

		if(x<0)
		{
			mask|=DirLeft;
		}
		else if(x>0)
		{
			mask|=DirRight;
		}

		if(y<0)
		{
			mask|=DirUp;
		}
		else if(x>0)
		{
			mask|=DirDown;
		}

		return !(tileFlags&mask);
	}


	inline unsigned int rgbaFtoI(float r, float g, float b, float a)
	{
		int color = 0;
		color |= (int(a * 255) & 0xff) << 24;
		color |= (int(b * 255) & 0xff) << 16;
		color |= (int(g * 255) & 0xff) << 8;
		color |= (int(r * 255) & 0xff);
		return color;
	}

	inline unsigned int rgbaitoI(int r, int g, int b, int a)
	{
		int color = 0;
		color |= (r & 0xff);
		color |= (g & 0xff) << 8;
		color |= (b & 0xff) << 16;
		color |= (a & 0xff) << 24;
		return color;
	}

	inline void rgbaItoF(int color, float *r, float *g, float *b, float *a)
	{
		*a = ((color >> 24) & 0xff) / 255.0f;
		*b = ((color >> 16) & 0xff) / 255.0f;
		*g = ((color >> 8) & 0xff) / 255.0f;
		*r = ((color) & 0xff) / 255.0f;
	}

};
