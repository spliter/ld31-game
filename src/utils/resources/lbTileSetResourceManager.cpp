/*
 * lbTextureManager.cpp
 *
 *  Created on: 25-05-2013
 *      Author: Spliter
 */

//#include <utils/graphics/lbSprite.h>
#include <utils/graphics/lbTexture.h>
#include <utils/resources/lbTileSetResourceManager.h>

lbTileSetResourceManager::lbTileSetResourceManager(lbTextureResourceManager* textureManager)
{
	_textureManager = textureManager;
	createDefaultTileSet();
}

lbTileSetResourceManager::~lbTileSetResourceManager()
{
	delete _defaultTileSet;
	delete _defaultTexture;
}

lbTileSet* lbTileSetResourceManager::loadResourceFromFile(const std::string & fname)
{
	return lbTileSet::loadFromFile(fname,_textureManager);
}

void lbTileSetResourceManager::createDefaultTileSet()
{
	int texWidth = 8;
	int texHeight = 8;
	int texBpp = 4;
	unsigned char data[texWidth * texHeight * texBpp];
	for(int x = 0; x < texWidth; x++)
		for(int y = 0; y < texHeight; y++)
		{
			int i = (y * texWidth + x);

			int* datai = (int*) data;
			if((y + x) % 2)
			{
				datai[i] = 0xffbf5680;
			}
			else
			{
				datai[i] = 0xff000000;
			}
		}
	_defaultTexture = new lbTexture(texWidth, texHeight, data);
	_defaultTileSet = new lbTileSet(_defaultTexture,0,0,_defaultTexture->getWidth(),_defaultTexture->getHeight(),1,1);
}
