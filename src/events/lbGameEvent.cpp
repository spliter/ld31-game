/*
 * lbGameEvent.cpp
 *
 *  Created on: 10-01-2013
 *      Author: Spliter
 */

#include "lbGameEvent.h"

lbGameEventListener::lbGameEventListener()
{
	_hasEmptyLinks = false;
	_isFiringEvent = false;
}

lbGameEventListener::~lbGameEventListener()
{
	LinkIter iter = _listenerLinks.begin();
	while(iter!=_listenerLinks.end())
	{
		if(*iter)
		{
			(*iter)->removeEmmiter(this);
		}
		iter++;
	}

	while(!_emmiterLinks.empty())
	{
		int emmiterCount = _emmiterLinks.size();
		(*_emmiterLinks.begin())->removeGameEventListener(this);
		if((int)_emmiterLinks.size()==emmiterCount)
		{
			_emmiterLinks.pop_front();
		}
	}
}

void lbGameEventListener::addGameEventListener(lbGameEventListener* listener)
{
	LinkIter iter = findListener(listener);
	if(iter==_listenerLinks.end())
	{
		_listenerLinks.push_back(listener);
		listener->addEmmiter(this);
	}
}

void lbGameEventListener::removeGameEventListener(lbGameEventListener* listener)
{
	LinkIter listenerIter = findListener(listener);
	if(listenerIter!=_listenerLinks.end())
	{
		listener->removeEmmiter(this);

		if(!_isFiringEvent)
		{
			_listenerLinks.erase(listenerIter);
		}
		else
		{
			(*listenerIter) = NULL;
			_hasEmptyLinks = true;
		}
	}
}

void lbGameEventListener::removeEmptyLinks()
{
	_listenerLinks.remove(NULL);
	_hasEmptyLinks = false;
}

void lbGameEventListener::fireEvent(lbGameEvent* event)
{
	_isFiringEvent = true;
	LinkIter iter = _listenerLinks.begin();
	while(iter!=_listenerLinks.end())
	{
		if((*iter))
		{
			(*iter)->handleEvent(event);
		}
		iter++;
	}
	_isFiringEvent = false;
	if(_hasEmptyLinks)
	{
		removeEmptyLinks();
	}
}


lbGameEventListener::LinkIter lbGameEventListener::findListener(lbGameEventListener* listener)
{
	LinkIter iter = _listenerLinks.begin();
	while(iter!=_listenerLinks.end())
	{
		if((*iter)==listener)
		{
			return iter;
		}
		iter++;
	}
	return _listenerLinks.end();
}


void lbGameEventListener::addEmmiter(lbGameEventListener* emmiter)
{
	_emmiterLinks.push_back(emmiter);
}

void lbGameEventListener::removeEmmiter(lbGameEventListener* emmiter)
{
	_emmiterLinks.remove(emmiter);
}
