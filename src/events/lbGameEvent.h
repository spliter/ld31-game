/*
 * lbGameEvent.h
 *
 *  Created on: 30-07-2011
 *      Author: Spliter
 */

#pragma once
#include <list>

struct lbEventDef_t{};
typedef lbEventDef_t* lbEventType;

class lbGameEvent
{
public:
	virtual ~lbGameEvent(){}
	static lbEventType classDef(){static lbEventDef_t def; return &def;}
	virtual lbEventType getType(){return lbGameEvent::getType();}
};

#define DEFINE_EVENT(eventClassName)\
	public:\
		static lbEventType classDef(){static lbEventDef_t def; return &def;}\
		virtual lbEventType getType(){return eventClassName::classDef();}



#define eventHandleStart(eventVarName,eventName,eventClass)\
		if(eventVarName->getType()==eventClass::classDef())\
		{\
			eventClass* eventName=(eventClass*)(eventVarName);

#define eventHandleEnd() }

#define DELEGATE_EVENT(eventVarName, eventClass, eventHandlerFunction) if(eventVarName->getType()==eventClass::classDef()){this->eventHandlerFunction((eventClass*)eventVarName);}

class lbGameEventListener
{
public:
	lbGameEventListener();
	virtual ~lbGameEventListener();

	void addGameEventListener(lbGameEventListener* listener);
	void removeGameEventListener(lbGameEventListener* listener);

	virtual void handleEvent(lbGameEvent* event){};

protected:
	void fireEvent(lbGameEvent* event);
private:
	typedef std::list<lbGameEventListener*>::iterator LinkIter;

	void removeEmptyLinks();
	void addEmmiter(lbGameEventListener* emmiter);
	void removeEmmiter(lbGameEventListener* emmiter);
	LinkIter findListener(lbGameEventListener* listener);

	std::list<lbGameEventListener*> _emmiterLinks;//emmiters I listen to
	std::list<lbGameEventListener*> _listenerLinks;//listeners I emmit to
	bool _isFiringEvent;
	bool _hasEmptyLinks;
};
