/*
 * lbEntityManager.h
 *
 *  Created on: 10-11-2012
 *      Author: Spliter
 */

#pragma once

#include <stddef.h>
#include <iostream>

template <class T>
class lbEntityManager
{
public:
	lbEntityManager(int maxEntities);
	virtual ~lbEntityManager();

	void reset();//note: it deletes everything and resets the freeIds

	//note: entityId 0 is reserved as "NULL" entity
	//note: entity pointers should never be stored outside the update() functions as they might be deleted, use the id instead.
	T* getEntity(int index) const;
	T* getEntityUid(int uniqueId) const;
	virtual int addEntity(T* entity);
	virtual void deleteKilledEntities();
	virtual void deleteAllEntities();

	T* getHeadEntity() const {return _headEntity;}
	T** getEntities() const {return _entities;}
	int getMaxEntityNum() const {return _maxEntityNum;}

	bool hasFreeIds() const {return _freeIdHead!=0;}
private:

	int useFreeId();//returns 0 if no freeIds
	void addFreeId(int freeId);
	void deleteEntity(T* entity);
	T* _headEntity;
	T** _entities;
	int* _freeIds;
	//note: all freeIds that aren't the _freeIdHead hold the index of the next freeId
	int _freeIdHead;//if 0 then there are no free ids
	int _maxEntityNum;
	int _lastUniqueId;
};

template <class T>
lbEntityManager<T>::lbEntityManager(int maxEntities)
{
	_maxEntityNum = maxEntities;
	_entities = new T*[_maxEntityNum];
	_freeIds = new int[_maxEntityNum];
	_headEntity=NULL;
	_freeIdHead = _maxEntityNum-1;
	_entities[0]=NULL;
	_lastUniqueId=0;
	for(int i=1;i<_maxEntityNum;i++)
	{
		_entities[i]=NULL;
		_freeIds[i]=i-1;
	}
}

template <class T>
lbEntityManager<T>::~lbEntityManager()
{
	for(int i=0;i<_maxEntityNum;i++)
	{
		if(_entities[i])
		{
			_entities[i]->callDestroy();
			delete _entities[i];
		}
	}
	delete[] _entities;
	delete[] _freeIds;
}

template <class T>
void lbEntityManager<T>::reset()
{
	deleteAllEntities();
	_lastUniqueId=0;
}

template <class T>
T* lbEntityManager<T>::getEntity(int id) const
{
	if(id<_maxEntityNum && id>0)
	{
		return _entities[id];
	}
	return NULL;
}

template <class T>
T* lbEntityManager<T>::getEntityUid(int uniqueId) const
{
	T* cur = _headEntity;
	if(!uniqueId)
	{
		return NULL;
	}
	while(cur)
	{
		if(cur->_uniqueId==uniqueId)
		{
			return cur;
		}
		cur=cur->_next;
	}
	return NULL;
}


template <class T>
int lbEntityManager<T>::addEntity(T* entity)
{
	int id = useFreeId();
	if(id)
	{
		_lastUniqueId+=1;
		entity->_uniqueId=_lastUniqueId;
		entity->_id=id;
		_entities[id]=entity;

		entity->_prev=NULL;
		if(_headEntity==NULL)
		{
			entity->_next=NULL;
		}
		else
		{
			_headEntity->_prev = entity;
			entity->_next = _headEntity;
		}
		_headEntity = entity;

		_headEntity ->callCreate();
	}
	return id;
}

template <class T>
void lbEntityManager<T>::deleteKilledEntities()
{
	for(int i=0;i<_maxEntityNum;i++)
	{
		if(_entities[i] && _entities[i]->_isKilled)
		{
			_entities[i]->callDestroy();
			addFreeId(_entities[i]->_id);
			deleteEntity(_entities[i]);
			_entities[i]=NULL;
		}
	}
}

template <class T>
void lbEntityManager<T>::deleteAllEntities()
{
	for(int i=0;i<_maxEntityNum;i++)
	{
		if(_entities[i])
		{
			_entities[i]->callDestroy();
			deleteEntity(_entities[i]);
			_entities[i]=NULL;
		}
	}

	_freeIdHead = _maxEntityNum-1;
	for(int i=1;i<_maxEntityNum;i++)
	{
		_freeIds[i]=i-1;
	}
}


template <class T>
int lbEntityManager<T>::useFreeId()
{
	if(_freeIdHead)
	{
		int id = _freeIdHead;
		_freeIdHead=_freeIds[_freeIdHead];
		return id;
	}

	return 0;
}

template <class T>
void lbEntityManager<T>::addFreeId(int freeId)
{
	if(freeId && freeId<_maxEntityNum)
	{
		_freeIds[freeId]=_freeIdHead;
		_freeIdHead=freeId;
	}
	else
	{
		std::cout<<"Attempting to add free id: "<<freeId<<" but can only add up to "<<_maxEntityNum-1<<std::endl;
	}
}

template <class T>
void lbEntityManager<T>::deleteEntity(T* entity)
{
	if(entity!=_headEntity)
	{
		entity->_prev->_next = entity->_next;
	}
	else
	{
		_headEntity = entity->_next;
	}
	if(entity->_next)
	{
		entity->_next->_prev = entity->_prev;
	}
	delete entity;
}
