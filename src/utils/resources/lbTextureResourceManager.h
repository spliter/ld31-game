/*
 * lbTextureManager.h
 *
 *  Created on: 25-05-2013
 *      Author: Spliter
 */

#pragma once
#include <utils/graphics/lbTexture.h>
#include <utils/resources/lbResourceManager.h>
#include <string>



class lbTextureResourceManager: public lbResourceManager<lbTexture>
{
public:
	lbTextureResourceManager();
	virtual ~lbTextureResourceManager();


	virtual lbTexture* getDefaultResource(){return _defaultTexture;}
	virtual lbTexture* loadResourceFromFile(const std::string & fname);
	virtual void freeResource(lbTexture* resource){delete resource;}

private:
	void createDefaultTexture();
	lbTexture* _defaultTexture;
};
