/*
 * lbVec2.cpp
 *
 *  Created on: 3 gru 2014
 *      Author: Miko Kuta
 */


#include "lbVec2.h"

#define LB_CLASS_NAME __internal_lbVec2f_t
#define LB_PARAM_TYPE float
#include "internal/lbVec2_impl_internal"
#undef LB_CLASS_NAME
#undef LB_PARAM_TYPE

#define LB_CLASS_NAME __internal_lbVec2d_t
#define LB_PARAM_TYPE double
#include "internal/lbVec2_impl_internal"
#undef LB_CLASS_NAME
#undef LB_PARAM_TYPE
