/*
 * lbVec2ff.h
 *
 *  Created on: 3 gru 2014
 *      Author: Miko Kuta
 */

#pragma once

#include "lbBaseMath.h"
#include <ostream>

#define LB_CLASS_NAME __internal_lbVec2f_t
#define LB_PARAM_TYPE float
#include "internal/lbVec2_internal" // IWYU pragma: export
typedef __internal_lbVec2f_t lbVec2f;
#undef LB_CLASS_NAME
#undef LB_PARAM_TYPE

#define LB_CLASS_NAME __internal_lbVec2d_t
#define LB_PARAM_TYPE double
#include "internal/lbVec2_internal" // IWYU pragma: export
typedef __internal_lbVec2d_t lbVec2d;
#undef LB_CLASS_NAME
#undef LB_PARAM_TYPE

