/*
 * UiElementGroup.h
 *
 *  Created on: 24-09-2012
 *      Author: Spliter
 */

#pragma once

#include "UiElement.h"
#include <list>

class UiElementGroup: public UiElement
{
public:
	UiElementGroup();
	virtual ~UiElementGroup();

	virtual void update(unsigned long frameDiffMillis);
	virtual void draw();

	virtual void addElement(UiElement* element);
	virtual void removeElement(UiElement* element);

	virtual bool handleMouseButtonEvent(lbMouseButtonEvent* event);//returns true if the event has been handled
	virtual bool handleMouseMotionEvent(lbMouseMotionEvent* event);//returns true if the event has been handled

	virtual void markTransformDirty();

protected:
	virtual void updateChildren(unsigned long frameDiffMillis);
	virtual void drawChildren();

	std::list<UiElement*> _children;
};
