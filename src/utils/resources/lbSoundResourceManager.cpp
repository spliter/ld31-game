/*
 * lbSoundResourceManager.cpp
 *
 *  Created on: 21-09-2013
 *      Author: Spliter
 */

#include "lbSoundResourceManager.h"
#include <core/lbAudioSystem.h>
#include <climits>
#include <math.h>
#include <string.h>
#include <iostream>

lbSoundResourceManager::lbSoundResourceManager()
{
	createDefaultSound();
}

lbSoundResourceManager::~lbSoundResourceManager()
{
	FMOD_Sound_Release(_defaultSound);
}

FMOD_SOUND* lbSoundResourceManager::loadResourceFromFile(const std::string & fname)
{
	FMOD_SOUND *sound=NULL;
	FMOD_RESULT result = FMOD_System_CreateSound(lbAudioSystem::instance().getSoundSystem(),fname.c_str(),FMOD_DEFAULT,0,&sound);
	if(result!=FMOD_OK)
	{
		std::cout<<"Failed to load sound "<<fname<<std::endl<<" error: "<<result<<" - "<<lbAudioSystem::getErrorString(result)<<std::endl;
		return NULL;
	}
	return sound;
}

void lbSoundResourceManager::createDefaultSound()
{
//	FMOD_SYSTEM* system = lbAudioSystem::instance().getSoundSystem();
//	const int sampleRate = 44100;
//	const int channelNum = 1;
//	const float lengthInSeconds = 0.1f;
//	const int dataLen = (int) (sampleRate * lengthInSeconds) * channelNum;
//	short data[dataLen];
//
//	const float amplitude = SHRT_MAX/2;
//	const float frequency = 440;
//	const float increment = ((float)sampleRate/frequency)/(M_PI*2);
//	float t=0;
//
//	for(int i=0;i<dataLen;i++)
//	{
//		data[i] = sin(t)*amplitude;
//		t+=increment;
//	}
//
//	FMOD_CREATESOUNDEXINFO soundInfo;
//	memset(&soundInfo, 0, sizeof(FMOD_CREATESOUNDEXINFO));
//
//	soundInfo.cbsize = sizeof(FMOD_CREATESOUNDEXINFO);
//	soundInfo.length = dataLen*sizeof(short);
//	soundInfo.numchannels = channelNum;
//	soundInfo.defaultfrequency = sampleRate;
//	soundInfo.format = FMOD_SOUND_FORMAT_PCM16;
//	soundInfo.decodebuffersize = sampleRate;
//
//	FMOD_SOUND *sound = NULL;
//	FMOD_RESULT result = FMOD_System_CreateSound(system,(const char*)data, FMOD_OPENMEMORY|FMOD_CREATESAMPLE, &soundInfo, &sound);
//	if(!sound)
//	{
//		std::cout << "Failed to create a default sound\n";
//	}
//	lbAudioSystem::FMODErrorCheck(result);
//	_defaultSound = sound;
	_defaultSound = NULL;
}
