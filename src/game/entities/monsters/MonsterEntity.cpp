/*
 * MonsterEntity.cpp
 *
 *  Created on: 4 gru 2014
 *      Author: Miko Kuta
 */

#include <game/entities/monsters/MonsterEntity.h>
#include <game/entities/particles/BloodParticleSpawner.h>
#include <game/entities/PlayerEntity.h>
#include <game/entities/SolidRectangleEntity.h>
#include <game/GameContext.h>
#include <game/level/GameLevel.h>
#include <game/states/GameState.h>
#include <Glee.h>
#include <utils/debug/debug.h>
#include <utils/graphics/lbColor.h>
#include <utils/graphics/lbTexture.h>
#include <utils/graphics/scene/lbSceneLayer2D.h>
#include <cstdio>
#include <cstdlib>
#include <list>
#include <vector>


//REGISTER_GAME_ENTITY(MonsterEntity, "generic_monster");

MonsterEntity::MonsterEntity(GameContext* context) :
		SolidEntity(context), _monsterSprite(NULL),
		_timeSinceShove(0),
		_shoveForceDecay(1),
		_shoveStunDuration(0.3f),
		_healthPoints(100),
		_maxHealthPoints(100),
		_playerId(0),
		_playerUid(0),
		_isAlive(false)
{
	_colliders[0] = &_collider;
}

MonsterEntity::~MonsterEntity()
{
}

void MonsterEntity::onCreated()
{
	LogEntryPoint();
	super::onCreated();
	addChild(&_spriteNode);
	_collider.makeCircle(0, 0, 6, 4);
}

void MonsterEntity::onSpawned()
{
	LogEntryPoint();
	super::onSpawned();

	GameLevel* level = getLevel();
	lbAssert(level);
	level->characterLayer.addNode(&_spriteNode);
	level->enemiesWorld.add(this);
	setZOrder(-1);
	_shoveForce.set(0, 0);
	_timeSinceShove = _shoveStunDuration;
	_healthPoints = 100;
}

void MonsterEntity::onDestroy()
{
	GameLevel* level = getLevel();
	lbAssert(level);
	level->characterLayer.removeNode(&_spriteNode);
	level->enemiesWorld.remove(this);
	super::onDestroy();
}

void MonsterEntity::onUpdate(float dt)
{
	LogEntryPoint();
	if (_timeSinceShove < _shoveStunDuration)
	{
		_timeSinceShove += dt;
	}
	_shoveForce -= _shoveForce* _shoveForceDecay* dt;
}

void MonsterEntity::draw(lbCameraSceneNode2D* camera)
{
	LogEntryPoint();
	lbTexture::unbind();
	glColor3f(1,0,0);
	_collider.updatePointsIfNeeded();
	glBegin(GL_LINE_STRIP);
	for(int i=0;i<=(int)_collider.getTransformedConvexVerts().size();i++)
	{
		int id = i%_collider.getTransformedConvexVerts().size();
		glVertex2fv(_collider.getTransformedConvexVerts()[id].me);
	}
	glEnd();
}


void MonsterEntity::die()
{
	if(_isAlive)
	{
		_isAlive = false;
		onDeath();
	}
}

void MonsterEntity::onHitByBullet(lbVec2f position, lbVec2f speed, int damage)
{
	if(damage>10)
	{
		BloodParticleSpawner* spawner = BloodParticleSpawner::castEntity(getGameContext()->gameState->spawnEntity("blood_spawner"));
		if(spawner)
		{
			spawner->spray(position,speed*0.1f,2,0.5,30,lbColor(1.0f,0.0f,0.0f,1.0f),0,2.0f,1.0f,30,true);
		}
	}
	applyDamage(damage);
}

void MonsterEntity::setBaseSprite(lbSprite* sprite)
{
	_monsterSprite = sprite;
	_spriteNode.setSprite(sprite);
}

void MonsterEntity::setShoveStunDuration(float shoveStunDuration)
{
	_shoveStunDuration = shoveStunDuration;
}

void MonsterEntity::setShoveDecay(float shoveDecay)
{
	_shoveForceDecay = shoveDecay;
}

void MonsterEntity::setHealth(int healthAmount)
{
	_healthPoints=healthAmount;
	if(_healthPoints>_maxHealthPoints)
	{
		_healthPoints = _maxHealthPoints;
	}
}

void MonsterEntity::applyDamage(int damagePoints)
{
	_healthPoints-=damagePoints;
	if(_healthPoints<0)
	{
		_healthPoints = 0;
		onDeath();
		kill();
	}
}

void MonsterEntity::addHealth(int healthAmount)
{
	_healthPoints+=healthAmount;
	if(_healthPoints>_maxHealthPoints)
	{
		_healthPoints = _maxHealthPoints;
	}
}

PlayerEntity* MonsterEntity::getPlayer()
{
	PlayerEntity* playerEnt=NULL;

	if(_playerId!=0 && _playerUid!=0)
	{
		playerEnt = cast<PlayerEntity>(getGameContext()->gameState->getEntity(_playerId, _playerUid));
	}

	if (!playerEnt)
	{
		playerEnt = cast<PlayerEntity>(getGameContext()->gameState->findEntityByName("player"));
		if (playerEnt)
		{
			_playerId = playerEnt->getIndexId();
			_playerUid = playerEnt->getUniqueId();
		}
	}
	else
	{
		_playerId=0;
		_playerUid=0;
	}
	return playerEnt;
}


void MonsterEntity::checkCollisions()
{
	LogEntryPoint();
	GameLevel* level = getLevel();
	lbAssert(level);
	solidList solids = level->solidGeometryWorld.getSolids();
	solidIter iter = solids.begin();
	while (iter != solids.end())
	{
		SolidEntity* ent = (*iter);
		if (ent->instanceOf(SolidRectangleEntity::getClassDef()))
		{
			CollisionPoly** polys = ent->getCollisionPolygons();
			int polyNum = ent->getCollisionPolyNum();
			for (int i = 0; i < polyNum; i++)
			{
				lbVec2f separatingVector;
				bool collided = CollisionPoly::GetSeparatingVector(_collider, *polys[i], separatingVector);
				if (collided)
				{
					setPosition(getPosition() + separatingVector);
				}
			}
		}
		else
		{

		}
		iter++;
	}
//
//	PlayerEntity* playerEnt = cast<PlayerEntity>(getGameContext()->gameState->getEntity(_playerId, _playerUid));
//	if(playerEnt)
//	{
//		CollisionPoly** polys = playerEnt->getCollisionPolygons();
//		int polyNum = playerEnt->getCollisionPolyNum();
//		for (int i = 0; i < polyNum; i++)
//		{
//			lbVec2f separatingVector;
//			bool collided = CollisionPoly::GetSeparatingVector(_collider, *polys[i], separatingVector);
//			if (collided)
//			{
//				setPosition(getPosition() + separatingVector);
//				onCollidedWithPlayer(playerEnt);
//			}
//		}
//	}
}

