/*
 * main.cpp
 *
 *  Created on: 12-02-2011
 *      Author: Spliter
 */

#include <core/GameCore.h>
//#include <GLee.h>
#include <math/lbMatrix4x4.h>
#include <math/lbQuaternion.h>
#include <math/lbVec4.h>
#include <stddef.h>
//#include <SDL2/SDL.h>
#include <SDL2/SDL_main.h>
#include <utils/debug/Logger.h>
#include <utils/debug/debug.h>
#include <cstdlib>
#include <ctime>
#include <iostream>
#include <string>

using namespace std;

int main(int argc, char** argv)
{
	LogEntryPoint();
	//Note: for some stupid reason we have to put these here, otherwise it'll complain in linker
	lbMatrix4x4f mat;
	lbQuaternion quat;
	lbVec4f vec4;
	std::cout << "Started program\n";

	Logger::logText("Logging successful");
	try
	{
		srand(time(NULL));
		GameCore &core = GameCore::getInstance();
		if (core.init(16*64, 16*32))
		{
			std::cout << "Initialized program\n";
			core.run();
			core.deinit();
		}
		Logger::logText("Program Terminated Successfully.");
	} catch (...)
	{
		Logger::logText("Program crashed");
	}

	std::cout << "Finished program\n";

	Logger::flush();
	return 0;
}

std::string retrieveDirectory(std::string path)
{
	std::string levelDir = "";
	size_t pos = path.rfind('/', path.length() - 1);
	if (pos != std::string::npos)
	{
		levelDir = path.substr(0, pos);
		if (!levelDir.empty() && levelDir.at(levelDir.length() - 1) != '/')
		{
			levelDir += '/';
		}
	}
	else
	{
		size_t pos = path.rfind('\\', path.length() - 1);
		if (pos != std::string::npos)
		{
			levelDir = path.substr(0, pos);
			if (!levelDir.empty() && levelDir.at(levelDir.length() - 1) != '\\')
			{
				levelDir += '\\';
			}
		}
	}
	return levelDir;
}

std::string getGraphicCommonDirectory()
{
	return "data/graphics/";
}

std::string getSpriteCommonDirectory()
{
	return "data/graphics/sprites/";
}



std::string getSpriteFileExtension()
{
	return ".xml";
}

std::string buildSpriteFilename(std::string spriteName)
{
	return getSpriteCommonDirectory() + spriteName + getSpriteFileExtension();
}

std::string getLevelCommonDirectory()
{
	return "data/levels/";
}

std::string getLevelFileExtension()
{
	return ".tmx";
}

std::string buildLevelFilename(std::string levelName)
{
	return getLevelCommonDirectory() + levelName + getLevelFileExtension();
}
