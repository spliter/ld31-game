#pragma once

#include <GLee.h>
#include <math.h>
#include "Viewport.h"
#include <math/lbVec3.h>

#define DEGTORAD 0.017453292519943295769236907684886
struct glCamera
{
	Viewport vp;
	float x,y,z;
	bool isOrbit;
	float orbitDist;
	union
	{
		struct
		{
			float pitch, yaw, roll;
		};
		struct
		{
			float rotx, roty, rotz;
		};
	};

	glCamera()
	{
		x=0;y=0;z=0;
		pitch=0;yaw=0;roll=0;
		vp.set(0,0,1,1,0,0.1,100);
		isOrbit=false;
		orbitDist=10;
	}

	glCamera(float _x,float _y,float _z,float _pitch,float _yaw,float _roll)
	{
		set( _x, _y, _z, _pitch, _yaw, _roll);
		isOrbit=false;
		orbitDist=10;
	}

	void set(float _x,float _y,float _z,float _pitch,float _yaw,float _roll)
	{
		x=_x;
		y=_y;
		z=_z;
		pitch=_pitch;
		yaw=_yaw;
		roll=_roll;
	}

	void setRect(float _x, float _y, float _width, float _height)
	{
		vp.x=_x;
		vp.y=_y;
		vp.w=_width;
		vp.h=_height;
	}

	void setPlanes(float nearPlane, float farPlane)
	{
		vp.nearClip=nearPlane;
		vp.farClip=farPlane;
	}

	void setFoV(float fov)
	{
		vp.fov=fov;
	}

	void useCamera()
	{
		if(!isOrbit)
		{
			glRotatef(-roll,0,0,1);
			glRotatef(-pitch,1,0,0);
			glRotatef(-yaw,0,1,0);

			glTranslatef(-x,-y,-z);
		}
		else
		{

			glTranslatef(0,0,-orbitDist);

			glRotatef(-roll,0,0,1);
			glRotatef(-pitch,1,0,0);
			glRotatef(-yaw,0,1,0);


			glTranslatef(-x,-y,-z);
		}
	}


	void use()
	{
		setViewport(vp);
		useCamera();
	}

	void push()
	{
		pushViewport(vp);
		useCamera();
	}

	void pop()
	{
		popViewport();
	}

	void moveForward(float dist)
	{
		float sy=sin(yaw*DEGTORAD);
		float sp=sin(pitch*DEGTORAD);
		float cy=cos(yaw*DEGTORAD);
		float cp=cos(pitch*DEGTORAD);
		x-=sy*cp*dist;
		y+=sp*dist;
		z-=cy*cp*dist;
	}

	void moveRight(float dist)
	{
		float s=sin(yaw*DEGTORAD);
		float c=cos(yaw*DEGTORAD);
		x+=c*dist;
		z-=s*dist;
	}

	lbVec3f getForwardVector()
	{
		float sy=sin(yaw*DEGTORAD);
		float sp=sin(pitch*DEGTORAD);
		float cy=cos(yaw*DEGTORAD);
		float cp=cos(pitch*DEGTORAD);
		float x=-sy*cp;
		float y=sp;
		float z=-cy*cp;

		return lbVec3f(x,y,z);
	}

	lbVec3f getRightVector()
	{
		float s=sin(yaw*DEGTORAD);
		float c=cos(yaw*DEGTORAD);
		float x=c;
		float z=-s;
		return lbVec3f(x,0,z);
	}
};

//inline void useCamera(glCamera &cam)
//{
//	setViewport(vp);
//	glRotatef(-cam.roll,0,0,1);
//	glRotatef(-cam.pitch,1,0,0);
//	glRotatef(-cam.yaw,0,1,0);
//
//	glTranslatef(-cam.x,-cam.y,-cam.z);
//}

//moves camera forward (and backward, using negative dist)
inline void moveCameraForward(glCamera &cam,float dist)
{
//	float s=sin(cam.yaw*DEGTORAD);
//	float c=cos(cam.yaw*DEGTORAD);
//
//	cam.x-=s*dist;
//	cam.z+=c*dist;

	float sy=sin(cam.yaw*DEGTORAD);
	float sp=sin(cam.pitch*DEGTORAD);
	float cy=cos(cam.yaw*DEGTORAD);
	float cp=cos(cam.pitch*DEGTORAD);
	cam.x-=sy*cp*dist;
	cam.y+=sp*dist;
	cam.z-=cy*cp*dist;
}

//moves camera right (and left, using negative dist)
inline void moveCameraRight(glCamera &cam,float dist)
{
	float s=sin(cam.yaw*DEGTORAD);
	float c=cos(cam.yaw*DEGTORAD);

	cam.x+=c*dist;
	cam.z-=s*dist;
}
