/*
 * DebugCameraEntity.h
 *
 *  Created on: 4 gru 2014
 *      Author: Miko Kuta
 */

#pragma once

#include <game/GameEntity.h>
#include <game/GameContext.h>
#include <utils/Object.h>


struct Bullet
{
	union
	{
		int nextFreeId;
		float decaySpeed;
	};

	float decayCollision;
	float damageScale;
	float timeToLive;
	lbVec2f prevPos;
	lbVec2f pos;
	lbVec2f movement;


	bool collided;
	bool isALive;

	void create(lbVec2f position, lbVec2f movementVec, float damageScale,float decaySpeed, float decayCollision,float timeToLive)
	{
		isALive = true;
		collided = false;
		pos = position;
		prevPos = position;
		movement = movementVec;
		this->decaySpeed = decaySpeed;
		this->damageScale = damageScale;
		this->decayCollision= decayCollision;
		this->timeToLive = timeToLive;
	}
};
#define MAX_BULLET_NUM 256

class BulletControllerEntity: public GameEntity
{
public:
	DEFINE_CLASS(BulletControllerEntity,GameEntity);

	BulletControllerEntity(GameContext* context);
	virtual ~BulletControllerEntity();
	virtual void onCreated();
	virtual void onSpawned();
	virtual void onUpdate(float dt);

	virtual void draw(lbCameraSceneNode2D* camera);

	void spawnBullet(lbVec2f pos, lbVec2f movement,float damageScale, float decaySpeed, float decayCollision, float timeToLive);
private:
	void resetBullets();
	void killBullet(int bullet);
	void updateBullet(int bulletId, float dt);
	Bullet _bullets[MAX_BULLET_NUM];
	int _freeIdHead;
};

