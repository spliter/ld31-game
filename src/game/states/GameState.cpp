/*
 * GameState.cpp
 *
 *  Created on: 13-09-2012
 *      Author: Spliter
 */

#include <core/GameCore.h>
#include <events/input/lbInputEvents.h>
#include <events/input/lbInputSystem.h>
#include <FTGL/FTGLTextureFont.h>
#include <game/states/GameState.h>
#include <global.h>
#include <Glee.h>
#include <math/util/lbRect.h>
#include <SDL2/SDL_keycode.h>
#include <utils/debug/debug.h>
#include <utils/graphics/basic/Viewport.h>
#include <utils/graphics/lbTexture.h>
#include <cstdio>
#include <iostream>

GameState::GameState(GameCore *core) :
		_gameCore(core),
		_currentLevel(NULL),
		_curGameTime(0),
		_curSecondStart(0),
		_framesEllapsedInCurSecond(0),
		_fps(0),
		_gameSpeed(1.0f),
		_hasLevelToLoad(false),
		_messageFont(NULL),
		_bigFont(NULL)
{

}

GameState::~GameState()
{
}

void GameState::onStart()
{
	super::onStart();
	LogEntryPoint();
	_gameSpeed = 1.0f;
	_uiCamera.vp.set(0, 0, getScreenWidth(), getScreenHeight(), 0, -1000, 1000);
	_uiCamera.set(0, 0, 0, 0, 0, 0);

	_messageFont = new FTGLTextureFont("data/fonts/alphbeta.ttf");
	if (_messageFont->Error())
	{
		std::cout << "Failure when loading font: " << std::hex << _messageFont->Error() << std::dec << std::endl;
	}

	if (!_messageFont->FaceSize(16))
	{
		std::cout << "Failure setting font face size, error :" << std::hex << _messageFont->Error() << std::dec << std::endl;
	}

	_bigFont = new FTGLTextureFont("data/fonts/alphbeta.ttf");
	if (_bigFont->Error())
	{
		std::cout << "Failure when loading font: " << std::hex << _bigFont->Error() << std::dec << std::endl;
	}

	if (!_bigFont->FaceSize(48))
	{
		std::cout << "Failure setting font face size, error :" << std::hex << _bigFont->Error() << std::dec << std::endl;
	}

	_uiRoot.setup();

	_fpsTextBox.setText("Basic Text Box");
	_fpsTextBox.setPosition(0, 0);
	_fpsTextBox.setFont(_messageFont);
	_uiRoot.addElement(&_fpsTextBox);

	_curSecondStart = GameCore::getInstance().getCurrentFrameTime();
	_framesEllapsedInCurSecond = 0;
	_fps = 0;

	_curGameTime = 0;

	_gameContext.gameCore = _gameCore;
	_gameContext.gameState = this;

	_gameCore->enableFPSLimit(60);
	loadLevel("arena");
	lbInputSystem::getInstance().setMouseMode(lbMouseModeRelative);
	lbInputSystem::getInstance().setRelativeMouseSpeed(3.0f,3.0f);
	lbInputSystem::getInstance().warpMouse(getScreenWidth()/2,getScreenHeight()/2);
}

void GameState::onUpdate()
{
	LogEntryPoint();
	if(_hasLevelToLoad)
	{
		_hasLevelToLoad = false;

		if(_currentLevel)
		{
			_currentLevel->onEnd();
			delete _currentLevel;
		}
		_currentLevel = GameLevel::loadLevel(_gameContext,_levelToLoad);
//		_currentLevel = new GameLevel(_gameContext);
		_currentLevel->onStart();

//		spawnEntity("game_controller");
//		spawnEntity("player_entity");
////		spawnEntity("monster_spawner");
////		spawnEntity("generic_monster");

	}

	super::onUpdate();
	_debugStream.str("");
	unsigned long frameDiffMillis = GameCore::getInstance().getFrameTimeDifference();
	frameDiffMillis = 1000 / 60;
	float frameDif = ((float) frameDiffMillis / 1000.0f) * _gameSpeed;

	_curGameTime += frameDiffMillis;

	_framesEllapsedInCurSecond++;

	long curTime = GameCore::getInstance().getCurrentFrameTime();
	if (curTime - _curSecondStart > 1000)
	{
		double secTime = double(curTime - _curSecondStart) / 1000.0;
		_fps = double(_framesEllapsedInCurSecond) / secTime;
		_curSecondStart = curTime;
		_framesEllapsedInCurSecond = 0;
	}

	char fpsStr[100];
	fpsStr[0] = 0;
	sprintf(fpsStr, "FPS: %6.2f", _fps);
	_debugStream << fpsStr << std::endl;

	if (_currentLevel)
	{
		_currentLevel->onUpdate(frameDif);
	}

	_uiRoot.update(frameDiffMillis);
	_fpsTextBox.setText(_debugStream.str());
}

void GameState::onFrameRender()
{
	LogEntryPoint();
	super::onFrameRender();
	glClearColor(0.3, 0.3, 0.3, 1.0);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glDisable(GL_CULL_FACE);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_LIGHTING);
	glEnable(GL_TEXTURE_2D);
	glLoadIdentity();

	lbTexture::unbind();
	glColor3f(1, 1, 1);
	if (_currentLevel)
	{
		_currentLevel->onFrameRender();
	}

	lbRectF uiCameraRect;
	uiCameraRect.set(_uiCamera.x, _uiCamera.y, _uiCamera.x + _uiCamera.vp.w, _uiCamera.y + _uiCamera.vp.h);
	_uiCamera.push();
	_uiLayer.draw(uiCameraRect);
	_uiRoot.draw();
	_uiCamera.pop();
}

void GameState::onEnd()
{
	LogEntryPoint();
	_currentLevel->onEnd();
	delete _currentLevel;
	_currentLevel = NULL;
	delete _messageFont;
	super::onEnd();
}

void GameState::onMouseMotionEvent(lbMouseMotionEvent* event)
{
	LogEntryPoint();
}

void GameState::onMouseButtonEvent(lbMouseButtonEvent* event)
{
	LogEntryPoint();
}

void GameState::onMouseWheelEvent(lbMouseWheelEvent* event)
{
	LogEntryPoint();
}

void GameState::onKeyboardEvent(lbKeyboardEvent *event)
{
	LogEntryPoint();
	if (event->key == SDLK_ESCAPE)
	{
		if (event->type == lbKeyboardEvent::KeyPressed)
		{
			_gameCore->quit();
		}
	}
}

GameEntity* GameState::spawnEntity(const std::string& entName)
{
	LogEntryPoint();
	if (_currentLevel)
	{
		GameEntity* entity = _currentLevel->createEntity(entName);
		if (entity)
		{
			entity->callSpawn();
		}
		return entity;
	}
	return NULL;
}

GameEntity* GameState::findEntityByName(std::string name) const
{
	LogEntryPoint();
	if (_currentLevel)
	{
		GameEntity* curEnt = _currentLevel->entManager.getHeadEntity();
		while (curEnt)
		{
			if (curEnt->getName() == name)
			{
				return curEnt;
			}
			curEnt = curEnt->getNext();
		}
	}
	return NULL;
}

GameEntity* GameState::getEntity(int id) const
{
	LogEntryPoint();
	if (_currentLevel)
	{
		return _currentLevel->entManager.getEntity(id);
	}
	return NULL;
}

GameEntity* GameState::getEntity(int id, int uniqueId) const
{
	LogEntryPoint();
	if (_currentLevel)
	{
		GameEntity* ent = _currentLevel->entManager.getEntity(id);
		if (ent && ent->getUniqueId() == uniqueId)
		{
			return ent;
		}
	}
	return NULL;
}

GameEntity* GameState::getEntityUid(int uniqueId) const
{
	LogEntryPoint();
	if (_currentLevel)
	{
		return _currentLevel->entManager.getEntityUid(uniqueId);
	}
	return NULL;
}

void GameState::loadLevel(std::string levelName)
{
	LogEntryPoint();
	_hasLevelToLoad = true;
	_levelToLoad = levelName;
}
