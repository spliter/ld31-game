/*
 * lbSceneGridNode2D.h
 *
 *  Created on: 2 mar 2014
 *      Author: spliter
 */

#pragma once

#include <Glee.h>
#include <math/lbMatrix3x3.h>
#include <utils/graphics/lbTexture.h>
#include <utils/graphics/scene/lbSceneNode2D.h>
#include <utils/Object.h>

class lbCameraSceneNode2D;

class lbSceneGridNode2D: public lbSceneNode2D
{
DEFINE_CLASS(lbSceneGridNode2D,lbSceneNode2D)
public:
	lbSceneGridNode2D(){setGrid(1,1);}
	lbSceneGridNode2D(int columns, int rows);
	virtual ~lbSceneGridNode2D();
	virtual void draw(lbCameraSceneNode2D* camera);

	void setGrid(int columns, int rows){_columns = columns;_rows = rows;if(_columns<=0){_columns = 0;}if(_rows<=0){_rows= 0;}}
private:
	int _rows;
	int _columns;
};

