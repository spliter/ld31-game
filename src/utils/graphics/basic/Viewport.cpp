#include "Viewport.h"
#include <GLee.h>
#include <GL/glu.h>
#include <iostream>
void setViewport(Viewport &vp)
{
	glViewport(vp.x,vp.y,vp.w,vp.h);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
    if(vp.fov<=0.0001)
    {
//    	std::cout	<<"using viewport ortho with "
//    				<<vp.w<<"|"<<vp.h
//    				<<" <"<<vp.nearClip<<"|"<<vp.farClip<<">\n";
		glOrtho(0,vp.w,vp.h,0,vp.nearClip,vp.farClip);
    }
    else
    {
		if(vp.h!=0)
			gluPerspective(vp.fov,(float)vp.w/(float)vp.h,vp.nearClip,vp.farClip);
		else
			gluPerspective(vp.fov,1,vp.nearClip,vp.farClip);
//		std::cout<<"using viewport perspective with "<<vp.w<<"|"<<vp.h<<" <"<<vp.nearClip<<"|"<<vp.farClip<<">\n";
    }
    glMatrixMode(GL_MODELVIEW);
    //glLoadIdentity();
}

void pushViewport(Viewport &vp)
{
	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glPushAttrib(GL_VIEWPORT_BIT);
	setViewport(vp);
}
void popViewport()
{

	glMatrixMode(GL_PROJECTION);
	glPopAttrib();
	glPopMatrix();
	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();
}
