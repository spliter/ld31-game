/*
 * lbSpriteAnimator.cpp
 *
 *  Created on: 11-10-2012
 *      Author: Spliter
 */

#include "lbSpriteAnimator.h"
#include "../graphics/lbSprite.h"

lbSpriteAnimator::lbSpriteAnimator():
	_currentAnimationIndex(-1),
	_currentAnimationRef(-1),
	_currentAnimationTime(0),
	_isPlaying(false),
	_listener(NULL)
{
}

lbSpriteAnimator::~lbSpriteAnimator()
{
}

void lbSpriteAnimator::update(unsigned long frameMillisDiff)
{
	if(_isPlaying && _currentAnimationIndex>=0)
	{
		lbSpriteAnimation& anim = _animations[_currentAnimationIndex];
		int frameNum = anim.sprite->getFrameNum();
		int frameTime = 1000/anim.fps;
		_currentAnimationTime+=frameMillisDiff;
		anim.curFrame = _currentAnimationTime/frameTime;
		if(anim.curFrame>=frameNum)
		{
			if(anim.loop)
			{
				_currentAnimationTime%= frameTime*frameNum;
				anim.curFrame = _currentAnimationTime/frameTime;
				if(_listener)
				{
					_listener->onAnimationLooped(this,_currentAnimationRef);
				}
			}
			else
			{
				_currentAnimationTime= frameTime*frameNum-1;
				anim.curFrame = frameNum-1;
				_isPlaying = false;
				if(_listener)
				{
					_listener->onAnimationFinished(this,_currentAnimationRef);
				}
			}
		}
	}
}

lbSpriteAnimation lbSpriteAnimator::getCurrentAnimation()
{
	if(_currentAnimationIndex>=0)
	{
		return _animations[_currentAnimationIndex];
	}

	return lbSpriteAnimation();
}

int lbSpriteAnimator::getCurrentAnimationId()
{
	return _currentAnimationRef;
}

void lbSpriteAnimator::addAnimation(int refId, lbSprite* sprite, int fps, bool resetOnPause, bool loop)
{
	AnimIter iter = _animationMap.find(refId);
	if(iter!=_animationMap.end())
	{
		_animations[iter->second].set(sprite,0,resetOnPause,loop,fps);
		return;
	}

	lbSpriteAnimation anim;
	anim.set(sprite,0,resetOnPause,loop,fps);
	_animations.push_back(anim);
	_animationMap.insert(std::pair<const int,int>(refId,_animations.size()-1));
}

void lbSpriteAnimator::startAnimation(int refId, bool forceRestart)
{
	if(refId!=_currentAnimationRef && !forceRestart)
	{
		AnimIter iter = _animationMap.find(refId);
		if(iter!=_animationMap.end())
		{
			if(_currentAnimationRef!=-1 && refId!=_currentAnimationRef)
			{
				if(_animations[_currentAnimationIndex].resetOnSwitch)
				{
					_animations[_currentAnimationIndex].curFrame=0;
				}
			}
			_currentAnimationRef = refId;
			_currentAnimationIndex = iter->second;

			if(forceRestart)
			{
				_animations[_currentAnimationIndex].curFrame=0;
			}
			_isPlaying=_animations[_currentAnimationIndex].sprite->getFrameNum()>0;
			_currentAnimationTime=0;
		}
		else
		{
			_isPlaying=false;
			_currentAnimationTime=0;
		}
	}
}

void lbSpriteAnimator::stopAnimation()
{
	if(_currentAnimationIndex>=0)
	{
		_animations[_currentAnimationIndex].curFrame=0;
		_isPlaying=false;
		_currentAnimationTime=0;
	}
}

void lbSpriteAnimator::pauseAnimation()
{
	_isPlaying=false;
	_currentAnimationTime=0;
}

void lbSpriteAnimator::setAnimationListener(lbSpriteAnimationListener* listener)
{
	_listener=listener;
}
