/*
 * lbTextBox.h
 *
 *  Created on: 01-12-2012
 *      Author: Spliter
 */

#pragma once
#include <string>
#include <vector>
#include "UiElement.h"

class FTFont;

class lbTextBox: public UiElement
{
public:

	lbTextBox();
	virtual ~lbTextBox();

	virtual void setFont(FTFont* font);
	virtual void setText(std::string text);//if showAll is false then it will reset the animation and hide all characters

	virtual void draw();
	void setTextColour(float r, float g, float b, float a){_textColour[0]=r;_textColour[1]=g;_textColour[2]=b;_textColour[3]=a;}
protected:
	int _maxCharsPerLine;
	float _textColour[4];
	std::vector<std::string> _textLines;
	FTFont* _font;
};
