/*
 * lbGameContext.h
 *
 *  Created on: 05-04-2013
 *      Author: Spliter
 */

#pragma once
class GameCore;
class GameState;
class lbSceneLayer2D;

struct GameContext
{
	GameCore  *gameCore;
	GameState *gameState;
};
