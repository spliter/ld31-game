/*
 * lbSDLAppDriver.h
 *
 *  Created on: 11-09-2011
 *      Author: Spliter
 */

#pragma once

class SDL_Window;

class lbSDLAppDriver
{
public:
	lbSDLAppDriver();
	~lbSDLAppDriver();

	void clear();
	bool setup(int width, int height, bool fullscreen);

	void handleEvents();

	void beginFrame();
	void finalizeFrame();

	SDL_Window* getWindow(){return _window;}
	int getWindowWidth(){return _windowWidth;}
	int getWindowHeight(){return _windowHeight;}

	void warpMouse(int x, int y);

	unsigned long getEllapsedMillis();

	void wait(unsigned long millis);

private:
	bool _fullscreen;
	int _windowWidth,_windowHeight;
	int _bpp;
	int _stencilBits;
	int _depthBits;
	int _multisampleLevels;

	SDL_Window* _window;
	void* _glContext;
};
