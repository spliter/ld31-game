/*
 * levelUtils.h
 *
 *  Created on: 3 gru 2014
 *      Author: Miko Kuta
 */

#pragma once

#include <game/Constants.h>
#include <string>
#include <vector>

class lbSceneTileMap;
class lbTexture;
struct Map;
class TiXmlElement;

class TileSet
{
public:
	int firstId;
	int tileWidth, tileHeight;
	int width, height;
	int columns, rows;
	std::string textureFname;
	lbTexture* texture;
	std::vector<int> tileFlags;


	TileSet():
		firstId(-1),
		tileWidth(0),
		tileHeight(0),
		width(0), height(0),
		columns(0), rows(0),
		textureFname(""),
		texture(NULL)
	{

	}

	void setup(int _firstId, int _tileWidth, int _tileHeight, int _width, int _height, std::string _textureFname, std::vector<int> &flags)
	{
		firstId = _firstId;
		width = _width;
		height = _height;
		tileWidth = _tileWidth;
		tileHeight = _tileHeight;
		columns = _width / _tileWidth;
		rows = _height / _tileHeight;
		textureFname = _textureFname;
		tileFlags = flags;
		if ((int) tileFlags.size() < width * height)
		{
			tileFlags.resize(width * height, GameConst::TFEmpty);
		}
	}

	void getTileCoord(int tileId, int &retX, int &retY)
	{
		retX = (tileId - firstId) % columns;
		retY = (tileId - firstId) / columns;
	}
};

#define MAX_TILE_SET_NUM 10
struct TilesetList
{
	TilesetList()
	{
		setNumber=0;
	}
	int setNumber;
	TileSet set[MAX_TILE_SET_NUM];
};

int getTilesetUsed(TilesetList& tileSets, int sampleTileId);
TileSet getTileSetFromXmlElement(TiXmlElement* element, std::string levelDir);
lbSceneTileMap* createTileMapFromXmlElement(TiXmlElement* element, TilesetList& tilesets);
Map* createCollisionMapFromXmlElement(TiXmlElement* element, TilesetList& tilesets);
