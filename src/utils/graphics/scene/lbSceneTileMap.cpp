/*
 * lbSceneTileMap.cpp
 *
 *  Created on: 2 mar 2014
 *      Author: spliter
 */

#include <math/lbMatrix3x3.h>
#include <math/util/lbRect.h>
#include <utils/graphics/scene/lbCameraSceneNode2D.h>
#include <utils/graphics/scene/lbSceneTileMap.h>

void lbSceneTileMap::draw(lbCameraSceneNode2D* camera)
{
	glPushMatrix();
	glMultMatrixf(getGLWorldTransform().me);
	lbRectF cameraRect;

	lbVec2f viewport[4];
	camera->getWorldSpaceViewport(viewport);

	for(int i=0;i<4;i++)
	{
		viewport[i] = getWorldInverseTransform()*viewport[i];
	}
	cameraRect.prepareForVertices();
	cameraRect.addVertice(viewport[0]);
	cameraRect.addVertice(viewport[1]);
	cameraRect.addVertice(viewport[2]);
	cameraRect.addVertice(viewport[3]);
	_tileMap.draw(cameraRect);
	glPopMatrix();
}
