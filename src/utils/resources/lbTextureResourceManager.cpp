/*
 * lbTextureManager.cpp
 *
 *  Created on: 25-05-2013
 *      Author: Spliter
 */

#include <utils/debug/debug.h>
//#include <utils/graphics/lbTexture.h>
#include <utils/resources/lbTextureResourceManager.h>

lbTextureResourceManager::lbTextureResourceManager()
{
	createDefaultTexture();
}

lbTextureResourceManager::~lbTextureResourceManager()
{
	delete _defaultTexture;
}

lbTexture* lbTextureResourceManager::loadResourceFromFile(const std::string & fname)
{
	LogEntryPoint();
	return lbTexture::loadTexture(fname);
}

void lbTextureResourceManager::createDefaultTexture()
{
	int texWidth = 8;
	int texHeight = 8;
	int texBpp = 4;
	unsigned char data[texWidth * texHeight * texBpp];
	for(int x = 0; x < texWidth; x++)
		for(int y = 0; y < texHeight; y++)
		{
			int i = (y * texWidth + x);

			int* datai = (int*) data;
			if((y + x) % 2)
			{
				datai[i] = 0xffbf5680;
			}
			else
			{
				datai[i] = 0xff000000;
			}
		}
	_defaultTexture = new lbTexture(texWidth, texHeight, data);
}
