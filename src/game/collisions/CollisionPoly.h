/*
 * CollisionPoly.h
 *
 *  Created on: 7 gru 2013
 *      Author: spliter
 */

#ifndef COLLISIONPOLY_H_
#define COLLISIONPOLY_H_
#include <vector>
#include <math/lbVec2.h>
#include <math/lbMatrix3x3.h>
#include <math/util/lbRect.h>

/**
 * CollisionPoly
 * The points should be in a counter clockwise order.
 *
 * (considering that y is down and x is right)
 *
 * 1<------4
 * V       A
 * 2------>3
 */
class CollisionPoly
{
public:

	/**
	 * getCastRange
	 * returns: nothing;
	 * return params: retMin and retMax return the minimum and maximum cast along the specified axis
	 */
	static void getCastRange(const std::vector<lbVec2f>& verts, const lbVec2f& castAxis, const lbVec2f& castOrigin, float& retMin, float& retMax);

	/**
	 * IsColliding
	 * returns: true if is polygons overlap, false otherwise
	 */
	static bool IsColliding(CollisionPoly& a, CollisionPoly&b);

	/**
	 * GetSeparatingVector
	 * param a: the polygon that will should be moved bu the separating vector
	 * param b: the polygon against which the test is performed, it's assumed to be immovable
	 * param separatingVector: The vector that if applied to a will separate it from b. (alternatively apply negative to b to separate from a)
	 * returns true if polygons overlap, false otherwise
	 *
	 */
	static bool GetSeparatingVector(CollisionPoly& a, CollisionPoly&b, lbVec2f& separatingVector);

	/**
	 * ApplySeparatingAxisTheorem
	 * param a: the polygon that will be cast
	 * param castVector: The vector along which the polygon a will be cast
	 * param b: the polygon against which the test is performed, it's assumed to be immovable
	 * param retCollisionTime: the time or ratio (0,1) at which a cast along castVector will collide b, -1 if a and b are already colliding
	 * param retLastAxis: the axis of the face where the collision occurs (if any)
	 * returns true if polygons collide, false otherwise
	 *
	 */
	static bool CastCollisionPoly(CollisionPoly& a, const lbVec2f& castVector, CollisionPoly&b, float& retCollisionTime, lbVec2f& retLastAxis);


	static bool checkRayCast(lbVec2f start, lbVec2f end, CollisionPoly& poly, float &retIntersectionTime, lbVec2f& retCollisionAxis, lbVec2f& retCollisionPoint);

	/**
	 * Utility functions that might be useful outside of the collision poly itself.
	 */
	static void CalculateBoundingBox(const std::vector<lbVec2f>& convexVerts, lbRectF& retBBox);
	static void CalculateTransformedBoundingBox(const lbMatrix3x3f& mat, const lbRectF& bbox, lbRectF& retTransformedBBox);
	static void CalculateStretchedBoundingBox(const lbVec2f& stretchVector, const lbRectF& bbox, lbRectF& retStretchedBBox);

	CollisionPoly();
	CollisionPoly(const CollisionPoly& other);
	virtual ~CollisionPoly();

	void operator=(const CollisionPoly& other);

	void updatePointsIfNeeded();

	const lbVec2f& getPosition() const
	{
		return _position;
	}

	void setPosition(const lbVec2f& position)
	{
		this->_position = position;
		_isDirty = true;
	}

	float getRotationRad() const
	{
		return _rotation;
	}

	float getRotationDeg() const
	{
		return radToDeg(_rotation);
	}

	void setRotationRad(float rotation)
	{
		this->_rotation = rotation;
		_isDirty = true;
	}

	void setRotationDeg(float rotation)
	{
		this->_rotation = degToRad(rotation);
		_isDirty = true;
	}

	bool isIsDirty() const
	{
		return _isDirty;
	}

	const std::vector<lbVec2f>& getVerts() const
	{
		return _verts;
	}

	const std::vector<lbVec2f>& getConvexVerts() const
	{
		return _convexVerts;
	}

	const std::vector<lbVec2f>& getTransformedConvexVerts() const
	{
		return _transformedConvexVerts;
	}

	const std::vector<lbVec2f>& getTransformedAxes() const
	{
		return _transformedAxes;
	}

	const lbRectF& getBoundingBox() const
	{
		return _bbox;
	}

	const lbRectF& getTransformedBoundingBox() const
	{
		return _transformedBBox;
	}

	void resizeVertsVector(int newSize, const lbVec2f& elem = lbVec2f(0, 0))
	{
		_verts.resize(newSize);
		_isDirty = true;
		_isPointsDirty = true;
	}

	void appendVert(const lbVec2f& vert)
	{
		_verts.push_back(vert);
		_isDirty = true;
		_isPointsDirty = true;
	}

	void setVert(const lbVec2f& vert, int index)
	{
		_verts[index] = vert;
		_isDirty = true;
		_isPointsDirty = true;
	}

	const lbMatrix3x3f& getTransform() const
	{
		return _transform;
	}

	void setParentTransform(const lbMatrix3x3f& newParentTransform)
	{
		_parentTransform = newParentTransform;
		_isDirty = true;
	}

	const lbMatrix3x3f& getParentTransform() const
	{
		return _parentTransform;
	}


	void makeRectangle(float x1, float y1, float x2, float y2);
	void makeCircle(float x, float y, float radius, int vertNum);

private:
	lbVec2f _position;
	float _rotation;

	lbMatrix3x3f _transform;
	lbMatrix3x3f _parentTransform;
	std::vector<lbVec2f> _verts;
	std::vector<lbVec2f> _convexVerts;
	std::vector<lbVec2f> _transformedConvexVerts;
	std::vector<lbVec2f> _transformedAxes;
	lbRectF _bbox;
	lbRectF _transformedBBox;

	bool _isDirty;
	bool _isPointsDirty;
};

#endif /* COLLISIONPOLY_H_ */
