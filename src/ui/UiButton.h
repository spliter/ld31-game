/*
 * UiButton.h
 *
 *  Created on: 24-09-2012
 *      Author: Spliter
 */

#pragma once
#include "UiElement.h"

class lbTexture;

class UiButton:public UiElement
{
public:
	UiButton();
	virtual ~UiButton();
	virtual void draw();

	void setGraphics(lbTexture* pressedTexture,lbTexture* releasedTexture);
private:
	lbTexture* _pressedTexture;
	lbTexture* _releasedTexture;
};
