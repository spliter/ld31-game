/*
 * GameControllerEntity.cpp
 *
 *  Created on: 4 gru 2014
 *      Author: Miko Kuta
 */

#include <game/entities/controllers/GameControllerEntity.h>
#include <game/GameEntityFactoryManager.h>
#include <game/level/GameLevel.h>
#include <game/states/GameState.h>
#include <utils/graphics/scene/lbCameraSceneNode2D.h>


REGISTER_GAME_ENTITY(GameControllerEntity,"game_controller");

GameControllerEntity::GameControllerEntity(GameContext* context):
	GameEntity(context),
	_zoom(1.0f)
{

}


GameControllerEntity::~GameControllerEntity()
{
}

void GameControllerEntity::onSetupFromXmlElement(TiXmlElement* element)
{
	super::onSetupFromXmlElement(element);

	parseFloatProperty(element,"zoom",&_zoom);
}

void GameControllerEntity::onSpawned()
{
	super::onSpawned();
	getGameContext()->gameState->getCurrentLevel()->backgroundCamera.setZoom(_zoom);
	getGameContext()->gameState->getCurrentLevel()->characterCamera.setZoom(_zoom);
	getGameContext()->gameState->getCurrentLevel()->foregroundCamera.setZoom(_zoom);
}

void GameControllerEntity::onUpdate(float dt)
{
}


