/*
 * DebugCameraEntity.h
 *
 *  Created on: 4 gru 2014
 *      Author: Miko Kuta
 */

#pragma once

//#include <game/collisions/CollisionPoly.h>
#include <game/GameEntity.h>
#include <lbTypes.h>
#include <utils/Object.h>
#include <iostream>
#include <list>

class CollisionPoly;
struct GameContext;

class SolidEntity;

class SolidWorld
{
public:
	void add(SolidEntity* ent){SolidEntities.push_back(ent);}
	void remove(SolidEntity* ent){SolidEntities.remove(ent);}
	const std::list<SolidEntity*> getSolids(){return SolidEntities;}
private:
	std::list<SolidEntity*> SolidEntities;
};

class SolidEntity: public GameEntity
{
public:
	typedef std::list<SolidEntity*>::iterator solidIter;
	typedef std::list<SolidEntity*> solidList;

	DEFINE_CLASS(SolidEntity,GameEntity);

	SolidEntity(GameContext* context);
	virtual ~SolidEntity();
//	virtual void onCreated();
//	virtual void onDestroy();
	virtual CollisionPoly** getCollisionPolygons()=0;
	virtual int getCollisionPolyNum()=0;

private:
};

