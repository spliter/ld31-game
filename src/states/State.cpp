/*
 * State.cpp
 *
 *  Created on: 12-09-2012
 *      Author: Spliter
 */

#include <states/State.h>
#include <utils/debug/debug.h>
#include <cstdio>
#include <cstdlib>
#include <string>

//#include <iostream>

State::State() :
	_requestedFunc(None),
	_calledFunc(None),
	_substatesInUseDepth(0),
	_substatesDeleted(false),
	_parentState(NULL),
	_childState(NULL),
	_isStarted(false),
	_isResumed(false),
	_isFinished(false)
{
	LogEntryPoint();
}

State::~State()
{
	LogEntryPoint();
	enterSubstateUseMode();
	SubIter iter = _substates.begin();
	while (iter != _substates.end())
	{
		Substate* substate = (*iter);
		if(substate->isAttached() && substate->isResumed())
		{
			substate->onPause();
		}
		iter = _substates.erase(iter);
		substate->onRemoved(this);
	}
	leaveSubstateUseMode();
}

#define ASSERT_PARENT_MSG(func)\
		if(!(_requestedFunc == _calledFunc)){printf("Assert Failure, %s must call the parent's function %s().",getClass()->getClassName().c_str(),#func);exit(1);}

#define SETUP_PARENT_NEED_FUNC0(func, enumVal)\
	_requestedFunc = enumVal;\
	_calledFunc = State::None;\
	func();\
	ASSERT_PARENT_MSG(func)

#define SETUP_PARENT_NEED_FUNC1(func,arg0, enumVal)\
	_requestedFunc = enumVal;\
	_calledFunc = State::None;\
	func(arg0);\
	ASSERT_PARENT_MSG(func)

#define SETUP_PARENT_NEED_FUNC2(func, arg0, arg1, enumVal)\
	_requestedFunc = enumVal;\
	_calledFunc = State::None;\
	func(arg0,arg1);\
	ASSERT_PARENT_MSG(func)

#define INFORM_PARENT_FUNC_CALLED(enumVal)\
	if(_requestedFunc==enumVal){_calledFunc=enumVal;}

void State::onStart()
{
	LogEntryPoint();
	INFORM_PARENT_FUNC_CALLED(Start);
	_isFinished = false;
	_isStarted = true;
	_isResumed = false;
}

void State::onResume()
{
	LogEntryPoint();
	INFORM_PARENT_FUNC_CALLED(Resume);
	_isResumed = true;
}

void State::onUpdate()
{
	LogEntryPoint();
	INFORM_PARENT_FUNC_CALLED(Update);
	enterSubstateUseMode();
	SubIter iter = _substates.begin();
	while (iter != _substates.end())
	{
		if ((*iter))
		{
			if (!((*iter)->_isResumed))
			{
				(*iter)->onUpdate();
			}
			else if ((*iter)->_requestedState == Substate::Start)
			{
				(*iter)->_requestedState = Substate::None;
				(*iter)->_isResumed = true;
				(*iter)->onResume();
			}
			else if ((*iter)->_requestedState == Substate::Pause)
			{
				(*iter)->_requestedState = Substate::None;
				(*iter)->_isResumed = false;
				(*iter)->onPause();
			}

			if ((*iter)->_isResumed)
			{
				(*iter)->onUpdate();
			}
		}
		iter++;
	}
	leaveSubstateUseMode();
}

void State::onFrameBegin()
{
	LogEntryPoint();
	INFORM_PARENT_FUNC_CALLED(FrameBegin);
	enterSubstateUseMode();
	SubIter iter = _substates.begin();
	while (iter != _substates.end())
	{
		if ((*iter))
		{
			if ((*iter)->_isResumed)
			{
				(*iter)->onFrameBegin();
			}
		}
		iter++;
	}
	leaveSubstateUseMode();
}

void State::onFrameRender()
{
	LogEntryPoint();
	INFORM_PARENT_FUNC_CALLED(FrameRender);
	enterSubstateUseMode();
	SubIter iter = _substates.begin();
	while (iter != _substates.end())
	{
		if ((*iter))
		{
			if ((*iter)->_isResumed)
			{
				(*iter)->onFrameRender();
			}
		}
		iter++;
	}
	leaveSubstateUseMode();
}

void State::onFrameEnd()
{
	LogEntryPoint();
	INFORM_PARENT_FUNC_CALLED(FrameEnd);
	enterSubstateUseMode();
	SubIter iter = _substates.begin();
	while (iter != _substates.end())
	{
		if ((*iter))
		{
			if ((*iter)->_isResumed)
			{
				(*iter)->onFrameEnd();
			}
		}
		iter++;
	}
	leaveSubstateUseMode();
}

void State::onPause()
{
	LogEntryPoint();
	INFORM_PARENT_FUNC_CALLED(Pause);
	_isResumed = false;
	resetInput();
	enterSubstateUseMode();
	SubIter iter = _substates.begin();
	while (iter != _substates.end())
	{
		if ((*iter))
		{
			if ((*iter)->_isResumed)
			{
				(*iter)->onPause();
				(*iter)->_isResumed = false;
				(*iter)->_requestedState = Substate::Start;
			}
		}
		iter++;
	}
	leaveSubstateUseMode();
}

void State::onEnd()
{
	LogEntryPoint();
	INFORM_PARENT_FUNC_CALLED(End);
	_isStarted = false;

	enterSubstateUseMode();
	SubIter iter = _substates.begin();
	while (iter != _substates.end())
	{
		if ((*iter))
		{
			if ((*iter)->_isResumed)
			{
				(*iter)->onPause();
				(*iter)->_isResumed = false;
				(*iter)->_requestedState = Substate::Start;
			}
			(*iter)->onRemoved(this);
			(*iter) = NULL;
		}
		iter++;
	}
	leaveSubstateUseMode();
}

void State::onKeyboardEvent(lbKeyboardEvent *event)
{
	LogEntryPoint();
}

void State::onMouseMotionEvent(lbMouseMotionEvent* event)
{
	LogEntryPoint();
}

void State::onMouseButtonEvent(lbMouseButtonEvent* event)
{
	LogEntryPoint();
}

void State::onMouseWheelEvent(lbMouseWheelEvent* event)
{
	LogEntryPoint();
}

void State::addSubState(Substate* substate)
{
	LogEntryPoint();
	if (substate && !(substate->_owner))
	{
		_substates.push_back(substate);
		substate->_owner = this;
		substate->_isResumed = false;
		substate->_requestedState = Substate::None;
		substate->onAdded(this);
	}
}

void State::removeSubState(Substate* substate)
{
	LogEntryPoint();
	if (substate && substate->_owner == this)
	{
		enterSubstateUseMode();
		if (substate->_isResumed)
		{
			substate->_isResumed = false;
			substate->onPause();
		}
		leaveSubstateUseMode();
		substate->_owner = NULL;
		substate->_requestedState = Substate::None;
		if (_substatesInUseDepth)
		{
			SubIter iter = _substates.begin();
			while (iter != _substates.end())
			{
				if ((*iter) == substate)
				{
					(*iter) = NULL;
					break;
				}
				iter++;
			}
			_substatesDeleted = true;
		}
		else
		{
			_substates.remove(substate);
		}
		substate->onRemoved(this);
	}
}

void State::callOnStart()
{
	LogEntryPoint();
	SETUP_PARENT_NEED_FUNC0(onStart, Start);
}

void State::callOnResume()
{
	LogEntryPoint();
	SETUP_PARENT_NEED_FUNC0(onResume, Resume);
}

void State::callOnFrameBegin()
{
	LogEntryPoint();
	SETUP_PARENT_NEED_FUNC0(onFrameBegin, FrameBegin);
}

void State::callOnUpdate()
{
	LogEntryPoint();
	SETUP_PARENT_NEED_FUNC0(onUpdate, Update);
}

void State::callOnFrameRender()
{
	LogEntryPoint();
	SETUP_PARENT_NEED_FUNC0(onFrameRender, FrameRender);
}

void State::callOnFrameEnd()
{
	LogEntryPoint();
	SETUP_PARENT_NEED_FUNC0(onFrameEnd, FrameEnd);
}

void State::callOnPause()
{
	LogEntryPoint();
	SETUP_PARENT_NEED_FUNC0(onPause, Pause);
}

void State::callOnEnd()
{
	LogEntryPoint();
	SETUP_PARENT_NEED_FUNC0(onEnd, End);
}

void State::callOnKeyboardEvent(lbKeyboardEvent *event)
{
	LogEntryPoint();
	onKeyboardEvent(event);
//	SETUP_PARENT_NEED_FUNC1(onKeyboardEvent, event, Keyboard);
	enterSubstateUseMode();
	SubIter iter = _substates.begin();
	while (iter != _substates.end())
	{
		if ((*iter))
		{
			if ((*iter)->_isResumed)
			{
				(*iter)->onKeyboardEvent(event);
			}
		}
		iter++;
	}
	leaveSubstateUseMode();
}

void State::callOnMouseMotionEvent(lbMouseMotionEvent* event)
{
	LogEntryPoint();
	onMouseMotionEvent(event);
//	SETUP_PARENT_NEED_FUNC1(onMouseMotionEvent, event, MouseMotion);
	enterSubstateUseMode();
	SubIter iter = _substates.begin();
	while (iter != _substates.end())
	{
		if ((*iter))
		{
			if ((*iter)->_isResumed)
			{
				(*iter)->onMouseMotionEvent(event);
			}
		}
		iter++;
	}
	leaveSubstateUseMode();
}

void State::callOnMouseButtonEvent(lbMouseButtonEvent* event)
{
	LogEntryPoint();
	onMouseButtonEvent(event);
//	SETUP_PARENT_NEED_FUNC1(onMouseButtonEvent, event, MouseButton);
	enterSubstateUseMode();
	SubIter iter = _substates.begin();
	while (iter != _substates.end())
	{
		if ((*iter))
		{
			if ((*iter)->_isResumed)
			{
				(*iter)->onMouseButtonEvent(event);
			}
		}
		iter++;
	}
	leaveSubstateUseMode();
}

void State::callOnMouseWheelEvent(lbMouseWheelEvent* event)
{
	LogEntryPoint();
	onMouseWheelEvent(event);
//	SETUP_PARENT_NEED_FUNC1(onMouseWheelEvent, event, MouseWheel);
	enterSubstateUseMode();
	SubIter iter = _substates.begin();
	while (iter != _substates.end())
	{
		if ((*iter))
		{
			if ((*iter)->_isResumed)
			{
				(*iter)->onMouseWheelEvent(event);
			}
		}
		iter++;
	}
	leaveSubstateUseMode();
}

void State::enterSubstateUseMode()
{
	LogEntryPoint();
	_substatesInUseDepth++;
}

void State::leaveSubstateUseMode()
{
	LogEntryPoint();
	_substatesInUseDepth--;
	if (_substatesInUseDepth <= 0)
	{
		_substatesInUseDepth = 0;
		if (_substatesDeleted)
		{
			SubIter iter = _substates.begin();
			while (iter != _substates.end())
			{
				if ((*iter) == NULL)
				{
					iter = _substates.erase(iter);
				}
				else
				{
					iter++;
				}
			}
			_substatesDeleted = false;
		}
	}
}
