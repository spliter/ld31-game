/*
 * lbCameraSceneNode2D.cpp
 *
 *  Created on: 2 mar 2014
 *      Author: spliter
 */

#include <Glee.h>
#include <math/lbMatrix3x3.h>
#include <math/util/lbRect.h>
#include <utils/graphics/scene/lbCameraSceneNode2D.h>

lbCameraSceneNode2D::lbCameraSceneNode2D() :
		_windowViewport(0, 0, 0, 0), _nearClip(-1), _farClip(1),_zoom(1.0f)
{
}

lbCameraSceneNode2D::~lbCameraSceneNode2D()
{
}

void lbCameraSceneNode2D::pushCamera()
{

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glPushAttrib (GL_VIEWPORT_BIT);

	useCamera();
}

void lbCameraSceneNode2D::popCamera()
{
	glMatrixMode(GL_PROJECTION);
	glPopAttrib();
	glPopMatrix();
	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();
}

void lbCameraSceneNode2D::useCamera()
{

	glViewport(_windowViewport.x1, _windowViewport.y1, _windowViewport.x2 - _windowViewport.x1, _windowViewport.y2 - _windowViewport.y1);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(_localViewport.x1, _localViewport.x2, _localViewport.y2, _localViewport.y1, _nearClip, _farClip);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glMultMatrixf(getGLWorldInverseTransform().me);
}

void lbCameraSceneNode2D::getWorldSpaceViewport(lbVec2f frustrum[4])
{
	frustrum[0] = getWorldTransform() * lbVec2f(_windowViewport.x1, _windowViewport.y1);
	frustrum[1] = getWorldTransform() * lbVec2f(_windowViewport.x1, _windowViewport.y2);
	frustrum[2] = getWorldTransform() * lbVec2f(_windowViewport.x2, _windowViewport.y2);
	frustrum[3] = getWorldTransform() * lbVec2f(_windowViewport.x2, _windowViewport.y1);
}

void lbCameraSceneNode2D::setViewportAnchor(lbVec2f anchorRatio)
{
	_vpAnchor.set(anchorRatio);
	updateLocalViewport();
}

void lbCameraSceneNode2D::setViewport(lbRectI viewport, float nearClip, float farClip)
{
	_windowViewport = viewport;
	_nearClip = nearClip;
	_farClip = farClip;
	updateLocalViewport();
	markTransformDirty();
}

void lbCameraSceneNode2D::setZoom(float zoom)
{
	_zoom = zoom;
	updateLocalViewport();
}

lbVec2f lbCameraSceneNode2D::screenToWorldSpace(lbVec2f pos)
{
	pos-=lbVec2f(_windowViewport.getWidth(),_windowViewport.getHeight())* _vpAnchor;
	float scale = 0.0f;
	if(_zoom>=FLT_EPSILON)
	{
		scale = 1.0f/_zoom;
	}
	pos*=scale;
	return localToWorldSpace(pos);
}

lbVec2f lbCameraSceneNode2D::worldToScreenSpace(lbVec2f pos)
{
	pos = worldToLocalSpace(pos);
	pos*=_zoom;

	pos+=lbVec2f(_windowViewport.getWidth(),_windowViewport.getHeight())* _vpAnchor;
	return pos;
}

void lbCameraSceneNode2D::updateLocalViewport()
{
	float scale = 0.0f;
	if(_zoom>=FLT_EPSILON)
	{
		scale = 1.0f/_zoom;
	}
	_localViewport = toRectF(_windowViewport)
			.move(_windowViewport.getWidth()* -_vpAnchor.x, _windowViewport.getHeight() * -_vpAnchor.y)
			.scale(scale,scale);
}
