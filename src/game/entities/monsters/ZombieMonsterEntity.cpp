/*
 * ZombieMonsterEntity.cpp
 *
 *  Created on: 4 gru 2014
 *      Author: Miko Kuta
 */

#include <game/entities/monsters/ZombieMonsterEntity.h>
#include <game/entities/PlayerEntity.h>
#include <game/GameEntity.h>
#include <game/GameEntityFactoryManager.h>
#include <global.h>
#include <utils/debug/debug.h>
#include <utils/resources/lbDefaultResourceManagers.h>
#include <utils/resources/lbResourceManager.h>
#include <utils/resources/lbSpriteResourceManager.h>
#include <cfloat>

static float MovementSpeed = 60.0f; //units/second

REGISTER_GAME_ENTITY(ZombieMonsterEntity, "zombie_monster");

ZombieMonsterEntity::ZombieMonsterEntity(GameContext* context) :
		MonsterEntity(context)
{
}

ZombieMonsterEntity::~ZombieMonsterEntity()
{
}

void ZombieMonsterEntity::onCreated()
{
	LogEntryPoint();
	super::onCreated();
	lbSprite* sprite = lbDefaultResourceManagers::instance().getSpriteManager()->getResource(buildSpriteFilename("zombie_monster"));
	setBaseSprite(sprite);
}

void ZombieMonsterEntity::onSpawned()
{
	LogEntryPoint();
	super::onSpawned();
	setZOrder(-1);
	setShoveStunDuration(0.01);
	setShoveDecay(3);
}

void ZombieMonsterEntity::onUpdate(float dt)
{
	LogEntryPoint();
	super::onUpdate(dt);

	PlayerEntity* player = getPlayer();

	lbVec2f movement = 0;
	if (player)
	{
		lbVec2f dif = player->getPosition() - getPosition();
		dif.normalize();
		movement = dif * MovementSpeed;
		if (movement.squareLength() > FLT_EPSILON)
		{
			float dir = movement.directionDeg()+90;
			setRotationDeg(dir);
		}
	}
	if(isStunnedFromShove())
	{
		movement.set(0,0);
	}
	setPosition(getPosition() + movement * dt + _shoveForce * dt);

	checkCollisions();
}

void ZombieMonsterEntity::draw(lbCameraSceneNode2D* camera)
{
	LogEntryPoint();
	super::draw(camera);
}

void ZombieMonsterEntity::onHitByBullet(lbVec2f position, lbVec2f speed, int damage)
{
	super::onHitByBullet(position,speed,damage);
	shove(speed*0.0005f*damage);
}
