/*
 * lbTileMap.cpp
 *
 *  Created on: 06-11-2012
 *      Author: Spliter
 */


#include <Glee.h>
#include <stddef.h>
#include <utils/graphics/lbTexture.h>
#include <utils/graphics/lbTileMap.h>
#include <utils/graphics/lbTileSet.h>
#include <algorithm>





lbTileMap* lbTileMap::createFromXmlElement(TiXmlElement* element)
{
	return NULL;
}

lbTileMap::lbTileMap() :
		_tileMap(NULL),
		_texture(NULL),
		_vertexData(NULL),
		_vbo(0),
		_width(-1),
		_height(-1),
		_tileWidth(32),
		_tileHeight(32),
		_textureTileColumns(1),
		_textureTileRows(1),
		_numVertices(0),
		_isDirty(true),
		_prevStartX(0),
		_prevStartY(0),
		_prevVisibleRows(0),
		_prevVisibleColumns(0)
{
	resize(0, 0);
}

lbTileMap::lbTileMap(int width, int height, float tileWidth, float tileHeight) :
		_tileMap(NULL),
		_texture(NULL),
		_vertexData(NULL),
		_vbo(0),
		_width(-1),
		_height(-1),
		_tileWidth(tileWidth),
		_tileHeight(tileHeight),
		_textureTileColumns(1),
		_textureTileRows(1),
		_numVertices(0),
		_isDirty(true),
		_prevStartX(0),
		_prevStartY(0),
		_prevVisibleRows(0),
		_prevVisibleColumns(0)
{
	resize(width, height);
}

lbTileMap::~lbTileMap()
{
	if (_vbo != 0)
	{
		glDeleteBuffers(1, &_vbo);
		delete[] _vertexData;
		_vbo = 0;
	}
	if (_tileMap)
	{
		delete[] _tileMap;
	}
}

void lbTileMap::setTexture(lbTexture* texture, int tileColumns, int tileRows)
{
	_texture = texture;
	_textureTileColumns = tileColumns;
	_textureTileRows = tileRows;
}

void lbTileMap::setFromTileSet(lbTileSet *info)
{
	_texture = info->getTexture();
	_textureTileColumns = info->getNumColumns();
	_textureTileRows = info->getNumRows();
}


void lbTileMap::draw(lbRectF camera)
{
	int startX = camera.x1 / _tileWidth;
	int startY = camera.y1 / _tileHeight;

	int columns = ((camera.x2 - camera.x1) / _tileWidth) + 2;
	int rows = ((camera.y2 - camera.y1) / _tileHeight) + 2;

	int usedVerts = setupVertexBuffer(startX, startY, columns, rows);

	glBindBuffer(GL_ARRAY_BUFFER, _vbo);
	glEnableClientState(GL_VERTEX_ARRAY);
	glVertexPointer(3, GL_FLOAT, sizeof(VertexData), (void*) (offsetof(VertexData, x)));
	glEnableClientState(GL_TEXTURE_COORD_ARRAY);
	glTexCoordPointer(2, GL_FLOAT, sizeof(VertexData), (void*) (offsetof(VertexData, tx)));
	glEnableClientState(GL_COLOR_ARRAY);
	glColorPointer(4, GL_UNSIGNED_BYTE, sizeof(VertexData), (void*) (offsetof(VertexData, color)));

	if (_texture)
	{
		_texture->bind();
	}
	else
	{
		lbTexture::unbind();
	}
	glPushMatrix();
	//		glTranslatef(xOffset,yOffset,0);
	glScalef(_tileWidth, _tileHeight, 1);
	glPushAttrib(GL_TEXTURE_BIT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glDrawArrays(GL_QUADS, 0, usedVerts);
	glPopAttrib();
	glPopMatrix();

	lbTexture::unbind();
	glBindBuffer(GL_ARRAY_BUFFER, 0);
}

void lbTileMap::resize(int width, int height)
{
	if (_width != width || _height != height)
	{

		delete[] _vertexData;
		delete[] _tileMap;

		_width = width;
		_height = height;
		_numVertices = width * height * 4;
		_vertexData = new VertexData[_numVertices];
		_tileMap = new Tile[width*height];
		if(_vbo==0)
		{
			glGenBuffers(1, &_vbo);
		}
		glBindBuffer(GL_ARRAY_BUFFER, _vbo);
		glBufferData(GL_ARRAY_BUFFER, _numVertices * sizeof(VertexData), _vertexData, GL_DYNAMIC_DRAW);
	}
}

void lbTileMap::setTileSize(float tileWidth, float tileHeight)
{
	_tileWidth = tileWidth;
	_tileHeight = tileHeight;
}

int lbTileMap::setupVertexBuffer(int startX, int startY, int visibleColumns, int visibleRows)
{

	int i = 0;
	int tid = 0;

	int tileWidth = 1;
	int tileHeight = 1;

	if (_texture)
	{
		tileWidth = _texture->getWidth() / _textureTileColumns;
		tileHeight = _texture->getHeight() / _textureTileRows;
	}

	float tileScaledWidth = 1.0f / _textureTileColumns;
	float tileScaledHeight = 1.0f / _textureTileRows;

	float tileOffsetX = 0;
	float tileOffsetY = 0;
	if (_texture)
	{
		_texture->remapNormalCoords(tileOffsetX, tileOffsetY, tileOffsetX, tileOffsetY);
		_texture->remapNormalCoords(tileScaledWidth, tileScaledHeight, tileScaledWidth, tileScaledHeight);
	}

	float pixelOffsetX = tileScaledWidth / (float) tileWidth / 8.0f;
	float pixelOffsetY = tileScaledHeight / (float) tileHeight / 8.0f;

	int maxX = std::min<int>(startX + visibleColumns, _width);
	int maxY = std::min<int>(startY + visibleRows, _height);
	int usedVertices = (maxX-startX) * (maxY-startY) * 4;

	startX = std::max<int>(startX, 0);
	startY = std::max<int>(startY, 0);

	for (int y = startY; y < maxY; y++)
	{
		for (int x = startX; x < maxX; x++)
		{
			tid = y * _width + x;
			int tile = _tileMap[tid].renderInfo;
			int tileX = getTileTextureX(tile);
			int tileY = getTileTextureY(tile);
			float tx = tileX * tileScaledWidth + tileOffsetX;
			float ty = tileY * tileScaledHeight + tileOffsetY;
			lbRectF tc(tx, ty, tx + tileScaledWidth, ty + tileScaledHeight);
			unsigned int tileColor = _tileMap[tid].color;

			if (tileX != 255 || tileY != 255)
			{
				_vertexData[i].x = x;
				_vertexData[i].y = y;
				_vertexData[i].z = 0;
				_vertexData[i].tx = tx + pixelOffsetX;
				_vertexData[i].ty = ty + pixelOffsetY;
				_vertexData[i].color = tileColor;

				i++;
				_vertexData[i].x = x;
				_vertexData[i].y = y + 1;
				_vertexData[i].z = 0;
				_vertexData[i].tx = tx + pixelOffsetX;
				_vertexData[i].ty = ty + tileScaledHeight - pixelOffsetY;
				_vertexData[i].color = tileColor;

				i++;
				_vertexData[i].x = x + 1;
				_vertexData[i].y = y + 1;
				_vertexData[i].z = 0;
				_vertexData[i].tx = tx + tileScaledWidth - pixelOffsetX;
				_vertexData[i].ty = ty + tileScaledHeight - pixelOffsetY;
				_vertexData[i].color = tileColor;

				i++;
				_vertexData[i].x = x + 1;
				_vertexData[i].y = y;
				_vertexData[i].z = 0;
				_vertexData[i].tx = tx + tileScaledWidth - pixelOffsetX;
				_vertexData[i].ty = ty + pixelOffsetY;
				_vertexData[i].color = tileColor;

				i++;
			}
		}
	}
	glBindBuffer(GL_ARRAY_BUFFER, _vbo);
	glBufferSubData(GL_ARRAY_BUFFER, 0, usedVertices * sizeof(VertexData), _vertexData);
//	glBufferData(GL_ARRAY_BUFFER, usedVertices * sizeof(VertexData), _vertexData, GL_STREAM_DRAW);
//	std::cout<<"drawing from "<<startX<<" , "<<startY<<" visible: "<<visibleColumns<<"x"<<visibleRows<<" with "<<usedVertices<<" verts"<<std::endl;
//	std::cout<<"tieMapInfo: "<<_width<<"x"<<_height<<" tiles: "<<_tileWidth<<"x"<<_tileHeight<<" texTiles: "<<_textureTileColumns<<"x"<<_textureTileRows<<std::endl;
	return usedVertices;
}
