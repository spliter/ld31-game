/*
 * lbTextBox.cpp
 *
 *  Created on: 01-12-2012
 *      Author: Spliter
 */

#include <FTGL/ftgl.h>
#include <FTGL/FTPoint.h>
#include <Glee.h>
#include <math/lbVec2.h>
#include <ui/lbTextBox.h>
#include <utils/graphics/lbTexture.h>


lbTextBox::lbTextBox():
	_maxCharsPerLine(40),
	_font(NULL)
{
	_textColour[0]=1.0f;
	_textColour[1]=1.0f;
	_textColour[2]=1.0f;
	_textColour[3]=1.0f;
}

lbTextBox::~lbTextBox()
{
}


void lbTextBox::setFont(FTFont* font)
{
	_font=font;
}

void lbTextBox::setText(std::string text)
{
	_textLines.clear();
	if(text.size()>0)
	{
		int i=0;
		int lineLen=0;
		int lineStart=0;
		char c=text.at(0);
		int lastSpace=0;
		while(c)
		{
			if(c=='\n')
			{
				if(lineLen!=0)
				{
					_textLines.push_back(std::string(text.c_str(),lineStart,lineLen));
				}
				lastSpace=i;
				lineStart=i+1;
				lineLen=0;
			}
			else if (lineLen==_maxCharsPerLine && _maxCharsPerLine!=-1)
			{
				if((i-lastSpace)<_maxCharsPerLine && lastSpace>lineStart)
				{
					int len = (lastSpace+1)-lineStart;
					_textLines.push_back(std::string(text.c_str(),lineStart,len));
					lineStart=lastSpace+1;
					lineLen=i-lastSpace;
				}
				else
				{
					_textLines.push_back(std::string(text.c_str(),lineStart,lineLen));
					lastSpace=i;
					lineStart=i+1;
					lineLen=0;
				}
			}
			else
			{
				if(c==' ')
				{
					lastSpace=i;
				}
				lineLen++;
			}
			i++;
			c=text.c_str()[i];
		}
		if(lineLen!=0)
		{
			_textLines.push_back(std::string(text.c_str(),lineStart,lineLen));
		}
	}
}

void lbTextBox::draw()
{
	lbTexture::unbind();
//	glColor3f(0.3f, 0.3f, 0.0f);//glColor3f(1.0f, 0.0f, 0.0f);
//	lbRectF rect = getGlobalBoundingRect();
//	drawRect(rect.x1, rect.y1, rect.x2, rect.y2);

	glColor4fv(_textColour);
	lbVec2f pos = getPosition();
	glPushMatrix();
	glTranslatef(pos.x,pos.y,0);
	glScalef(1,-1,1);
	if(_font && !_font->Error())
	{
		for(int i = 0; i < (int)_textLines.size(); i++)
		{
			_font->Render(_textLines[i].c_str(), -1, FTPoint(0, (-i - 1) * _font->LineHeight(), 0));
		}
	}
	glPopMatrix();
}
