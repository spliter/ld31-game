/*
 * State.h
 *
 *  Created on: 12-09-2012
 *      Author: Spliter
 */

#pragma once

#include <states/StateManager.h>
#include <states/Substate.h>
#include <utils/Object.h>
#include "../events/input/lbInputEvents.h"
#include <list>

class Substate;

class State: public Object, public lbInputListener
{
	DEFINE_CLASS(State,Object)
public:
	State();
	virtual ~State();

	virtual void onStart();//after the state has been created and right before it's run for the first time, Note: if another state has been started before this one, we still call onStart, but we don't call onResume
	virtual void onResume();//when resuming from another state (note: state is not deleted) or when starting for the first time with NULL as argument
	virtual void onUpdate();
	virtual void onFrameBegin();
	virtual void onFrameRender();
	virtual void onFrameEnd();
	virtual void onPause();//when either going to another state or exiting closing state
	virtual void onEnd();//when closing this state, right before disconnecting, state can be deleted after this function is called, if the state is added again, we'll call onStart()

	static void startState(State* state){getStateManager().startState(state);}
	void finish(){_isResumed = false;}

	virtual void onKeyboardEvent(lbKeyboardEvent *event);
	virtual void onMouseMotionEvent(lbMouseMotionEvent* event);
	virtual void onMouseButtonEvent(lbMouseButtonEvent* event);
	virtual void onMouseWheelEvent(lbMouseWheelEvent* event);

	//Note: State will not delete substates, this will be left to whoever created them

	//States can be added and removed as many times as the user wants
	void addSubState(Substate* substate);
	void removeSubState(Substate* substate);

	virtual void resetInput(){}

	State* getChildState(){return _childState;}
	State* getParentState(){return _parentState;}

	bool isStarted(){return _isStarted;}
	bool isResumed(){return _isResumed;}
	bool isPaused(){return !_isResumed;}
	bool isFinished(){return !_isFinished;}
private:
	friend class StateManager;

	void callOnStart();
	void callOnResume();
	void callOnUpdate();
	void callOnFrameBegin();
	void callOnFrameRender();
	void callOnFrameEnd();
	void callOnPause();
	void callOnEnd();

	void callOnKeyboardEvent(lbKeyboardEvent *event);
	void callOnMouseMotionEvent(lbMouseMotionEvent* event);
	void callOnMouseButtonEvent(lbMouseButtonEvent* event);
	void callOnMouseWheelEvent(lbMouseWheelEvent* event);

	void enterSubstateUseMode();
	void leaveSubstateUseMode();

	enum CalledFunc
	{
		None,
		Start,Resume,
		Update,
		FrameBegin,FrameRender,FrameEnd,
		Pause,End
	};

	typedef std::list<Substate*>::iterator SubIter;
	CalledFunc _requestedFunc;
	CalledFunc _calledFunc;

	std::list<Substate*> _substates;
	int _substatesInUseDepth;
	bool _substatesDeleted;
	State* _parentState;
	State* _childState;
	bool _isStarted;
	bool _isResumed;
	bool _isFinished;
};
