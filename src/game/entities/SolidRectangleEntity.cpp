/*
 * SolidRectangleEntity.cpp
 *
 *  Created on: 4 gru 2014
 *      Author: Miko Kuta
 */

#include <game/Constants.h>
#include <game/entities/SolidRectangleEntity.h>
#include <game/GameEntity.h>
#include <game/GameEntityFactoryManager.h>
#include <game/level/GameLevel.h>
#include <Glee.h>
#include <utils/file/TinyXml/tinyxml.h>
#include <utils/graphics/lbTexture.h>


REGISTER_GAME_ENTITY(SolidRectangleEntity,"collision_rect");

SolidRectangleEntity::SolidRectangleEntity(GameContext* context):
	SolidEntity(context),
	_width(32),
	_height(32)
{
	_colliders[0]=&_collider;
}


SolidRectangleEntity::~SolidRectangleEntity()
{
}

void SolidRectangleEntity::onSetupFromXmlElement(TiXmlElement* element)
{
	super::onSetupFromXmlElement(element);

	if(element->QueryFloatAttribute("width", &_width)==TIXML_SUCCESS)
	{
		_width*=(float)TILE_SIZE/(float)TILE_IMPORT_SIZE;
	}
	if(element->QueryFloatAttribute("height", &_height)==TIXML_SUCCESS)
	{
		_height*=(float)TILE_SIZE/(float)TILE_IMPORT_SIZE;
	}
}

void SolidRectangleEntity::onSpawned()
{
	super::onSpawned();
	_collider.makeRectangle(0,0,_width,_height);
	GameLevel* level = getLevel();
	level->solidGeometryWorld.add(this);
}

void SolidRectangleEntity::onDestroy()
{
	GameLevel* level = getLevel();
	level->solidGeometryWorld.remove(this);
	super::onDestroy();
}

void SolidRectangleEntity::draw(lbCameraSceneNode2D* camera)
{
	//temp
	lbTexture::unbind();
	_collider.updatePointsIfNeeded();
	glColor4f(1.0f,0.0f,0.0f,0.5f);
	glBegin(GL_TRIANGLE_FAN);
	glVertex2fv(_collider.getTransformedConvexVerts()[0].me);
	glVertex2fv(_collider.getTransformedConvexVerts()[1].me);
	glVertex2fv(_collider.getTransformedConvexVerts()[2].me);
	glVertex2fv(_collider.getTransformedConvexVerts()[3].me);
	glEnd();
}

void SolidRectangleEntity::markTransformDirty()
{
	_collider.setPosition(getPosition());
	_collider.setRotationDeg(getRotationDeg());
}
