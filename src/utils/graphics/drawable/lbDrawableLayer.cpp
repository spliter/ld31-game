/*
 * lbDrawableLayer.cpp
 *
 *  Created on: 17-10-2012
 *      Author: Spliter
 */

#include "lbDrawableLayer.h"
#include "lbDrawable.h"
#include <algorithm>
#include <iostream>

lbDrawableLayer::lbDrawableLayer()
{
}

lbDrawableLayer::~lbDrawableLayer()
{
	std::list<lbDrawable*>::iterator it = _drawables.begin();
	while(it != _drawables.end())
	{
		(*it)->_layer=NULL;
		it++;
	}
}

void lbDrawableLayer::addDrawable(lbDrawable* drawable)
{
	if(drawable)
	{
		if(drawable->_layer)
		{
			drawable->_layer->removeDrawable(drawable);
		}
		drawable->_layer=this;
		_drawables.push_back(drawable);
	}
}

void lbDrawableLayer::removeDrawable(lbDrawable* drawable)
{
	if(drawable->_layer == this)
	{
		drawable->_layer = NULL;
		_drawables.remove(drawable);
	}
}

void lbDrawableLayer::clearDrawables()
{
	std::list<lbDrawable*>::iterator it = _drawables.begin();
	while(it != _drawables.end())
	{
		(*it)->_layer=NULL;
		it++;
	}
	_drawables.clear();
}

void lbDrawableLayer::draw(lbRectF camera)
{
	//sort
//	std::sort(_drawables.begin(), _drawables.end(), drawableDepthComparison);
	_drawables.sort(drawableDepthComparison);
	std::list<lbDrawable*>::iterator it = _drawables.begin();

	while(it != _drawables.end())
	{
		if((*it)->isVisible())// && (*it)->getRenderArea().isIntersecting(camera))
		{
			(*it)->draw(camera);
		}
		it++;
	}
}
