/*
 * lbTextureManager.h
 *
 *  Created on: 25-05-2013
 *      Author: Spliter
 */

#pragma once

#include <utils/graphics/lbSprite.h>
#include <utils/graphics/lbTileSet.h>
#include <utils/resources/lbResourceManager.h>
#include <string>

class lbSprite;
class lbTexture;
class lbTextureResourceManager;

class lbTileSetResourceManager: public lbResourceManager<lbTileSet>
{
public:
	static lbTileSetResourceManager& getInstance();
	lbTileSetResourceManager(lbTextureResourceManager* textureManager);
	virtual ~lbTileSetResourceManager();


	virtual lbTileSet* getDefaultResource(){return _defaultTileSet;}
	virtual lbTileSet* loadResourceFromFile(const std::string & fname);
	virtual void freeResource(lbTileSet* resource){delete resource;}
private:
	void createDefaultTileSet();

	lbTexture* _defaultTexture;
	lbTileSet* _defaultTileSet;

	lbTextureResourceManager* _textureManager;
};
