/*
 * lbColor.h
 *
 *  Created on: 8 sty 2014
 *      Author: spliter
 */

#pragma once

#include <math/lbMath.h>
#include <ostream>

class lbColor
{
public:
	union
	{
		float me[4];
		struct
		{
			float r;
			float g;
			float b;
			float a;
		};
	};

	lbColor()
	{
		r = g = b = a = 1;
	}

	lbColor(const lbColor& other)
	{
		r = other.r;
		g = other.g;
		b = other.b;
		a = other.a;
	}

	lbColor(float red, float green, float blue, float alpha)
	{
		set(red, green, blue, alpha);
	}

	lbColor(int red, int green, int blue, int alpha)
	{
		seti(red, green, blue, alpha);
	}

	lbColor(int rgba)
	{
		setRGBA(rgba);
	}

	lbColor& set(float red, float green, float blue, float alpha)
	{
		r = red;
		g = green;
		b = blue;
		a = alpha;
		return *this;
	}

	lbColor& seti(int red, int green, int blue, int alpha)
	{
		r = red / 255.0f;
		g = green / 255.0f;
		b = blue / 255.0f;
		a = alpha / 255.0f;
		return *this;
	}

	lbColor& setRGBA(int rgba)
	{
		int red = (rgba >> 24) & 0xff;
		int green = (rgba >> 16) & 0xff;
		int blue = (rgba >> 8) & 0xff;
		int alpha = (rgba) & 0xff;
		seti(red, green, blue, alpha);
		return *this;
	}

	lbColor& setARGB(int argb)
	{
		int alpha = (argb >> 24) & 0xff;
		int red = (argb >> 16) & 0xff;
		int green = (argb >> 8) & 0xff;
		int blue = (argb) & 0xff;
		seti(red, green, blue, alpha);
		return *this;
	}

	lbColor& setHSV(float hue, float saturation, float value)
	{
		return *this;
	}

	void getHSV(float& hue, float& saturation, float& value) const
	{
		float h = 0, s = 0, v = 0;

		float rgbMin, rgbMax;

		float sr, sg, sb;

		sr = fmodf(r, 1);
		sg = fmodf(g, 1);
		sb = fmodf(b, 1);

		//value

		rgbMax = maxf(sr, maxf(sg, sb));
		v = rgbMax;

		if (v == 0)
		{
			hue = 0;
			saturation = 0;
			value = 0;
			return;
		}

		sr = sr / v;
		sg = sg / v;
		sb = sb / v;
		rgbMin = minf(sr, minf(sg, sb));
		rgbMax = maxf(sr, maxf(sg, sb));

		//saturation

		s = rgbMax - rgbMin;

		if (s == 0)
		{
			value = v;
			saturation = s;
			hue = 0;
			return;
		}

		//hue

		sr = (sr - rgbMin) / s;
		sg = (sg - rgbMin) / s;
		sb = (sb - rgbMin) / s;
		rgbMin = minf(sr, minf(sg, sb));
		rgbMax = maxf(sr, maxf(sg, sb));

		if(rgbMax==sr)
		{
			h = 0.0f + 60.0f*(sg-sb);
		}
		else if(rgbMax==sg)
		{
			h = 120.0f + 60.0f*(sb-sr);
		}
		else
		{
			h = 240.0f + 60.0f*(sr-sg);
		}

		if(h<0)
		{
			h+=360;
		}

		hue = h;
		saturation = s;
		value = v;
	}

	float getComponent(int i) const
	{
		return me[i];
	}

	int getComponenti(int i) const
	{
		return (int) (me[i] * 255);
	}

	int getRGBA() const
	{
		int color = 0;
		color |= (getComponenti(0) & 0xff) << 24;
		color |= (getComponenti(1) & 0xff) << 16;
		color |= (getComponenti(2) & 0xff) << 8;
		color |= (getComponenti(3) & 0xff);
		return color;
	}

	int getARGB()
	{
		int color = 0;
		color |= (getComponenti(3) & 0xff) << 24;
		color |= (getComponenti(0) & 0xff) << 16;
		color |= (getComponenti(1) & 0xff) << 8;
		color |= (getComponenti(2) & 0xff);
		return color;
	}

	lbColor& operator=(const lbColor& other)
	{
		r = other.r;
		g = other.g;
		b = other.b;
		a = other.a;
		return *this;
	}

	lbColor& makeGrayscale()
	{
		float lum = r * 0.2989f + g * 0.5870f + b * 0.1140f;
		r = lum;
		g = lum;
		b = lum;
		return *this;
	}

	lbColor getGrayscale() const
	{
		float lum = r * 0.2989f + g * 0.5870f + b * 0.1140f;
		return lbColor(lum, lum, lum, a);
	}
};

inline std::ostream &operator<<(std::ostream &stream, const lbColor &col)
{
	stream<<"colorRGBA("<<col.r<<" , "<<col.g<<" , "<<col.b<<" , "<<col.a<<")";
	return stream;
}
