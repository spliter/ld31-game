#ifndef __GLVIEWPORT_H__
#define __GLVIEWPORT_H__
struct Viewport
{
	int x;
	int y;
	int w;
	int h;
	float fov;
	float nearClip;
	float farClip;
	Viewport()
	{
		x=y=0;
		w=h=2;
		fov=0;
		nearClip=-1;
		farClip=1;
	}

	void set(int _x, int _y, int  _width, int _height, float _fov, float _near, float _far)
	{
		x=_x;
		y=_y;
		w=_width;
		h=_height;
		fov=_fov;
		nearClip=_near;
		farClip=_far;
	}
};

void setViewport(Viewport &vp);
void pushViewport(Viewport &vp);
void popViewport();
#endif
