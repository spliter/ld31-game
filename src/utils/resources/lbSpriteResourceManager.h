/*
 * lbSpriteResourceManager.h
 *
 *  Created on: 25-05-2013
 *      Author: Spliter
 */

#pragma once

#include <utils/graphics/lbSprite.h>
#include <utils/resources/lbResourceManager.h>
#include <string>

class lbTexture;
class lbTextureResourceManager;

class lbSpriteResourceManager: public lbResourceManager<lbSprite>
{
public:
	static lbSpriteResourceManager& getInstance();
	lbSpriteResourceManager(lbTextureResourceManager* textureManager);
	virtual ~lbSpriteResourceManager();

	virtual lbSprite* getDefaultResource(){return _defaultSprite;}
	virtual lbSprite* loadResourceFromFile(const std::string & fname);
	virtual void freeResource(lbSprite* resource){delete resource;}

private:
	void createDefaultSprite();
	lbTexture* _defaultTexture;
	lbSprite* _defaultSprite;
	lbTextureResourceManager* _textureManager;
};
