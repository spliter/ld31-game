/*
 * lb3DMathUtils.h
 *
 *  Created on: 04-01-2013
 *      Author: Spliter
 */

#pragma once
#include "lbVec3.h"
lbVec3f castRayFromCamera(lbVec3f camPos, lbVec3f camNormal, lbVec3f camUp, float camFov, float viewportWidth,float viewportHeight, float x, float y);
float intersectRayWithPlane(lbVec3f rayStart, lbVec3f rayNormal, lbVec3f planeNormal, float planeDistance);

