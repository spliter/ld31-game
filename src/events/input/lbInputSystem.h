/*
 * lbInputSystem.h
 *
 *  Created on: 05-09-2012
 *      Author: Spliter
 */

#pragma once

#include <events/lbGameEvent.h>
#include <lbTypes.h>

union SDL_Event;
struct SDL_KeyboardEvent;
struct SDL_MouseButtonEvent;
struct SDL_MouseMotionEvent;
struct SDL_MouseWheelEvent;

enum lbMouseMode
{
	lbMouseModeRelative=1,
	lbMouseModeAbsolute=1
};

struct Mouse
{
	bool lb,mb,rb;
	int x,y;
};

class lbGameEvent;

class lbInputSystem: public lbGameEventListener
{
public:
	static lbInputSystem& getInstance();

	virtual ~lbInputSystem();
	virtual void setup();
	virtual void update(long curTime);
	virtual void clear();

	bool handleSDLEvent(SDL_Event& event);//returns true if this was an input event and was handled

	void handleSDLKeyboardEvent(SDL_KeyboardEvent& event);
	void handleSDLMouseMotionEvent(SDL_MouseMotionEvent& event);
	void handleSDLMouseButtonEvent(SDL_MouseButtonEvent& event);
	void handleSDLMouseWheelEvent(SDL_MouseWheelEvent& event);

	int getSDLKeyNumber();
	const uint8* getSDLKeyboardState();
	const uint8* getSDLKeyboardPrevState();
	Mouse getSDLMouseState();
	Mouse getSDLMousePrevState();
	Mouse getSDLMouseState(int mouseId);
	Mouse getSDLMousePrevState(int mouseId);

	bool keyCheck(uint32 sdlKey);
	bool keyPressed(uint32 sdlKey);
	bool keyReleased(uint32 sdlKey);
	void warpMouse(int x,int y);//Caution: will provoke an event if not in relative mode
	void setMouseMode(lbMouseMode mode);
	void setRelativeMouseSpeed(float relXSpeed, float relYSpeed){_mouseRelSpeedX = relXSpeed;_mouseRelSpeedY = relYSpeed;};

private:
	lbInputSystem();
	Mouse _mouse;
	Mouse _prevmouse;
	const uint8* _keyState;
	uint8* _prevKeyState;
	int _keynum;
	float _mouseRelSpeedX;
	float _mouseRelSpeedY;
};
