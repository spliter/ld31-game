/*
 * lbSceneNode2D.h
 *
 *  Created on: 1 mar 2014
 *      Author: spliter
 */

#pragma once

#include <Glee.h>
#include <math/lbMatrix3x3.h>
#include <math/lbMatrix4x4.h>
#include <math/lbVec2.h>
#include <utils/Object.h>


class lbCameraSceneNode2D;
class lbSceneLayer2D;

class lbSceneNode2D: public Object
{
	DEFINE_CLASS(lbSceneNode2D, Object)
public:
	lbSceneNode2D();
	virtual ~lbSceneNode2D();

	virtual void draw(lbCameraSceneNode2D* camera){};


	void setVisibility(bool isVisible){_visible=isVisible;}
	bool isVisible() const {return _visible;}

	void setZOrder(int zOrder){_zOrder=zOrder;}//higher z-order means it'll be drawn on top
	bool getZOrder() const {return _zOrder;}

	lbSceneLayer2D* getLayer() const {return _layer;}

	void addToLayer(lbSceneLayer2D* layer);
	void removeFromLayer();

	virtual void setPosition(float x, float y){_position.set(x,y);markTransformDirty();}
	virtual void setPosition(lbVec2f newPosition){setPosition(newPosition.x,newPosition.y);}
	virtual void setScale(float xScale, float yScale){_scale.set(xScale,yScale);markTransformDirty();}
	virtual void setScale(lbVec2f newScale){setScale(newScale.x,newScale.y);}
	virtual void setRotationDeg(float newRotationDeg){_rotationDeg = newRotationDeg;markTransformDirty();}

	lbVec2f getPosition(){return _position;}
	lbVec2f getScale(){return _scale;}
	float getRotationDeg(){return _rotationDeg;}

	lbVec2f getWorldPosition(){updateTransformIfDirty();return _worldTransform*lbVec2f(0,0);}

	lbVec2f worldToLocalSpace(lbVec2f pos);
	lbVec2f localToWorldSpace(lbVec2f pos);

	const lbMatrix3x3f& getLocalTransform(){updateTransformIfDirty();return _transform;}
	const lbMatrix3x3f& getLocalInverseTransform(){updateTransformIfDirty();return _invTransform;}
	const lbMatrix3x3f& getWorldTransform(){updateTransformIfDirty();return _worldTransform;}
	const lbMatrix3x3f& getWorldInverseTransform(){updateTransformIfDirty();return _invWorldTransform;}

	const lbMatrix4x4f getGLLocalTransform(){updateTransformIfDirty();return lbMatrix4x4f(_transform);}
	const lbMatrix4x4f getGLLocalInverseTransform(){updateTransformIfDirty();return lbMatrix4x4f(_invTransform);}
	const lbMatrix4x4f getGLWorldTransform(){updateTransformIfDirty();return lbMatrix4x4f(_worldTransform);}
	const lbMatrix4x4f getGLWorldInverseTransform(){updateTransformIfDirty();return lbMatrix4x4f(_invWorldTransform);}

	const void pushGLTransform(){glPushMatrix(),glMultMatrixf(getGLWorldTransform().me);};
	const void popGLTransform(){glPopMatrix();}

	virtual void markTransformDirty();

	virtual void updateTransformIfDirty();

	void addChild(lbSceneNode2D* child);
	void removeChild(lbSceneNode2D* child);
	lbSceneNode2D* getChildren(){return _childrenHead;}
	lbSceneNode2D* getNext(){return _next;}
	lbSceneNode2D* getPrev(){return _prev;}
	lbSceneNode2D* getParent(){return _parent;}

private:
	friend class lbSceneLayer2D;
	friend bool sceneDepthComparison(lbSceneNode2D* a, lbSceneNode2D* b);

	lbSceneNode2D* _parent;
	lbSceneNode2D* _childrenHead;

	lbSceneNode2D* _next;
	lbSceneNode2D* _prev;

	lbSceneLayer2D* _layer;
	bool _visible;
	int _zOrder;

	lbVec2f _position;
	lbVec2f _scale;
	float _rotationDeg;

	lbMatrix3x3f _worldTransform;
	lbMatrix3x3f _invWorldTransform;
	lbMatrix3x3f _transform;
	lbMatrix3x3f _invTransform;
	bool _isDirty;
};

inline bool sceneDepthComparison(lbSceneNode2D* a, lbSceneNode2D* b)
{
	return a->_zOrder < b->_zOrder;
}
