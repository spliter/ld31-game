/*
 * lbAnimatedTextBox.cpp
 *
 *  Created on: 19-11-2012
 *      Author: Spliter
 */

#include <GLee.h>
#include "lbAnimatedTextBox.h"
#include <FTGL/ftgl.h>
#include <ft2build.h>
#include "../utils/graphics/drawUtils.h"
#include <utils/graphics/lbTexture.h>
#include <iostream>
#include <iomanip>
#include <sstream>

lbAnimatedTextBox::lbAnimatedTextBox():
	_animationState(asPaused),
	_visibleCharacters(0),
	_totalCharacters(0),
	_millisPerCharacter(30),
	_ellapsedCharMillis(0),
	_maxCharsPerLine(40),
	_font(NULL)
{

}

lbAnimatedTextBox::~lbAnimatedTextBox()
{
}

void lbAnimatedTextBox::setFont(FTFont* font)
{
	_font=font;
}

void lbAnimatedTextBox::setText(const char text[],bool showAll)
{
	_textLines.clear();
	int i=0;
	int lineLen=0;
	int lineStart=0;
	char c=text[0];
	_totalCharacters=0;
	int lastSpace=0;
	while(c)
	{
		if(c=='\n')
		{
			if(lineLen!=0)
			{
				_textLines.push_back(std::string(text,lineStart,lineLen));
				std::cout<<"Pushed back: \""<<_textLines.back()<<"\"\n";
				_totalCharacters+=lineLen;
			}
			lastSpace=i;
			lineStart=i+1;
			lineLen=0;
		}
		else if (lineLen==_maxCharsPerLine)
		{
			if((i-lastSpace)<_maxCharsPerLine && lastSpace>lineStart)
			{
				int len = (lastSpace+1)-lineStart;
				_textLines.push_back(std::string(text,lineStart,len));
				std::cout<<"Pushed back: \""<<_textLines.back()<<"\"\n";
				_totalCharacters+=len;
				lineStart=lastSpace+1;
				lineLen=i-lastSpace;
			}
			else
			{
				_textLines.push_back(std::string(text,lineStart,lineLen));
				std::cout<<"Pushed back: \""<<_textLines.back()<<"\"\n";
				_totalCharacters+=lineLen;
				lastSpace=i;
				lineStart=i+1;
				lineLen=0;
			}
		}
		else
		{
			if(c==' ')
			{
				lastSpace=i;
			}
			lineLen++;
		}
		i++;
		c=text[i];
	}
	if(lineLen!=0)
	{
		_totalCharacters+=lineLen;
		_textLines.push_back(std::string(text,lineStart,lineLen));
		std::cout<<"Pushed back: \""<<_textLines.back()<<"\"\n";
	}

	_ellapsedCharMillis=0;
	if(showAll)
	{
		_visibleCharacters=_totalCharacters;
		_animationState=asFinished;
	}
	else
	{
		_visibleCharacters=0;
		_animationState=asPaused;
	}

}

void lbAnimatedTextBox::resetAnimation()
{
	_animationState=asPaused;
	_visibleCharacters=0;
	_ellapsedCharMillis=0;
}

void lbAnimatedTextBox::startAnimation()
{
	if(_animationState!=asFinished)
	{
		_animationState=asPlaying;
	}
}

void lbAnimatedTextBox::pauseAnimation()
{
	if(_animationState!=asFinished)
	{
		_animationState=asPaused;
	}
}

void lbAnimatedTextBox::showAll()
{
	_animationState=asFinished;
	_visibleCharacters=_totalCharacters;
}

void lbAnimatedTextBox::update(unsigned long frameDif)
{
//	std::cout<<"updating the animated textbox\n";
	if(_animationState==asPlaying)
	{
		_ellapsedCharMillis+=frameDif;
		int newChars = _ellapsedCharMillis/_millisPerCharacter;
//		std::cout<<"is playing "<<_ellapsedCharMillis<<" - "<<frameDif<<" - "<<newChars<<std::endl;
		if(newChars>0)
		{
			_ellapsedCharMillis%=_millisPerCharacter;
			_visibleCharacters+=newChars;
			if(_visibleCharacters>_totalCharacters)
			{
				_visibleCharacters=_totalCharacters;
				_animationState=asFinished;
			}
		}
	}
}

void lbAnimatedTextBox::draw()
{
	lbTexture::unbind();
	glColor3f(0.3f, 0.3f, 0.0f);//glColor3f(1.0f, 0.0f, 0.0f);
	lbRectF rect = getGlobalBoundingRect();
	drawRect(rect.x1, rect.y1, rect.x2, rect.y2);

	glColor3f(1.0f, 1.0f, 1.0f);
	lbVec2f pos = getPosition();
	glPushMatrix();
	glTranslatef(pos.x,pos.y,0);
	glScalef(1,-1,1);
	if(_font && !_font->Error())
	{
		if(_animationState==asFinished)
		{
			for(unsigned int i = 0; i < _textLines.size(); i++)
			{
				_font->Render(_textLines[i].c_str(), -1, FTPoint(0, (-i - 1) * _font->LineHeight(), 0));
			}
		}
		else
		{
			int charactersLeft = _visibleCharacters;
			for(unsigned int i = 0; i < _textLines.size() && charactersLeft > 0; i++)
			{
				int len = -1;
				if(charactersLeft < (int)_textLines[i].length())
				{
					len = charactersLeft;
				}

				_font->Render(_textLines[i].c_str(), len, FTPoint(0, (-i - 1) * _font->LineHeight(), 0));
				charactersLeft -= _textLines[i].length();
			}
		}
	}
	glPopMatrix();
}
