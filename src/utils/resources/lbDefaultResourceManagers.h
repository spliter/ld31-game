/*
 * lbDefaultResourceManagers.h
 *
 *  Created on: 25-05-2013
 *      Author: Spliter
 */

#pragma once

class lbTextureResourceManager;
class lbSpriteResourceManager;
class lbSoundResourceManager;
class lbTileSetResourceManager;

class lbDefaultResourceManagers
{
public:
	static lbDefaultResourceManagers& instance();
	lbDefaultResourceManagers();
	~lbDefaultResourceManagers();

	void setup();

	lbTextureResourceManager* getTextureManager(){return _textureMgr;}
	lbSpriteResourceManager* getSpriteManager(){return _spriteMgr;}
	lbSoundResourceManager* getSfxManager(){return _sfxMgr;}
	lbTileSetResourceManager* getTileSetManager(){return _tileMgr;}
private:
	lbTextureResourceManager* _textureMgr;
	lbSpriteResourceManager* _spriteMgr;
	lbSoundResourceManager* _sfxMgr;
	lbTileSetResourceManager* _tileMgr;
};
