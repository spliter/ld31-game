/*
 * lbMatrix3x3.cpp
 *
 *  Created on: 3 gru 2014
 *      Author: Miko Kuta
 */
#include "lbMatrix3x3.h"

#define LB_CLASS_NAME __internal_lbMatrix3x3f_t
#define LB_VECTOR_CLASS_NAME lbVec2f
#define LB_PARAM_TYPE float
#include "internal/lbMatrix3x3_impl_internal"
#undef LB_CLASS_NAME
#undef LB_VECTOR_CLASS_NAME
#undef LB_PARAM_TYPE

#define LB_CLASS_NAME __internal_lbMatrix3x3d_t
#define LB_VECTOR_CLASS_NAME lbVec2d
#define LB_PARAM_TYPE double
#include "internal/lbMatrix3x3_impl_internal"
#undef LB_CLASS_NAME
#undef LB_VECTOR_CLASS_NAME
#undef LB_PARAM_TYPE

