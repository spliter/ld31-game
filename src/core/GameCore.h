/*
 * DemoApp.h
 *
 *  Created on: 15-12-2011
 *      Author: Spliter
 */

#pragma once

union SDL_Event;
struct SDL_MouseButtonEvent;
struct SDL_MouseMotionEvent;
struct SDL_MouseWheelEvent;

class GameState;
class lbSDLAppDriver;

class GameCore
{
public:
	static GameCore& getInstance();
	bool init(int screenWidth, int screenHeight);
	void deinit();
	void run();

	long getPreviousFrameTime(){return _prevFrameTime;}//in millis
	long getCurrentFrameTime(){return _curFrameTime;}//in millis
	long getFrameTimeDifference(){return _frameDif;}//in millis
	float getFrameTimeDifferencef(){return _frameDifSeconds;}//in millis

	int getScreenWidth(){return _screenWidth;}
	int getScreenHeight(){return _screenHeight;}

	void warpMouse(int x, int y);

	void enableFPSLimit(int targetFPS);
	void disableFPSLimit(){_isFPSLimitEnabled = false;}

	void handleMouseMotionEvent(SDL_MouseMotionEvent& event);
	void handleMouseButtonEvent(SDL_MouseButtonEvent& event);
	void handleMouseWheelEvent(SDL_MouseWheelEvent& event);
	void handleKeyboardEvent(SDL_Event &event);

	void quit(){_quit=true;}


	GameState* getGameState(){return _gameState;}
private:
	void construct();
	GameCore(){construct();}
	GameCore(GameCore& core){construct();}

	lbSDLAppDriver* _driver;

	long _prevFrameTime;
	long _curFrameTime;
	long _frameDif;
	float _frameDifSeconds;

	bool _quit;

	int _screenWidth;
	int _screenHeight;

	bool _isFPSLimitEnabled;
	int _targetFPS;

	GameState* _gameState;
};

inline GameCore& getGameCore(){return GameCore::getInstance();}
