/*
 * lbTileSet.cpp
 *
 *  Created on: 12 lut 2014
 *      Author: spliter
 */

#include <math/util/lbRect.h>
#include <utils/file/TinyXml/tinyxml.h>
#include <utils/graphics/lbTexture.h>
#include <utils/graphics/lbTileSet.h>
#include <utils/resources/lbTextureResourceManager.h>
#include <string>

lbTileSet* lbTileSet::loadFromFile(const std::string &fname,lbTextureResourceManager* textureManager)
{

	TiXmlDocument doc(fname.c_str());
	lbTileSet* info = NULL;
	if(doc.LoadFile())
	{
		int width = -1;
		int height = -1;
		int xOffset = 0;
		int yOffset = 0;
		int numColumns = 0;
		int numRows = 0;
		lbTexture* texture = NULL;
		TiXmlElement* mapElem = doc.FirstChildElement("tileSet");

		bool failed = true;
		if(mapElem)
		{
			mapElem->QueryIntAttribute("width",&width);
			mapElem->QueryIntAttribute("height",&height);
			mapElem->QueryIntAttribute("xOffset",&xOffset);
			mapElem->QueryIntAttribute("yOffset",&yOffset);
			mapElem->QueryIntAttribute("columns",&numColumns);
			mapElem->QueryIntAttribute("rows",&numRows);

			if(numColumns>0 || numRows>0)
			{
				std::string texName;

				mapElem->QueryStringAttribute("texture",&texName);

				texture = textureManager->getResource(texName);

				if(width<1)
				{
					width = texture->getWidth();
				}

				if(height<1)
				{
					height = texture->getHeight();
				}
				failed = false;
			}
		}

		if(!failed)
		{
			info = new lbTileSet(texture,xOffset,yOffset,width,height,numRows,numColumns);
		}
	}

	return info;
}

lbTileSet::lbTileSet(lbTexture* texture, int xOffset, int yOffset, int width, int height, int numRows, int numColumns)
{
	_numRows = numRows;
	_numColumns = numColumns;
	if(texture)
	{
		_originalTexture = texture;
		_texture = new lbTexture(texture,lbRectI(xOffset,yOffset,xOffset+width,yOffset+height));
	}
	else
	{
		_texture = NULL;
		_originalTexture = NULL;
	}
}

lbTileSet::lbTileSet()
{
	_numRows = 1;
	_numColumns = 1;
	_texture = NULL;
	_originalTexture = NULL;
}

lbTileSet::~lbTileSet()
{
	delete _texture;
}

