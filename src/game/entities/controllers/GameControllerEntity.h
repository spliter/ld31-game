/*
 * DebugCameraEntity.h
 *
 *  Created on: 4 gru 2014
 *      Author: Miko Kuta
 */

#pragma once

#include <game/GameEntity.h>
#include <game/GameContext.h>
#include <utils/Object.h>

class GameControllerEntity: public GameEntity
{
public:
	DEFINE_CLASS(GameControllerEntity,GameEntity);

	GameControllerEntity(GameContext* context);
	virtual ~GameControllerEntity();
	virtual void onSetupFromXmlElement(TiXmlElement* element);
	virtual void onSpawned();
	virtual void onUpdate(float dt);

private:
	float _zoom;
};

