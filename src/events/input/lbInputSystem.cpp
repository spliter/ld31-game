/*
 * lbInputSystem.cpp
 *
 *  Created on: 05-09-2012
 *      Author: Spliter
 */

#include <core/GameCore.h>
#include <events/input/lbInputEvents.h>
#include <events/input/lbInputSystem.h>
#include <states/StateManager.h>
#include <string.h>
#include <SDL2/SDL_events.h>
#include <SDL2/SDL_keyboard.h>
#include <SDL2/SDL_mouse.h>
#include <SDL2/SDL_stdinc.h>


using namespace std;

lbInputSystem& lbInputSystem::getInstance()
{
	static lbInputSystem instance;
	return instance;
}

lbInputSystem::lbInputSystem()
{
	_keyState = NULL;
	_prevKeyState = NULL;
	_keynum = 0;
	_mouseRelSpeedX=0;
	_mouseRelSpeedY=0;
}

lbInputSystem::~lbInputSystem()
{
}

void lbInputSystem::setup()
{
	_keyState = SDL_GetKeyboardState(&_keynum);
	_prevKeyState = new Uint8[_keynum];
	memcpy(_prevKeyState, _keyState, sizeof(Uint8)*_keynum);
	_mouse.x = _prevmouse.x = 0;
	_mouse.y = _prevmouse.y = 0;
	_mouse.lb = _prevmouse.lb = 0;
	_mouse.rb = _prevmouse.rb = 0;
	_mouse.mb = _prevmouse.mb = 0;
}

void lbInputSystem::update(long curTime)
{
	memcpy(_prevKeyState, _keyState, sizeof(Uint8)*_keynum);
	SDL_PumpEvents();
	_prevmouse.x = _mouse.x;
	_prevmouse.y = _mouse.y;
	_prevmouse.lb = _mouse.lb;
	_prevmouse.mb = _mouse.mb;
	_prevmouse.rb = _mouse.rb;

	int x,y;
	Uint8 mstate = SDL_GetMouseState(&x, &y);
	if(!SDL_GetRelativeMouseMode())
	{
		_mouse.x=x;
		_mouse.y=y;
	}
	_mouse.lb = mstate&SDL_BUTTON(1);
	_mouse.mb = mstate&SDL_BUTTON(2);
	_mouse.rb = mstate&SDL_BUTTON(3);
}

void lbInputSystem::clear()
{
	delete[] _prevKeyState;
}

bool lbInputSystem::handleSDLEvent(SDL_Event& event)
{
	if(event.type==SDL_KEYUP||event.type==SDL_KEYDOWN)
	{
		handleSDLKeyboardEvent(event.key);
		return true;
	}
	else if(event.type==SDL_MOUSEMOTION)
	{
		handleSDLMouseMotionEvent(event.motion);
		return true;
	}
	else if(event.type==SDL_MOUSEBUTTONDOWN||event.type==SDL_MOUSEBUTTONUP)
	{
		handleSDLMouseButtonEvent(event.button);
		return true;
	}
	return false;
}

void lbInputSystem::handleSDLKeyboardEvent(SDL_KeyboardEvent& event)
{
	lbKeyboardEvent keyEvent;

	keyEvent.key = event.keysym.sym;
	keyEvent.keyMod = event.keysym.mod;
	keyEvent.type = event.state==SDL_PRESSED ? keyEvent.KeyPressed : keyEvent.KeyReleased;
	StateManager::getInstance().onKeyboardEvent(&keyEvent);
	fireEvent(&keyEvent);
}

void lbInputSystem::handleSDLMouseMotionEvent(SDL_MouseMotionEvent& event)
{
	if(SDL_GetRelativeMouseMode())
	{
		_mouse.x+=event.xrel*_mouseRelSpeedX;
		_mouse.y+=event.yrel*_mouseRelSpeedY;
	}
	lbMouseMotionEvent motionEvent;
	motionEvent.x = event.x;
	motionEvent.y = event.y;
	motionEvent.relX = event.xrel;
	motionEvent.relY = event.yrel;
	StateManager::getInstance().onMouseMotionEvent(&motionEvent);
	fireEvent(&motionEvent);
}

void lbInputSystem::handleSDLMouseButtonEvent(SDL_MouseButtonEvent& event)
{
	lbMouseButtonEvent buttonEvent;
	switch(event.button)
	{
	case SDL_BUTTON_LEFT:
		buttonEvent.button = buttonEvent.ButtonLeft;
		break;
	case SDL_BUTTON_MIDDLE:
		buttonEvent.button = buttonEvent.ButtonMiddle;
		break;
	case SDL_BUTTON_RIGHT:
		buttonEvent.button = buttonEvent.ButtonRight;
		break;
	case SDL_BUTTON_X1:
		buttonEvent.button = buttonEvent.ButtonExtra1;
		break;
	case SDL_BUTTON_X2:
		buttonEvent.button = buttonEvent.ButtonExtra2;
		break;
	}
	buttonEvent.type = event.state==SDL_PRESSED?buttonEvent.MousePressed:buttonEvent.MouseReleased;

	buttonEvent.x = event.x;
	buttonEvent.y = event.y;
	StateManager::getInstance().onMouseButtonEvent(&buttonEvent);
	fireEvent(&buttonEvent);
}

void lbInputSystem::handleSDLMouseWheelEvent(SDL_MouseWheelEvent& event)
{
	lbMouseWheelEvent buttonEvent;
	buttonEvent.x = event.x;
	buttonEvent.y = event.y;

	StateManager::getInstance().onMouseWheelEvent(&buttonEvent);
	fireEvent(&buttonEvent);
}

int lbInputSystem::getSDLKeyNumber()
{
	return _keynum;
}

const uint8* lbInputSystem::getSDLKeyboardState()
{
	return _keyState;
}

const uint8* lbInputSystem::getSDLKeyboardPrevState()
{
	return _prevKeyState;
}

Mouse lbInputSystem::getSDLMouseState()
{
	return _mouse;
}

Mouse lbInputSystem::getSDLMousePrevState()
{
	return _prevmouse;
}

bool lbInputSystem::keyCheck(uint32 key)
{
	if((int)key<_keynum)
		return _keyState[key];
	else
		return false;
}

bool lbInputSystem::keyPressed(uint32 key)
{
	if((int)key>=_keynum)
		return false;
	return _keyState[key]&&!_prevKeyState[key];
}

bool lbInputSystem::keyReleased(uint32 key)
{
	if((int)key>=_keynum)
		return false;
	return !_keyState[key]&&_prevKeyState[key];
}

void lbInputSystem::warpMouse(int x, int y)
{
	if(SDL_GetRelativeMouseMode())
	{
		_mouse.x = x;
		_mouse.y = y;
	}
	else
	{
		GameCore::getInstance().warpMouse(x,y);
		_mouse.x = x;
		_mouse.y = y;
	}
}

void lbInputSystem::setMouseMode(lbMouseMode mode)
{
	SDL_SetRelativeMouseMode(mode==lbMouseModeRelative?SDL_TRUE:SDL_FALSE);
}
