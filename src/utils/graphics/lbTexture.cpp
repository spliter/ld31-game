/*
 * lbTexture.cpp
 *
 *  Created on: 09-10-2012
 *      Author: Spliter
 */

#include <Glee.h>
#include <IL/il.h>
#include <math/util/lbRect.h>
#include <utils/debug/debug.h>
#include <utils/debug/Logger.h>
#include <utils/graphics/lbTexture.h>
#include <cstdio>
#include <cstdlib>

GLuint lbTexture::_boundGLTexture = 0;

static void logErrorMessage(GLenum error)
{
	switch (error)
	{
		case GL_NO_ERROR:
		break;
		case GL_INVALID_ENUM:
			Logger::logText("GL_INVALID_ENUM");
		break;
		case GL_INVALID_VALUE:
			Logger::logText("GL_INVALID_VALUE");
		break;
		case GL_INVALID_OPERATION:
			Logger::logText("GL_INVALID_OPERATION");
		break;
		case GL_INVALID_FRAMEBUFFER_OPERATION:
			Logger::logText("GL_INVALID_FRAMEBUFFER_OPERATION");
		break;
		case GL_OUT_OF_MEMORY:
			Logger::logText("GL_OUT_OF_MEMORY");
		break;
		case GL_STACK_UNDERFLOW:
			Logger::logText("GL_STACK_UNDERFLOW");
		break;
		case GL_STACK_OVERFLOW:
			Logger::logText("GL_STACK_OVERFLOW");
		break;
		default:
			Logger::logText("UNKNOWN GL ERROR");
	}
}

lbTexture* lbTexture::loadTexture(const std::string & fname)
{
	unsigned int image = ilGenImage();
	ilBindImage(image);

	bool loaded = ilLoadImage(fname.c_str());
	if (loaded)
	{
//		printf("loaded image %s\n", fname.c_str());
		lbTexture* texture = new lbTexture(image);
		return texture;
	}
	else
	{
		printf("Failed to load image %s\n", fname.c_str());
		ilDeleteImage(image);
		return NULL;
	}

}

lbTexture::lbTexture(lbTexture* parent, lbRectI region) :
		_isSubTexture(true), _glTexture(0), _ilImage(0), _width(0), _height(0), _xoffset(0), _yoffset(0), _xscale(1.0f), _yscale(1.0f), _parentTex(parent)
{
	lbAssert(parent);

	_xoffset = region.x1 + parent->_xoffset;
	_yoffset = region.y1 + parent->_yoffset;
	_width = region.x2 - region.x1;
	_height = region.y2 - region.y1;
	if (_width <= 0)
		_width = 1;
	if (_height <= 0)
		_height = 1;
	_xscale = _parentTex->_xscale; //1.0f / float(_width) / float(_parentTex->getWidth());
	_yscale = _parentTex->_yscale; //1.0f / float(_height) / float(_parentTex->getHeight());
	_glTexture = _parentTex->getGLTexture();
	_ilImage = _parentTex->getGLTexture();
}

lbTexture::lbTexture(int width, int height, unsigned char* data) :
		_isSubTexture(false), _glTexture(0), _ilImage(0), _width(width), _height(height), _xoffset(0), _yoffset(0), _xscale(1.0f), _yscale(1.0f), _parentTex(NULL)
{
	//lbAssert(width >= 0 && height >= 0 && data != NULL);

	_ilImage = ilGenImage();
	ilBindImage(_ilImage);

	ilTexImage(width, height, 0, 4, IL_RGBA, IL_UNSIGNED_BYTE, data);
	printf("IL error: %d", ilGetError());
	_xscale = 1.0f / float(_width);
	_yscale = 1.0f / float(_height);
	ilBindImage(0);
	updateGLTexture();
}

lbTexture::lbTexture(unsigned int image) :
		_isSubTexture(false), _glTexture(0), _ilImage(0), _width(1), _height(1), _xoffset(0), _yoffset(0), _xscale(1.0f), _yscale(1.0f), _parentTex(NULL)
{
	_ilImage = image;
	ilBindImage(_ilImage);
	_width = ilGetInteger(IL_IMAGE_WIDTH);
	_height = ilGetInteger(IL_IMAGE_HEIGHT);
	_xscale = 1.0f / float(_width);
	_yscale = 1.0f / float(_height);
	ilBindImage(0);
	updateGLTexture();
}

lbTexture::lbTexture(int width, int height, GLuint internalFormat, GLuint format, GLuint dataType, const char* data) :
		_isSubTexture(false), _glTexture(0), _ilImage(0), _width(width), _height(height), _xoffset(0), _yoffset(0), _xscale(1.0f), _yscale(1.0f), _parentTex(NULL)
{
	_xscale = 1.0f / float(_width);
	_yscale = 1.0f / float(_height);
	glGenTextures(1, &_glTexture);
	glBindTexture(GL_TEXTURE_2D, _glTexture);

	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

	glTexImage2D(GL_TEXTURE_2D, 0, internalFormat, _width, _height, 0, format, dataType, data);

	logErrorMessage(glGetError());
	glBindTexture(GL_TEXTURE_2D, 0);

}

lbTexture::~lbTexture()
{
	if (_boundGLTexture == _glTexture)
	{
		unbind();
	}

	if (!isSubTexture())
	{
		if (_ilImage)
		{
			ilDeleteImage(_ilImage);
		}

		if (_glTexture)
		{
			glDeleteTextures(1, &_glTexture);
		}
	}
}

void lbTexture::updateGLTexture()
{
	if (!isSubTexture() && _ilImage != 0)
	{
		if (_glTexture == 0)
		{
			glGenTextures(1, &_glTexture);
			glBindTexture(GL_TEXTURE_2D, _glTexture);
			_boundGLTexture = _glTexture;
			ilBindImage(_ilImage);
			int format = ilGetInteger(IL_IMAGE_FORMAT);
			unsigned const char* data = ilGetData();
			//			unsigned const int* datai= (unsigned const int*)data;

			//			for(int y=0;y<_height;y++)
			//			{
			//				for(int x=0;x<_width;x++)
			//				{
			//					printf("%#010x    ",datai[y*_width+x]);
			//				};
			//				printf("\n");
			//			}

			glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, _width, _height, 0, format, GL_UNSIGNED_BYTE, data);

			glTexEnvf( GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);

			glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
			// when texture area is large, bilinear filter the first mipmap
			glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

			// if wrap is true, the texture wraps over at the edges (repeat)
			//       ... false, the texture ends at the edges (clamp)
			glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
			glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
		}
		else
		{
			glBindTexture(GL_TEXTURE_2D, _glTexture);
			_boundGLTexture = _glTexture;
			ilBindImage(_ilImage);
			int format = ilGetInteger(IL_IMAGE_FORMAT);
			unsigned const char* data = ilGetData();
			glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, _width, _height, format, GL_UNSIGNED_BYTE, data);

		}
	}
}

lbRectF lbTexture::remapCoords(lbRectF rect)
{
	lbRectF ret;
	ret.x1 = (rect.x1 + _xoffset) * _xscale;
	ret.x2 = (rect.x2 + _xoffset) * _xscale;
	ret.y1 = (rect.y1 + _yoffset) * _yscale;
	ret.y2 = (rect.y2 + _yoffset) * _yscale;
	return ret;
}

void lbTexture::remapCoords(float inX, float inY, float &outNX, float &outNY)
{
	outNX = (inX + _xoffset) * _xscale;
	outNY = (inY + _yoffset) * _yscale;
}

lbVec2f lbTexture::remapCoord(lbVec2f tc)
{
	return lbVec2f((tc.x + _xoffset) * _xscale, (tc.y + _yoffset) * _yscale);
}

lbRectF lbTexture::remapNormalCoords(lbRectF rect)
{
	lbRectF ret;
	ret.x1 = (rect.x1 * _width + _xoffset) * _xscale;
	ret.x2 = (rect.x2 * _width + _xoffset) * _xscale;
	ret.y1 = (rect.y1 * _height + _yoffset) * _yscale;
	ret.y2 = (rect.y2 * _height + _yoffset) * _yscale;
	return ret;
}

void lbTexture::remapNormalCoords(float inNX, float inNY, float &outNX, float &outNY)
{
	outNX = (inNX * _width + _xoffset) * _xscale;
	outNY = (inNY * _height + _yoffset) * _yscale;
}

lbVec2f lbTexture::remapNormalCoord(lbVec2f tc)
{
	return lbVec2f((tc.x * _width + _xoffset) * _xscale, (tc.y * _height + _yoffset) * _yscale);
}

void lbTexture::bind()
{
	if (_boundGLTexture != _glTexture)
	{
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		//we only need to bind a new texture if the previous one was different from the current one
		glBindTexture(GL_TEXTURE_2D, _glTexture);
		_boundGLTexture = _glTexture;
	}
}

void lbTexture::unbind()
{
	glBindTexture(GL_TEXTURE_2D, 0);
	_boundGLTexture = 0;
}

