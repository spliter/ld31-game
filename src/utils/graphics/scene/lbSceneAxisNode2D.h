/*
 * lbSceneAxisNode2D.h
 *
 *  Created on: 2 mar 2014
 *      Author: spliter
 */

#pragma once

#include <Glee.h>
#include <math/lbMatrix3x3.h>
#include <utils/graphics/lbTexture.h>
#include <utils/graphics/scene/lbSceneNode2D.h>
#include <utils/Object.h>

class lbCameraSceneNode2D;

class lbSceneAxisNode2D: public lbSceneNode2D
{
DEFINE_CLASS(lbSceneAxisNode2D,lbSceneNode2D)
public:
	lbSceneAxisNode2D();
	virtual ~lbSceneAxisNode2D();
	virtual void draw(lbCameraSceneNode2D* camera);

};
