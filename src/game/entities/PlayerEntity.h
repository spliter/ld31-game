/*
 * PlayerEntity.h
 *
 *  Created on: 6 gru 2014
 *      Author: Miko Kuta
 */

#pragma once

#include <game/collisions/CollisionPoly.h>
#include <game/entities/SolidEntity.h>
#include <math/lbVec2.h>
#include <utils/graphics/scene/lbSpriteSceneNode2D.h>
#include <utils/Object.h>


extern const float PlayerShoveStrength;

class PlayerEntity: public SolidEntity
{
public:
	DEFINE_CLASS(PlayerEntity,SolidEntity);

	PlayerEntity(GameContext* context);
	virtual ~PlayerEntity();
	virtual void onCreated();
	virtual void onSpawned();
	virtual void onUpdate(float dt);

	virtual void draw(lbCameraSceneNode2D* camera);

	virtual void setPosition(float x, float y){super::setPosition(x,y);_collider.setPosition(getPosition());}
	virtual void setPosition(lbVec2f newPos){setPosition(newPos.x,newPos.y);}

	virtual CollisionPoly** getCollisionPolygons(){return _colliders;}
	virtual int getCollisionPolyNum(){return 1;}

	int getHealth(){return _healthPoints;}
	int getMaxHealth(){return _maxHealthPoints;}
	void applyDamage(int damagePoints);
	void addHealth(int healthAmount);
private:
	void checkCollisions();

	int _healthPoints;
	int _maxHealthPoints;

	lbSprite* _playerSprite;
	lbSpriteSceneNode2D _playerSpriteDrawable;
	CollisionPoly _collider;
	CollisionPoly* _colliders[1];

	lbVec2f _movement;

	int _bulletControllerId;
	int _bulletControllerUid;

	float _timeSinceLastShot;
};

