/*
 * DebugCameraEntity.h
 *
 *  Created on: 4 gru 2014
 *      Author: Miko Kuta
 */

#pragma once

#include <game/GameEntity.h>
#include <game/GameContext.h>
#include <utils/Object.h>

class DebugCameraEntity: public GameEntity
{
public:
	DEFINE_CLASS(DebugCameraEntity,GameEntity);

	DebugCameraEntity(GameContext* context);
	virtual ~DebugCameraEntity();
	virtual void onUpdate(float dt);
};

