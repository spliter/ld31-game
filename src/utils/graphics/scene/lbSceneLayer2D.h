/*
 * lbSceneLayer2D.h
 *
 *  Created on: 2 mar 2014
 *      Author: spliter
 */

#pragma once

#include <utils/Object.h>
#include <utils/graphics/scene/lbSceneNode2D.h>
#include <list>

class lbCameraSceneNode2D;

class lbSceneLayer2D: public Object
{
	DEFINE_CLASS(lbSceneLayer2D, Object)
public:
	lbSceneLayer2D();
	virtual ~lbSceneLayer2D();

	void addNode(lbSceneNode2D* drawable);
	void removeNode(lbSceneNode2D* drawable);
	void clearNodes();

	void draw(lbCameraSceneNode2D* camera);//sorts all drawables, checks if they're visible on screen and draws them
private:
	std::list<lbSceneNode2D*> _drawables;
};

