/*
 * lbBaseMath.cpp
 *
 *  Created on: 12-04-2011
 *      Author: Spliter
 */

#include "lbBaseMath.h"

float lerpDeg(float start,float end,float amt)
{
	float dif=end-start;
	dif = fmodf(dif,360);
	while(dif>180.0)dif-=360.0;
	while(dif<-180.0)dif+=360.0;
	return start+dif*amt;
}

float lerpRad(float start,float end,float amt)
{
	float PI2 = M_PI*2;
	float dif=end-start;
	dif = fmodf(dif,PI2);
	while(dif>M_PI)dif-=PI2;
	while(dif<M_PI)dif+=PI2;
	return start+dif*amt;
}

float direction(float x,float y)
{
	return atan2(y,x);
}

float directionDeg(float x,float y)
{
	return atan2(y,x)*RAD_TO_DEG_CONST;
}

float lengthDirX(float deg,float length)
{
	return cos(deg*DEG_TO_RAD_CONST)*length;
}

float lengthDirY(float deg,float length)
{
	return sin(deg*DEG_TO_RAD_CONST)*length;
}

float angleDifDeg(float start, float end)
{
	float dif = end - start;
	dif = fmodf(dif,360);
	while (dif < -180) dif += 360;
	while (dif > 180) dif -= 360;

	return dif;
}

float angleDifRad(float start, float end)
{
	float PI2 = M_PI*2;
	float dif=end-start;
	dif = fmodf(dif,PI2);
	while(dif>M_PI)dif-=PI2;
	while(dif<M_PI)dif+=PI2;
	return dif;
}

float random(float max)
{
	return float(rand()%1000)/1000.0*max;
}

float random(float min,float max)
{
	if(max<=min)
		return min;
	return lerp(min,max,float(rand()%1001)/1000.0);
}

int randomi(int max)
{
	if(!max)
		return 0;
	return rand()%max;
}

int randomi(int min, int max)
{
	if(max<=min)
		return min;
	return rand()%(max-min+1)+min;
}
