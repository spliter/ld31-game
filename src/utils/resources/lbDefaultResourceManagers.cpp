/*
 * lbDefaultResourceManagers.cpp
 *
 *  Created on: 25-05-2013
 *      Author: Spliter
 */

#include <utils/resources/lbDefaultResourceManagers.h>
#include <utils/resources/lbSoundResourceManager.h>
#include <utils/resources/lbSpriteResourceManager.h>
#include <utils/resources/lbTextureResourceManager.h>
#include <utils/resources/lbTileSetResourceManager.h>

lbDefaultResourceManagers& lbDefaultResourceManagers::instance()
{
	static lbDefaultResourceManagers instance;
	return instance;
}

lbDefaultResourceManagers::lbDefaultResourceManagers():
		_textureMgr(NULL),
		_spriteMgr(NULL),
		_sfxMgr(NULL),
		_tileMgr(NULL)
{

}

lbDefaultResourceManagers::~lbDefaultResourceManagers()
{
	_textureMgr->clearResources();
	_spriteMgr->clearResources();
	_sfxMgr->clearResources();

	delete _textureMgr;
	delete _spriteMgr;
	delete _sfxMgr;
}

void lbDefaultResourceManagers::setup()
{
	_textureMgr = new lbTextureResourceManager();
	_spriteMgr = new lbSpriteResourceManager(_textureMgr);
	_sfxMgr = new lbSoundResourceManager();
	_tileMgr = new lbTileSetResourceManager(_textureMgr);
}
