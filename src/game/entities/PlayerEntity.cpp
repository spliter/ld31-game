/*
 * PlayerEntity.cpp
 *
 *  Created on: 6 gru 2014
 *      Author: Miko Kuta
 */

#include <events/input/lbInputSystem.h>
#include <events/input/lbKeys.h>
#include <game/entities/controllers/BulletControllerEntity.h>
#include <game/entities/PlayerEntity.h>
#include <game/entities/SolidRectangleEntity.h>
#include <game/GameContext.h>
#include <game/GameEntity.h>
#include <game/GameEntityFactoryManager.h>
#include <game/level/GameLevel.h>
#include <game/states/GameState.h>
#include <global.h>
#include <Glee.h>
#include <math/lbBaseMath.h>
#include <utils/debug/debug.h>
#include <utils/graphics/drawUtils.h>
#include <utils/graphics/lbTexture.h>
#include <utils/graphics/scene/lbCameraSceneNode2D.h>
#include <utils/graphics/scene/lbSceneLayer2D.h>
#include <utils/resources/lbDefaultResourceManagers.h>
#include <utils/resources/lbResourceManager.h>
#include <utils/resources/lbSpriteResourceManager.h>
#include <cstdio>
#include <cstdlib>
#include <list>

struct GameContext;

const float PlayerShoveStrength = 80;
static float movementSpeed = 100.0f; //units/second
static float ShotDelay = 0.1f;
static float ShotScatterDeg = 8.0f;
static float BulletSpeedDecay = 4.0f;
static float BulletCollideDecay = 0.3f;
static float BulletSpeed = 2000.1f;
static float BulletTimeToLive = 1.3f;
static float BulletDamageScaler = 0.01f;

REGISTER_GAME_ENTITY(PlayerEntity, "player_entity");

PlayerEntity::PlayerEntity(GameContext* context) :
		SolidEntity(context),
		_healthPoints(0),
		_maxHealthPoints(0),
		_playerSprite(NULL),
		_bulletControllerId(0),
		_bulletControllerUid(0),
		_timeSinceLastShot(ShotDelay)
{
	LogEntryPoint();
	_colliders[0] = &_collider;
	setName("player");
}

PlayerEntity::~PlayerEntity()
{
	LogEntryPoint();
}

void PlayerEntity::onCreated()
{
	LogEntryPoint();
	super::onCreated();
	_playerSprite = lbDefaultResourceManagers::instance().getSpriteManager()->getResource(buildSpriteFilename("player"));
	_playerSpriteDrawable.setSprite(_playerSprite);
	addChild(&_playerSpriteDrawable);
	_collider.makeCircle(0, 0, 4, 8);

	_maxHealthPoints = 100;
	_healthPoints = _maxHealthPoints;
}

void PlayerEntity::onSpawned()
{
	LogEntryPoint();
	super::onSpawned();
	GameLevel* level = getLevel();
	lbAssert(level);
	level->characterLayer.addNode(&_playerSpriteDrawable);
	BulletControllerEntity* bulletEnt = BulletControllerEntity::castEntity(getGameContext()->gameState->spawnEntity("bullet_controller"));

	if (bulletEnt)
	{
		_bulletControllerId = bulletEnt->getIndexId();
		_bulletControllerUid = bulletEnt->getUniqueId();
	}
}

void PlayerEntity::onUpdate(float dt)
{
	LogEntryPoint();
	bool upPressed = lbInputSystem::getInstance().keyCheck(LBK_W) || lbInputSystem::getInstance().keyCheck(LBK_UP);
	bool downPressed = lbInputSystem::getInstance().keyCheck(LBK_S) || lbInputSystem::getInstance().keyCheck(LBK_DOWN);
	bool leftPressed = lbInputSystem::getInstance().keyCheck(LBK_A) || lbInputSystem::getInstance().keyCheck(LBK_LEFT);
	bool rightPressed = lbInputSystem::getInstance().keyCheck(LBK_D) || lbInputSystem::getInstance().keyCheck(LBK_RIGHT);
	lbVec2f movement;
	movement += upPressed ? lbVec2f(0, -movementSpeed) : lbVec2f(0, 0);
	movement += downPressed ? lbVec2f(0, movementSpeed) : lbVec2f(0, 0);
	movement += leftPressed ? lbVec2f(-movementSpeed, 0) : lbVec2f(0, 0);
	movement += rightPressed ? lbVec2f(movementSpeed, 0) : lbVec2f(0, 0);

	if (movement.squareLength() > 1)
	{
		float direction = movement.directionDeg() + 90;
		setRotationDeg(direction);
	}

	setPosition(getPosition() + movement * dt);
	_movement = movement;
	checkCollisions();
	if (_timeSinceLastShot < ShotDelay)
	{
		_timeSinceLastShot += dt;
	}
	else
	{
		Mouse mouse = lbInputSystem::getInstance().getSDLMouseState();
		if (mouse.lb)
		{
			_timeSinceLastShot = 0;
			BulletControllerEntity* bulletEnt = BulletControllerEntity::castEntity(getGameContext()->gameState->getEntity(_bulletControllerId, _bulletControllerUid));
			if (bulletEnt)
			{
				lbVec2f mouseWorldPos = getLevel()->characterCamera.screenToWorldSpace(lbVec2f(mouse.x, mouse.y));
				lbVec2f dif = (mouseWorldPos-getPosition()).normalize();
				dif.rotateDeg(random(ShotScatterDeg)-ShotScatterDeg*0.5f);
				bulletEnt->spawnBullet(getPosition(), dif.normalize() * BulletSpeed,BulletDamageScaler, BulletSpeedDecay,BulletCollideDecay, BulletTimeToLive);
			}
		}
	}
}

void PlayerEntity::draw(lbCameraSceneNode2D* camera)
{
	LogEntryPoint();
	lbTexture::unbind();

	Mouse mouse = lbInputSystem::getInstance().getSDLMouseState();
	lbVec2f mouseWorldPos = getLevel()->characterCamera.screenToWorldSpace(lbVec2f(mouse.x, mouse.y));
	glColor3f(1, 1, 0);
	drawCircle(mouseWorldPos.x, mouseWorldPos.y, 10, 16, false);
}

void PlayerEntity::applyDamage(int damagePoints)
{
	_healthPoints-=damagePoints;

}

void PlayerEntity::addHealth(int healthAmount)
{

}

void PlayerEntity::checkCollisions()
{
	LogEntryPoint();
	solidList solids = getLevel()->solidGeometryWorld.getSolids();
	solidIter iter = solids.begin();
	while (iter != solids.end())
	{
		SolidEntity* ent = (*iter);
		if (ent->instanceOf(SolidRectangleEntity::getClassDef()))
		{
			CollisionPoly** polys = ent->getCollisionPolygons();
			int polyNum = ent->getCollisionPolyNum();
			for (int i = 0; i < polyNum; i++)
			{
				lbVec2f separatingVector;
				bool collided = CollisionPoly::GetSeparatingVector(_collider, *polys[i], separatingVector);
				if (collided)
				{
					setPosition(getPosition() + separatingVector);
				}
			}
		}
		iter++;
	}
}
