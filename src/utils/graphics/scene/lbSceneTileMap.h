/*
 * lbSceneTileMap.h
 *
 *  Created on: 2 mar 2014
 *      Author: spliter
 */

#pragma once
#include <utils/graphics/scene/lbSceneNode2D.h>
#include <utils/graphics/lbTileMap.h>
#include <utils/Object.h>

class lbCameraSceneNode2D;


class lbSceneTileMap: public lbSceneNode2D
{
	DEFINE_CLASS(lbSceneTileMap,lbSceneNode2D)
public:
	lbSceneTileMap(){}
	virtual ~lbSceneTileMap(){}

	virtual void draw(lbCameraSceneNode2D* camera);

	lbTileMap& getTileMap(){return _tileMap;}
	const lbTileMap& getConstTileMap() const {return _tileMap;}
private:
	lbTileMap _tileMap;
};
