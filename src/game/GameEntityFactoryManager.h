/*
 * lbGameEntityFactoryManager.h
 *
 *  Created on: 05-04-2013
 *      Author: Spliter
 */

#pragma once

#include <string>

class GameEntityFactory;
namespace GameEntityFactories
{
	bool registerFactory(GameEntityFactory* factory, std::string entityName);
	GameEntityFactory* findFactory(const std::string &str);
}
