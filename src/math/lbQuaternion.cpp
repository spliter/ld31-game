/*
 * lbQuaternion.cpp
 *
 *  Created on: 20-05-2011
 *      Author: Spliter
 */
#include "lbQuaternion.h"

#include "iostream"
#include "lbQuaternion.h"

#define LB_CLASS_NAME __internal_lbQuaternion_t
#define LB_AANGLE_CLASS_NAME __internal_lbAxisAngle_t
#define LB_VECTOR_CLASS_NAME lbVec3f
#define LB_4x4MATRIX_CLASS_NAME lbMatrix4x4f
#define LB_PARAM_TYPE float
#include "internal/lbQuaternion_impl_internal"
#undef LB_CLASS_NAME
#undef LB_AANGLE_CLASS_NAME
#undef LB_VECTOR_CLASS_NAME
#undef LB_4x4MATRIX_CLASS_NAME
#undef LB_PARAM_TYPE

#define LB_CLASS_NAME __internal_lbQuaterniond_t
#define LB_AANGLE_CLASS_NAME __internal_lbAxisAngled_t
#define LB_VECTOR_CLASS_NAME lbVec3d
#define LB_4x4MATRIX_CLASS_NAME lbMatrix4x4d
#define LB_PARAM_TYPE double
#include "internal/lbQuaternion_impl_internal"
#undef LB_CLASS_NAME
#undef LB_AANGLE_CLASS_NAME
#undef LB_VECTOR_CLASS_NAME
#undef LB_4x4MATRIX_CLASS_NAME
#undef LB_PARAM_TYPE
