/*
 * lbSpriteDrawable.cpp
 *
 *  Created on: 24-11-2012
 *      Author: Spliter
 */

#include <Glee.h>
#include <math/util/lbRect.h>
#include <stddef.h>
#include <utils/graphics/drawable/helpers/lbSpriteDrawable.h>
#include <utils/graphics/lbSprite.h>

lbSpriteDrawable::lbSpriteDrawable():
	_x(0),
	_y(0),
	_r(1),_g(1),_b(1),_a(1),
	_curFrame(0),
	_sprite(NULL)
{

}

lbSpriteDrawable::~lbSpriteDrawable()
{
}

void lbSpriteDrawable::setSprite(lbSprite* sprite)
{
	_sprite=sprite;
	if(_sprite)
	{
		lbRectF bbox = toRectF(_sprite->getSpriteBbox());
		bbox.move(_x,_y);
		setRenderArea(bbox);
	}
	else
	{
		setRenderArea(-1,-1,-1,-1);
	}
}

void lbSpriteDrawable::setPosition(float x, float y)
{
	_x=x;
	_y=y;
	if(_sprite)
	{
		lbRectI bbox =_sprite->getSpriteBbox();
		bbox.move(_x,_y);
	}
}

void lbSpriteDrawable::draw(lbRectF camera)
{
	if(_sprite)
	{
		glColor4f(_r,_g,_b,_a);
		_sprite->renderFrame(_curFrame,_x,_y,1.0f,1.0f);
	}
}
