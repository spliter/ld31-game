/*
 * lbCameraSceneNode2D.h
 *
 *  Created on: 2 mar 2014
 *      Author: spliter
 */

#pragma once

#include <math/lbVec2.h>
#include <math/util/lbRect.h>
#include <utils/graphics/scene/lbSceneNode2D.h>
#include <utils/Object.h>

struct lbRectI;

class lbCameraSceneNode2D: public lbSceneNode2D
{
	DEFINE_CLASS(lbCameraSceneNode2D, lbSceneNode2D)
public:
	lbCameraSceneNode2D();
	virtual ~lbCameraSceneNode2D();

	void pushCamera();
	void popCamera();
	void useCamera();
	void getWorldSpaceViewport(lbVec2f frustrum[4]);

	void setViewportAnchor(lbVec2f anchorRatio);

	void setViewport(lbRectI viewport, float nearClip, float farClip);
	lbRectI getViewport(){return _windowViewport;}

	void setZoom(float zoom);
	float getZoom(){return _zoom;}

	lbVec2f screenToWorldSpace(lbVec2f pos);
	lbVec2f worldToScreenSpace(lbVec2f pos);
private:
	void updateLocalViewport();
	lbRectI _windowViewport;
	lbRectF _localViewport;
	float _nearClip;
	float _farClip;
	lbVec2f _vpAnchor;
	float _zoom;
};

