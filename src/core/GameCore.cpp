/*
 * DemoApp.cpp
 *
 *  Created on: 15-12-2011
 *      Author: Spliter
 */
#include <GLee.h>
#include <core/GameCore.h>
#include <core/lbAudioSystem.h>
#include <core/lbSDLAppDriver.h>
#include <events/input/lbInputSystem.h>
#include <game/states/GameState.h>
#include <IL/il.h>
#include <states/StateManager.h>
#include <SDL2/SDL_events.h>
#include <utils/base.h>
#include <utils/debug/Logger.h>
#include <utils/resources/lbDefaultResourceManagers.h>

using namespace std;

GameCore& GameCore::getInstance()
{
	static GameCore instance;
	return instance;
}
void GameCore::construct()
{
	_driver = NULL;
	_prevFrameTime = 0;
	_curFrameTime = 0;
	_frameDif = 0;
	_frameDifSeconds = 0;
	_quit= false;
	_screenWidth = 0;
	_screenHeight = 0;
	_isFPSLimitEnabled = false;
	_targetFPS = 120;
}

bool GameCore::init(int screenWidth, int screenHeight)
{
	LogState("initializing game");

	_driver = new lbSDLAppDriver();
	_driver->setup(screenWidth, screenHeight, false);
	ilInit();

	lbDefaultResourceManagers::instance().setup();
	lbAudioSystem::instance().init();

	this->_screenWidth = screenWidth;
	this->_screenHeight = screenHeight;

	_curFrameTime = _driver->getEllapsedMillis();
	_prevFrameTime = _curFrameTime;
	_frameDif = _curFrameTime - _prevFrameTime;
	_frameDifSeconds = (float) _frameDif / 1000.0f;
	lbInputSystem::getInstance().setup();
	_gameState = new GameState(this);
	getStateManager().startState(_gameState);
	LogState("init finished");
	return true;
}

void GameCore::deinit()
{
	StateManager::getInstance().cleanUp();
	lbAudioSystem::instance().deinit();
	_driver->clear();
	lbInputSystem::getInstance().clear();
	Logger::flush();

	delete _gameState;
}

void GameCore::run()
{
	while(!_quit)
	{
		_prevFrameTime = _curFrameTime;
		_curFrameTime = _driver->getEllapsedMillis();
		_frameDif = _curFrameTime - _prevFrameTime;
		_frameDifSeconds = (float) _frameDif / 1000.0f;

		if(_isFPSLimitEnabled)
		{
			long idealFrameLength = 1000 / _targetFPS;
			if(idealFrameLength > _frameDif)
			{
				_driver->wait(idealFrameLength - _frameDif);
			}
		}

		_driver->handleEvents();

		lbInputSystem::getInstance().update(_curFrameTime);
		getStateManager().update();
		lbAudioSystem::instance().update();
		_driver->beginFrame();
		getStateManager().startFrame();

		//rendering code here
		getStateManager().renderFrame();

		getStateManager().endFrame();
		_driver->finalizeFrame();
	}
}

void GameCore::warpMouse(int x, int y)
{
	_driver->warpMouse(x,y);
}

void GameCore::enableFPSLimit(int targetFPS)
{
	_isFPSLimitEnabled = true;
	if(targetFPS>=1)
	{
		_targetFPS = targetFPS;
	}
	else
	{
		_targetFPS = 1;
	}
}

void GameCore::handleMouseMotionEvent(SDL_MouseMotionEvent& event)
{
	lbInputSystem::getInstance().handleSDLMouseMotionEvent(event);
}

void GameCore::handleMouseButtonEvent(SDL_MouseButtonEvent& event)
{
	lbInputSystem::getInstance().handleSDLMouseButtonEvent(event);
}

void GameCore::handleMouseWheelEvent(SDL_MouseWheelEvent& event)
{
	lbInputSystem::getInstance().handleSDLMouseWheelEvent(event);
}

void GameCore::handleKeyboardEvent(SDL_Event &event)
{
	lbInputSystem::getInstance().handleSDLKeyboardEvent(event.key);
}

