/*
 * GameEntity.cpp
 *
 *  Created on: 23-08-2013
 *      Author: Spliter
 */

#include <game/Constants.h>
//#include <game/GameContext.h>
#include <game/GameEntity.h>
#include <game/level/GameLevel.h>
#include <game/level/Map.h>
#include <game/states/GameState.h>
#include <lbTypes.h>
#include <math/lbBaseMath.h>
#include <math/util/lbRect.h>
#include <utils/debug/debug.h>
#include <utils/file/TinyXml/tinyxml.h>
#include <cctype>
#include <cfloat>
#include <cstdio>
#include <cstdlib>


#define INFORM_PARENT_FUNC_CALLED(enumVal)\
	if(_parentFuncRequested==enumVal){_parentFuncCalled=enumVal;}

static bool iequals(const std::string& a, const std::string& b)
{
	unsigned int sz = a.size();
	if (b.size() != sz)
		return false;
	for (unsigned int i = 0; i < sz; ++i)
		if (tolower(a[i]) != tolower(b[i]))
			return false;
	return true;
}

static TiXmlElement* findProperty(TiXmlElement* rootElement, std::string propertyName)
{
	TiXmlElement* properties = rootElement->FirstChildElement("properties");
	if (properties)
	{
		TiXmlElement* property = properties->FirstChildElement("property");
		while (property)
		{
			std::string name;
			int result = property->QueryStringAttribute("name", &name);
			if (result == TIXML_SUCCESS && name == propertyName)
			{
				return property;
			}
			property = property->NextSiblingElement("property");
		}
	}
	return NULL;
}

bool GameEntity::parseBoolProperty(TiXmlElement* element, std::string propertyName, bool* retValue)
{
	LogEntryPoint();
	TiXmlElement* property = findProperty(element, propertyName);
	if (property)
	{
		std::string value;
		if (property->QueryStringAttribute("value", &value) == TIXML_SUCCESS)
		{
			(*retValue) = iequals(value, std::string("true"));
			return true;
		}
	}
	return false;
}

bool GameEntity::parseIntProperty(TiXmlElement* element, std::string propertyName, int* retValue)
{
	LogEntryPoint();
	TiXmlElement* property = findProperty(element, propertyName);
	if (property)
	{
		return property->QueryIntAttribute("value", retValue) == TIXML_SUCCESS;
	}
	return false;
}

bool GameEntity::parseFloatProperty(TiXmlElement* element, std::string propertyName, float* retValue)
{
	LogEntryPoint();
	TiXmlElement* property = findProperty(element, propertyName);
	if (property)
	{
		return property->QueryFloatAttribute("value", retValue) == TIXML_SUCCESS;
	}
	return false;
}

bool GameEntity::parseStringProperty(TiXmlElement* element, std::string propertyName, std::string* retValue)
{
	LogEntryPoint();
	TiXmlElement* property = findProperty(element, propertyName);
	if (property)
	{
		return property->QueryStringAttribute("value", retValue) == TIXML_SUCCESS;
	}
	return false;
}

GameEntity::GameEntity(GameContext* gameContext) :
				_parentFuncCalled(None),
				_parentFuncRequested(None),
				_id(0),
				_uniqueId(0),
				_isKilled(false),
				_prev(NULL),
				_next(NULL),
				_pos(0, 0),
				_name(""),
				_gameContext(gameContext)
{
}

GameEntity::~GameEntity()
{
}

GameLevel* GameEntity::getLevel()
{
	return _gameContext->gameState->getCurrentLevel();
}

void GameEntity::onCreated()
{
	LogEntryPoint();
	INFORM_PARENT_FUNC_CALLED(Create);
}

void GameEntity::onSetupFromXmlElement(TiXmlElement* element)
{
	LogEntryPoint();
	INFORM_PARENT_FUNC_CALLED(Setup);

	int x = 0, y = 0;
	element->QueryIntAttribute("x", &x);
	element->QueryIntAttribute("y", &y);
	element->QueryStringAttribute("name", &_name);

	int gid = 0;
	element->QueryIntAttribute("gid", &gid);
	//if we have a tile assigned then we must compensate for the fact that tiled entities in TilEd have a lower-left corner as the origin
	if (gid != 0)
	{
		y -= TILE_IMPORT_SIZE; //we must compensate for the fact that someone decided the origin of objects in TilEd is in lower left corner than the upper left
	}
	/*
	 * note: we adjust the position by half of the tile size
	 * so the middle of the icon in TilEd determines the cell
	 * the object is in and not the top left corner
	 */
	float worldScale = (float) TILE_SIZE / (float) TILE_IMPORT_SIZE;
	setPosition(lbVec2f(x * worldScale, y * worldScale));

	//	parseFloatProperty(element,"x",&_pos.x);
	//	parseFloatProperty(element,"y",&_pos.y);
	//	parseStringProperty(element,"name",&_name);
	//	_pos.x*=32;
	//	_pos.y*=32;
}

void GameEntity::onSpawned()
{
	LogEntryPoint();
	INFORM_PARENT_FUNC_CALLED(Spawn);
	if(!getLayer())
	{
		GameLevel* level = getGameContext()->gameState->getCurrentLevel();
		if(level)
		{
			addToLayer(&level->characterLayer);
		}
	}
}

void GameEntity::onKilled()
{
	LogEntryPoint();
	INFORM_PARENT_FUNC_CALLED(Kill);
	removeFromLayer();
}

void GameEntity::onDestroy()
{
	LogEntryPoint();
	INFORM_PARENT_FUNC_CALLED(Destroy);
}

void GameEntity::callCreate()
{
	LogEntryPoint();
	_parentFuncCalled = GameEntity::None;
	_parentFuncRequested = GameEntity::Create;
	onCreated();
	lbAssertMsg(_parentFuncCalled==GameEntity::Create, std::string("You must call the parent's onCreated() function in "+getClass()->getClassName()).c_str());
}

void GameEntity::callSetup(TiXmlElement* element)
{
	LogEntryPoint();
	_parentFuncCalled = GameEntity::None;
	_parentFuncRequested = GameEntity::Setup;
	onSetupFromXmlElement(element);
	lbAssertMsg(_parentFuncCalled==GameEntity::Setup, std::string("You must call the parent's onSetupFromXmlElement() function in "+getClass()->getClassName()).c_str());
}

void GameEntity::callSpawn()
{
	LogEntryPoint();
	_parentFuncCalled = GameEntity::None;
	_parentFuncRequested = GameEntity::Spawn;
	onSpawned();
	lbAssertMsg(_parentFuncCalled==GameEntity::Spawn, std::string("You must call the parent's onSpawned() function in "+getClass()->getClassName()).c_str());
}

void GameEntity::callKill()
{
	LogEntryPoint();
	_parentFuncCalled = GameEntity::None;
	_parentFuncRequested = GameEntity::Kill;
	onKilled();
	lbAssertMsg(_parentFuncCalled==GameEntity::Kill, std::string("You must call the parent's onKilled() function in "+getClass()->getClassName()).c_str());
}

void GameEntity::callDestroy()
{
	LogEntryPoint();
	_parentFuncCalled = GameEntity::None;
	_parentFuncRequested = GameEntity::Destroy;
	onDestroy();
	lbAssertMsg(_parentFuncCalled==GameEntity::Destroy, std::string("You must call the parent's onDestroy() function in "+getClass()->getClassName()).c_str());
}

int GameEntity::castBlockHorizontal(float dx, lbRectF rect, Map* map, int flags, int neededFlags, float& movedDistance)
{
	LogEntryPoint();
	if (dx > 0)
	{
		int minX = maxi(rect.x2 / TILE_SIZE, 0);
		int maxX = mini((rect.x2 + dx) / TILE_SIZE, map->width - 1);
		int minY = maxi((rect.y1 + 1) / TILE_SIZE, 0);
		int maxY = mini((rect.y2 - 1) / TILE_SIZE, map->height - 1);

		for (int x = minX; x <= maxX; x++)
		{
			for (int y = minY; y <= maxY; y++)
			{
				int tileFlags = map->getTile(x, y).tileFlags;
				if ((tileFlags & flags) || (tileFlags & neededFlags) != neededFlags)
				{
					float tileX = x * TILE_SIZE;
					if (rect.x2 + dx <= tileX)
					{
						movedDistance = dx;
						return 0;
					}
					else if (rect.x2 - 1 <= tileX)
					{
						movedDistance = (tileX - rect.x2);
						return 1;
					}
				}
			}
		}
	}
	else if (dx < 0)
	{
		int minX = maxi((rect.x1 + dx) / TILE_SIZE, 0);
		int maxX = mini((rect.x1) / TILE_SIZE, map->width - 1);
		int minY = maxi((rect.y1 + 1) / TILE_SIZE, 0);
		int maxY = mini((rect.y2 - 1) / TILE_SIZE, map->height - 1);

		for (int x = maxX; x >= minX; x--)
		{
			for (int y = minY; y <= maxY; y++)
			{
				int tileFlags = map->getTile(x, y).tileFlags;
				if ((tileFlags & flags) || (tileFlags & neededFlags) != neededFlags)
				{
					float tileX = x * TILE_SIZE + TILE_SIZE - 1;
					if (rect.x1 + dx > tileX)
					{
						movedDistance = dx;
						return 0;
					}
					else if (rect.x1 >= tileX)
					{
						movedDistance = (tileX - rect.x1);
						return -1;
					}
				}
			}
		}
	}
	movedDistance = dx;
	return 0;
}

int GameEntity::castBlockVertical(float dy, lbRectF rect, Map* map, int flags, int neededFlags, float& movedDistance)
{
	LogEntryPoint();
	if (dy > 0)
	{
		int minX = maxi((rect.x1 + 1) / TILE_SIZE, 0);
		int maxX = mini((rect.x2 - 1) / TILE_SIZE, map->width - 1);
		int minY = maxi((rect.y2) / TILE_SIZE, 0);
		int maxY = mini((rect.y2 + dy) / TILE_SIZE, map->height - 1);

		for (int y = minY; y <= maxY; y++)
		{
			for (int x = minX; x <= maxX; x++)
			{
				int tileFlags = map->getTile(x, y).tileFlags;
				if ((tileFlags & flags) || (tileFlags & neededFlags) != neededFlags)
				{
					float tileY = y * TILE_SIZE;
					if (rect.y2 + dy <= tileY)
					{
						movedDistance = dy;
						return 0;
					}
					else if (rect.y2 - 1 <= tileY)
					{
						movedDistance = (tileY - rect.y2);
						return 1;
					}
				}
			}
		}
	}
	else if (dy < 0)
	{
		int minX = maxi((rect.x1 + 1) / TILE_SIZE, 0);
		int maxX = mini((rect.x2 - 1) / TILE_SIZE, map->width - 1);
		int minY = maxi((rect.y1 + dy) / TILE_SIZE - 1, 0);
		int maxY = mini((rect.y1 - 2) / TILE_SIZE, map->height - 1);

		for (int y = maxY; y >= minY; y--)
		{
			for (int x = minX; x <= maxX; x++)
			{
				int tileFlags = map->getTile(x, y).tileFlags;
				if ((tileFlags & flags) || (tileFlags & neededFlags) != neededFlags)
				{
					float tileY = y * TILE_SIZE + TILE_SIZE;
					if (rect.y1 + dy > tileY)
					{
						movedDistance = dy;
						return 0;
					}
					else if (rect.y1 >= tileY)
					{
						movedDistance = (tileY - rect.y1);
						return -1;
					}
				}
			}
		}
	}
	movedDistance = dy;
	return 0;
}

bool GameEntity::checkCollisionAgainstMap(lbRectF rect, Map* map, int flags, int neededFlags)
{
	LogEntryPoint();
	int minX = maxi((rect.x1 + 1) / TILE_SIZE, 0);
	int maxX = mini((rect.x2 - 1) / TILE_SIZE, map->width - 1);
	int minY = maxi((rect.y1 + 1) / TILE_SIZE - 1, 0);
	int maxY = mini((rect.y2 - 1) / TILE_SIZE, map->height - 1);

	for (int y = minY; y <= maxY; y++)
	{
		for (int x = minX; x <= maxX; x++)
		{
			int tileFlags = map->getTile(x, y).tileFlags;
			if ((tileFlags & flags) || (tileFlags & neededFlags) != neededFlags)
			{
				return true;
			}
		}
	}
	return false;
}

void getProjection(lbRectF rect, lbVec2f origin, lbVec2f normal, float& _min, float& _max)
{
	LogEntryPoint();
	_min = FLT_MAX;
	_max = -FLT_MAX;

	lbVec2f corner[4];
	corner[0].set(rect.x1, rect.y1);
	corner[1].set(rect.x2, rect.y1);
	corner[2].set(rect.x1, rect.y2);
	corner[3].set(rect.x2, rect.y2);

	for (int i = 0; i < 4; i++)
	{
		float d = normal.dot(corner[i] - origin);
		_min = minf(_min, d);
		_max = maxf(_max, d);
	}
}

bool checkCollisionAgainstTile(lbVec2f src, lbVec2f normal, int x, int y, Map* map, uint32 flags, uint32 neededFlags)
{
	LogEntryPoint();
	Tile* tile = map->getTileRef(x, y);
	if (tile && ((tile->tileFlags & flags) || ((tile->tileFlags & neededFlags) != neededFlags)))
	{
		lbRectF cell(x * TILE_SIZE, y * TILE_SIZE, x * TILE_SIZE + TILE_SIZE, y * TILE_SIZE + TILE_SIZE);
		float min = 0;
		float max = 0;
		getProjection(cell, src, normal, min, max);

		if (inRange(min, max, 0))
		{
			return true;
		}
	}
	return false;
}

bool GameEntity::checkCollisionLineAgainstMap(lbVec2f src, lbVec2f dst, Map* map, int flags, int neededFlags)
{
	LogEntryPoint();
	//TODO implement a more optimized algorithm for this that only checks the necesary tiles

	int minX = minf(src.x, dst.x) / TILE_SIZE;
	int minY = minf(src.y, dst.y) / TILE_SIZE;

	int maxX = maxf(src.x, dst.x) / TILE_SIZE;
	int maxY = maxf(src.y, dst.y) / TILE_SIZE;

	lbVec2f normal = (dst - src).getLeftPerpendicular().normalized();

	for (int x = minX; x <= maxX; x++)
	{
		for (int y = minY; y <= maxY; y++)
		{
			if (checkCollisionAgainstTile(src, normal, x, y, map, flags, neededFlags))
			{
				return true;
			}
		}
	}
	return false;
}

