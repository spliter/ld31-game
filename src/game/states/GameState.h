/*
 * SpaceState.h
 *
 *  Created on: 13-09-2012
 *      Author: Spliter
 */

#pragma once

#include <game/GameContext.h>
#include <game/GameEntity.h>
#include <game/level/GameLevel.h>
#include <states/State.h>
#include <ui/lbTextBox.h>
#include <ui/UiElementGroup.h>
#include <utils/entities/lbEntityManager.h>
#include <utils/graphics/basic/glCamera.h>
#include <utils/graphics/drawable/lbDrawableLayer.h>
#include <utils/Object.h>
#include <sstream>
#include <string>


//#TODO(miko) put the level loading, update and render code into separate class

class GameCore;
class FTFont;
class GameEntity;
class lbTileMap;
class lbSprite;

typedef lbEntityManager<GameEntity> GameEntityManager;

class GameState: public State, public lbGameEventListener
{
	DEFINE_CLASS(GameState,State);
public:
	GameState(GameCore *core);
	virtual ~GameState();

	virtual void onStart();
	virtual void onUpdate();
	virtual void onFrameRender();
	virtual void onEnd();

	virtual void onMouseMotionEvent(lbMouseMotionEvent* event);
	virtual void onMouseButtonEvent(lbMouseButtonEvent* event);
	virtual void onMouseWheelEvent(lbMouseWheelEvent* event);
	virtual void onKeyboardEvent(lbKeyboardEvent* event);

	glCamera* getUICamera(){return &_uiCamera;}
	lbDrawableLayer *getUILayer() {return &_uiLayer;}
	GameEntity* spawnEntity(const std::string& type);

	GameEntity* findEntityByName(std::string name) const;
	GameEntity* getEntity(int id) const;
	GameEntity* getEntity(int id, int uniqueId) const;
	GameEntity* getEntityUid(int uniqueId) const;
	GameEntityManager* getEntityManager(){return _currentLevel?&_currentLevel->entManager:NULL;}

	GameContext* getGameContext(){return &_gameContext;}

	GameLevel* getCurrentLevel(){return _currentLevel;}
	FTFont* getBigFont(){return _bigFont;}
	UiElementGroup* getUIRoot(){return &_uiRoot;}

	std::stringstream& getDebugTextStream(){return _debugStream;}

	void loadLevel(std::string levelName);
private:
	//game systems
	GameCore* _gameCore;
	GameLevel* _currentLevel;

	unsigned long _curGameTime;
	GameContext _gameContext;

	long _curSecondStart;
	int _framesEllapsedInCurSecond;
	float _fps;
	float _gameSpeed;

	bool _hasLevelToLoad;
	std::string _levelToLoad;

	//ui
	UiElementGroup _uiRoot;
	lbTextBox _fpsTextBox;
	FTFont* _messageFont;
	FTFont* _bigFont;
	lbDrawableLayer _uiLayer;
	glCamera _uiCamera;

	//debug
	std::stringstream _debugStream;

};
