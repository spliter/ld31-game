/*
 * GameEntityFactoryManager.cpp
 *
 *  Created on: 05-04-2013
 *      Author: Spliter
 */

#include <game/GameEntity.h>
#include <utils/debug/debug.h>
#include <utils/debug/Logger.h>
#include "GameEntityFactoryManager.h"
#include <iostream>
#include <map>
#include <string>
#include <utility>

namespace GameEntityFactories
{
	typedef std::map<std::string, GameEntityFactory*>::iterator FactoryIterator_t;

	std::map<std::string, GameEntityFactory*> & factories()
	{
		static std::map<std::string, GameEntityFactory*> factories;
		return factories;
	}

	bool registerFactory(GameEntityFactory* factory, std::string entityName)
	{
		LogEntryPoint();
		if(!factory)
			return false;
		if(factories().insert(std::pair<std::string, GameEntityFactory*> (entityName, factory)).second)
		{
			return true;
		}
		Logger::getBuffer() << "This factory name has already been used before \"" << entityName << "\"\n";
		return false;
	}

	GameEntityFactory* findFactory(const std::string &entName)
	{
		LogEntryPoint();
		FactoryIterator_t it = factories().find(entName);
		if(it != factories().end())
		{
			return it->second;
		}
		return NULL;
	}
}
