/*
 * CollisionUtility.h
 *
 *  Created on: 19 sty 2014
 *      Author: spliter
 */

#pragma once
#include <vector>
#include <math/lbVec2.h>

typedef std::pair<lbVec2f, int> vertPair;
/*
 * Checks which order are the vertices (if clockwise or counter clockwise) and copies them so they are counter clockwise
 */
void copyVerticesWithCorrectOrientation(std::vector<lbVec2f>& dst, std::vector<lbVec2f>& src);

bool isInsideTriangle(lbVec2f p0, lbVec2f p1, lbVec2f p2, lbVec2f p);
bool isEar(int index, std::vector<vertPair>& verts);
bool runEarClippingAlgorithm(std::vector<vertPair>& verts, std::vector<int> & retTriangles);
