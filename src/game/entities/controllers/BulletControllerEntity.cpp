/*
 * BulletControllerEntity.cpp
 *
 *  Created on: 4 gru 2014
 *      Author: Miko Kuta
 */

#include <game/collisions/CollisionPoly.h>
#include <game/entities/controllers/BulletControllerEntity.h>
#include <game/entities/monsters/MonsterEntity.h>
#include <game/entities/SolidEntity.h>
#include <game/entities/SolidRectangleEntity.h>
#include <game/GameEntityFactoryManager.h>
#include <game/level/GameLevel.h>
#include <Glee.h>
#include <math/lbVec2.h>
#include <utils/graphics/lbTexture.h>
#include <iostream>
#include <list>

class lbCameraSceneNode2D;

REGISTER_GAME_ENTITY(BulletControllerEntity, "bullet_controller");

BulletControllerEntity::BulletControllerEntity(GameContext* context) :
		GameEntity(context),_freeIdHead(MAX_BULLET_NUM)
{

}

BulletControllerEntity::~BulletControllerEntity()
{
}

void BulletControllerEntity::onCreated()
{
	super::onCreated();
	resetBullets();
}

void BulletControllerEntity::onSpawned()
{
	super::onSpawned();
}

void BulletControllerEntity::onUpdate(float dt)
{
	for (int i = 0; i < MAX_BULLET_NUM; i++)
	{
		//check every bullet's collision against every monster....
		if (_bullets[i].isALive)
		{
			updateBullet(i,dt);
		}
	}
}

void BulletControllerEntity::draw(lbCameraSceneNode2D* camera)
{
	lbTexture::unbind();
	glLineWidth(3);
	glBegin(GL_LINES);
	for (int i = 0; i < MAX_BULLET_NUM; i++)
	{
		if(_bullets[i].isALive)
		{

			glColor3f(1,1,1);
			glVertex2fv(_bullets[i].pos.me);
			glColor4f(1,1,1,0.5f);
			glVertex2fv(_bullets[i].prevPos.me);
		}
	}
	glEnd();
	glLineWidth(1);
}

void BulletControllerEntity::spawnBullet(lbVec2f pos, lbVec2f movement, float damageScale, float decaySpeed, float decayCollision, float timeToLive)
{
	if (_freeIdHead < MAX_BULLET_NUM)
	{
		int usedId = _freeIdHead;
		_freeIdHead = _bullets[_freeIdHead].nextFreeId;
		_bullets[usedId].create(pos,movement,damageScale, decaySpeed,decayCollision,timeToLive);
	}
}

void BulletControllerEntity::resetBullets()
{
	_freeIdHead = 0;
	for (int i = 0; i < MAX_BULLET_NUM; i++)
	{
		_bullets[i].isALive = false;
		_bullets[i].nextFreeId = i + 1;
	}
}
void BulletControllerEntity::killBullet(int bullet)
{
	if (bullet < MAX_BULLET_NUM && _bullets[bullet].isALive)
	{
		_bullets[bullet].isALive = false;
		_bullets[bullet].nextFreeId = _freeIdHead;
		_freeIdHead = bullet;
	}
	else
	{
		std::cout << "Warning! Tried to kill bullet either outside of range, or one that is already dead\n";
	}
}

struct MonsterHitResult
{
	MonsterEntity* monster;
	float minTime;
	lbVec2f collisionPosition;
	MonsterHitResult(MonsterEntity* monster,
					float minTime,

					lbVec2f collisionPosition)
	{
		this->monster=monster;
		this->minTime=minTime;

		this->collisionPosition=collisionPosition;
	}
	MonsterHitResult(const MonsterHitResult& other)
	{
		this->monster=other.monster;
		this->minTime=other.minTime;

		this->collisionPosition=other.collisionPosition;
	}
};

bool compareMonsterHitResult(const MonsterHitResult& first, const MonsterHitResult& second)
{
	return first.minTime<second.minTime;
}

void BulletControllerEntity::updateBullet(int bulletId, float dt)
{
	Bullet &bullet = _bullets[bulletId];
	bullet.movement -= bullet.movement * bullet.decaySpeed * dt;
	bullet.timeToLive -= dt;
	bullet.prevPos = bullet.pos;
	if (bullet.timeToLive < 0)
	{
		killBullet(bulletId);
		return;
	}

	lbVec2f nextPos = bullet.pos + bullet.movement * dt;

	SolidEntity::solidList solids = getLevel()->solidGeometryWorld.getSolids();
	SolidEntity::solidIter iter = solids.begin();
	while (iter != solids.end())
	{
		SolidEntity* ent = (*iter);
		if (ent->instanceOf(SolidRectangleEntity::getClassDef()))
		{
			CollisionPoly** polys = ent->getCollisionPolygons();
			int polyNum = ent->getCollisionPolyNum();
			for (int i = 0; i < polyNum; i++)
			{
				float intersectionTime = 0;
				lbVec2f reflectAxis = 0;
				lbVec2f collisionPoint;
				bool collided = CollisionPoly::checkRayCast(bullet.pos, nextPos, *polys[i], intersectionTime, reflectAxis,collisionPoint);
				if (collided)
				{
					nextPos = (nextPos-bullet.pos)*intersectionTime+bullet.pos+reflectAxis;
					bullet.collided = true;
					bullet.movement = bullet.movement+-reflectAxis*(bullet.movement.dot(reflectAxis))*2;
					bullet.movement-=bullet.movement*bullet.decayCollision;
				}
			}
		}
		iter++;
	}

	std::list<MonsterHitResult> hitResults;
	SolidEntity::solidList enemies = getLevel()->enemiesWorld.getSolids();
	iter = enemies.begin();
	while (iter != enemies.end())
	{
		SolidEntity* ent = (*iter);
		if (ent->instanceOf(MonsterEntity::getClassDef()))
		{
			MonsterEntity* monster = MonsterEntity::castEntity(ent);
			CollisionPoly** polys = ent->getCollisionPolygons();
			int polyNum = ent->getCollisionPolyNum();
			for (int i = 0; i < polyNum; i++)
			{
				float intersectionTime = 0;
				lbVec2f reflectAxis;
				lbVec2f collisionPoint;
				bool collided = CollisionPoly::checkRayCast(bullet.pos, nextPos, *polys[i], intersectionTime, reflectAxis, collisionPoint);
				if (collided)
				{
					hitResults.push_back(MonsterHitResult(monster,intersectionTime,collisionPoint));
//					monster->onHitByBullet(collisionPoint,bullet.movement,bullet.movement.length()*bullet.damageScale);
				}
			}
		}
		iter++;
	}

	hitResults.sort(compareMonsterHitResult);
	std::list<MonsterHitResult>::iterator hitIter = hitResults.begin();
	while(hitIter!=hitResults.end())
	{
		int damage = bullet.movement.length()*bullet.damageScale;
		hitIter->monster->onHitByBullet(hitIter->collisionPosition,bullet.movement,damage);
		bullet.movement*=0.9;
		hitIter++;
	}

	bullet.pos = nextPos;

}
