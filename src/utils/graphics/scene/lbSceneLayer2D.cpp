/*
 * lbSceneLayer2D.cpp
 *
 *  Created on: 2 mar 2014
 *      Author: spliter
 */

#include <stddef.h>
#include <utils/graphics/scene/lbSceneLayer2D.h>
//#include <iostream>

lbSceneLayer2D::lbSceneLayer2D()
{
}

lbSceneLayer2D::~lbSceneLayer2D()
{
	std::list<lbSceneNode2D*>::iterator it = _drawables.begin();
	while(it != _drawables.end())
	{
		(*it)->_layer=NULL;
		it++;
	}
}

void lbSceneLayer2D::addNode(lbSceneNode2D* drawable)
{
	if(drawable)
	{
		if(drawable->_layer)
		{
			drawable->_layer->removeNode(drawable);
		}
		drawable->_layer=this;
		_drawables.push_back(drawable);
	}
}

void lbSceneLayer2D::removeNode(lbSceneNode2D* drawable)
{
	if(drawable->_layer == this)
	{
		drawable->_layer = NULL;
		_drawables.remove(drawable);
	}
}

void lbSceneLayer2D::clearNodes()
{
	std::list<lbSceneNode2D*>::iterator it = _drawables.begin();
	while(it != _drawables.end())
	{
		(*it)->_layer=NULL;
		it++;
	}
	_drawables.clear();
}

void lbSceneLayer2D::draw(lbCameraSceneNode2D* camera)
{
	_drawables.sort(sceneDepthComparison);
	std::list<lbSceneNode2D*>::iterator it = _drawables.begin();
	while(it != _drawables.end())
	{
		//TODO: optimize
		if((*it)->isVisible())
		{
			(*it)->draw(camera);
		}
		it++;
	}
}

