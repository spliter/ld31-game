/*
 * CollisionPoly.cpp
 *
 *  Created on: 7 gru 2013
 *      Author: spliter
 */

#include <game/collisions/CollisionPoly.h>
#include <utils/debug/debug.h>
#include <cmath>

void CollisionPoly::getCastRange(const std::vector<lbVec2f>& verts, const lbVec2f& castAxis, const lbVec2f& castOrigin, float& retMin, float& retMax)
{
	float min = castAxis.dot(verts[0] - castOrigin);
	float max = min;

	for (int i = 1; i < (int) verts.size(); i++)
	{
		float d = castAxis.dot(verts[i] - castOrigin);
		if (d < min)
		{
			min = d;
		}
		else if (d > max)
		{
			max = d;
		}
	}
	retMin = min;
	retMax = max;
}

bool CollisionPoly::IsColliding(CollisionPoly& a, CollisionPoly&b)
{
	LogEntryPoint();
	a.updatePointsIfNeeded();
	b.updatePointsIfNeeded();

	if (a.getTransformedAxes().empty() || b.getTransformedAxes().empty())
	{
		return false;
	}

	if (!(a.getTransformedBoundingBox().isIntersecting(b.getTransformedBoundingBox())))
	{
		return false;
	}

	const std::vector<lbVec2f>& aPoints = a.getTransformedConvexVerts();
	const std::vector<lbVec2f>& bPoints = b.getTransformedConvexVerts();
	const std::vector<lbVec2f>& aAxes = a.getTransformedAxes();
	const std::vector<lbVec2f>& bAxes = b.getTransformedAxes();

	for (int i = 0; i < (int) aAxes.size(); i++)
	{
		float min, max;
		getCastRange(bPoints, aAxes[i], aPoints[i], min, max);
		if (min > 0)
		{
			return false;
		}
	}

	for (int i = 0; i < (int) bAxes.size(); i++)
	{
		float min, max;
		getCastRange(aPoints, bAxes[i], bPoints[i], min, max);
		if (min > 0)
		{
			return false;
		}
	}

	return true;
}

bool CollisionPoly::GetSeparatingVector(CollisionPoly& a, CollisionPoly& b, lbVec2f& retSeparatingVector)
{
	LogEntryPoint();
	a.updatePointsIfNeeded();
	b.updatePointsIfNeeded();

	if (a.getTransformedAxes().empty() || b.getTransformedAxes().empty())
	{
		return false;
	}

	if (!(a.getTransformedBoundingBox().isIntersecting(b.getTransformedBoundingBox())))
	{
		return false;
	}

	const std::vector<lbVec2f>& aPoints = a.getTransformedConvexVerts();
	const std::vector<lbVec2f>& bPoints = b.getTransformedConvexVerts();
	const std::vector<lbVec2f>& aAxes = a.getTransformedAxes();
	const std::vector<lbVec2f>& bAxes = b.getTransformedAxes();

	float amin, amax;
	float bmin, bmax;

	retSeparatingVector.set(0, 0);

	getCastRange(aPoints, aAxes[0], aPoints[0], amin, amax);
	getCastRange(bPoints, aAxes[0], aPoints[0], bmin, bmax);

	if (bmax < amin || bmin > amax)
	{
		return false;
	}

	lbVec2f minSeparatingAxis = aAxes[0];

	float dif1 = bmax - amin;
	float dif2 = amax - bmin;
	float minSeparation = dif1 < dif2 ? dif1 : -dif2;

	for (int i = 1; i < (int) aAxes.size(); i++)
	{
		getCastRange(aPoints, aAxes[i], aPoints[i], amin, amax);
		getCastRange(bPoints, aAxes[i], aPoints[i], bmin, bmax);

		if (bmax < amin || bmin > amax)
		{
			return false;
		}
		else
		{
			float dif1 = bmax - amin;
			float dif2 = amax - bmin;
			float separation = dif1 < dif2 ? dif1 : -dif2;
			if (fabs(separation) < fabs(minSeparation))
			{
				minSeparatingAxis = aAxes[i];
				minSeparation = separation;
			}
		}
	}

	for (int i = 1; i < (int) bAxes.size(); i++)
	{
		getCastRange(aPoints, bAxes[i], bPoints[i], amin, amax);
		getCastRange(bPoints, bAxes[i], bPoints[i], bmin, bmax);

		if (bmax < amin || bmin > amax)
		{
			return false;
		}
		else
		{

			float dif1 = bmax - amin;
			float dif2 = amax - bmin;
			float separation = dif1 < dif2 ? dif1 : -dif2;

			if (fabs(separation) < fabs(minSeparation))
			{
				minSeparatingAxis = bAxes[i];
				minSeparation = separation;
			}
		}
	}

	retSeparatingVector = minSeparatingAxis * minSeparation;

	return true;
}

bool CollisionPoly::CastCollisionPoly(CollisionPoly& a, const lbVec2f& castVector, CollisionPoly&b, float& retCollisionTime, lbVec2f& retLastAxis)
{
	LogEntryPoint();
	retLastAxis.set(0, 0);
	retCollisionTime = 1.0f;

	a.updatePointsIfNeeded();
	b.updatePointsIfNeeded();

	if (a.getTransformedAxes().empty() || b.getTransformedAxes().empty() || castVector.squareLength() <= 0.0000001f)
	{
		return false;
	}

	lbRectF stretchedPolyBbox;
	CalculateStretchedBoundingBox(castVector, a.getTransformedBoundingBox(), stretchedPolyBbox);

	if (!stretchedPolyBbox.isIntersecting(b.getTransformedBoundingBox()))
	{
		return false;
	}

	const std::vector<lbVec2f>& aPoints = a.getTransformedConvexVerts();
	const std::vector<lbVec2f>& bPoints = b.getTransformedConvexVerts();
	const std::vector<lbVec2f>& aAxes = a.getTransformedAxes();
	const std::vector<lbVec2f>& bAxes = b.getTransformedAxes();

	float min, max;
	float amin, amax;

	getCastRange(bPoints, castVector.getLeftPerpendicular(), lbVec2f(0, 0), min, max);
	getCastRange(aPoints, castVector.getLeftPerpendicular(), lbVec2f(0, 0), amin, amax);

	if (amin > max || amax < min)
	{
		return false;
	}

	float castDist = 0;
	lbVec2f maxSeparatingAxis(0, 0);
	float maxTravelRatio = 0;

	for (int i = 0; i < (int) aAxes.size(); i++)
	{
		castDist = aAxes[i].dot(castVector);
		if (castDist > 0)
		{
			getCastRange(bPoints, aAxes[i], aPoints[i], min, max);
			if (min > castDist)
			{
				return false;
			}
			else if (min >= 0)
			{
				float travelRatio = 0;
				if (castDist != 0)
				{
					travelRatio = min / castDist;
				}

				if (travelRatio > maxTravelRatio)
				{
					maxSeparatingAxis = aAxes[i];
					maxTravelRatio = travelRatio;
				}
			}
		}
		else
		{
			getCastRange(bPoints, aAxes[i], aPoints[i], min, max);
			if (min > 0)
			{
				return false;
			}
		}
	}

	for (int i = 0; i < (int) bAxes.size(); i++)
	{
		castDist = bAxes[i].dot(-castVector);
		if (castDist > 0)
		{
			getCastRange(aPoints, bAxes[i], bPoints[i], min, max);
			if (min > castDist)
			{
				return false;
			}
			else if (min >= 0)
			{
				float travelRatio = 0;
				if (castDist != 0)
				{
					travelRatio = min / castDist;
				}

				if (travelRatio > maxTravelRatio)
				{
					maxSeparatingAxis = bAxes[i];
					maxTravelRatio = travelRatio;
				}
			}
		}
	}
//
//	for (int i = 0; i < bAxes.size(); i++)
//	{
//		float min, max;
//		getCastRange(aPoints, bAxes[i], bPoints[i], min, max);
//		if (min > 0)
//		{
//			return false;
//		}
//		else if (-min < maxSeparation)
//		{
//			minSeparatingAxis = -bAxes[i];
//			maxSeparation = -min;
//		}
//	}

	retCollisionTime = maxTravelRatio;
	retLastAxis = maxSeparatingAxis;
	return true;
}

bool CollisionPoly::checkRayCast(lbVec2f start, lbVec2f end, CollisionPoly& poly, float &retIntersectionTime, lbVec2f& retCollisionAxis, lbVec2f& retCollisionPoint)
{
	poly.updatePointsIfNeeded();
	if (poly.getTransformedAxes().empty())
	{
		return false;
	}

	lbVec2f ray = (end - start);
	lbVec2f normal = ray.normalized();
	lbRectF rect;
	rect.prepareForVertices();
	rect.addVertice(start);
	rect.addVertice(end);


	if (!rect.isIntersecting(poly.getTransformedBoundingBox()))
	{
		retIntersectionTime = 1.0f;
		return false;
	}

	float amin = 0, amax = ray.length();
	float bmin, bmax;

	const std::vector<lbVec2f>& bPoints = poly.getTransformedConvexVerts();
	const std::vector<lbVec2f>& bAxes = poly.getTransformedAxes();

	getCastRange(bPoints, normal, start, bmin, bmax);
	float rayLength = (amax - amin);

	if (bmin > amax || bmax < amin)
	{
		retIntersectionTime = 1.0f;
		return false;
	}

	bool startedOutside = false;
	for (int i = 0; i < (int) bAxes.size(); i++)
	{
		lbVec2f pos = start - bPoints[i];
		if (bAxes[i].dot(pos) > 0)
		{
			startedOutside = true;
			break;
		}
	}

	if (!startedOutside)
	{
		retCollisionAxis.set(0, 0);
		retIntersectionTime = 0;
		retCollisionPoint = start;
		return true;
	}

	lbVec2f p = start;
	lbVec2f r = end - start;
	lbVec2f reflectAxis(0, 0);
	float minTime = 1;
	bool foundOverlap = false;

	for (int i = 0; i < (int) bAxes.size(); i++)
	{
		int i1 = i;
		int i2 = (i + 1) % bPoints.size();

		lbVec2f pos = start - bPoints[i];
		if (bAxes[i].dot(pos) > 0)
		{
			lbVec2f q = bPoints[i1];
			lbVec2f s = bPoints[i2] - bPoints[i1];

			float rxs = r.cross(s);
			float qpxs = (q - p).cross(s);
			float qpxr = (q - p).cross(r);

			if (fabs(rxs) > FLT_EPSILON)
			{
				float intersectTime1 = qpxs / rxs;
				float intersectTime2 = qpxr / rxs;
				if (intersectTime1 >= 0 && intersectTime1 < 1.0f && intersectTime2 >= 0 && intersectTime2 < 1.0f)
				{
					foundOverlap = true;
					if (intersectTime1 < minTime)
					{
						minTime = intersectTime1;
						reflectAxis = bAxes[i];
					}
				}
			}
		}
	}

	if (foundOverlap)
	{
		retCollisionPoint = start+ray*minTime;
		retCollisionAxis = reflectAxis;
		retIntersectionTime = minTime;
		return true;
	}
	else
	{
		retCollisionAxis.set(0, 0);
		retIntersectionTime = 1;
		return false;
	}
}

CollisionPoly::CollisionPoly()
{
	LogEntryPoint();
	_isDirty = true;
	_isPointsDirty = true;
	_rotation = 0;
	_position.set(0, 0);
}

CollisionPoly::CollisionPoly(const CollisionPoly& other)
{
	LogEntryPoint();
	*this = other;
}

CollisionPoly::~CollisionPoly()
{
	LogEntryPoint();
}

void CollisionPoly::operator=(const CollisionPoly& other)
{
	LogEntryPoint();
	_isDirty = other._isDirty;
	_isPointsDirty = other._isPointsDirty;
	_rotation = other._rotation;
	_position = other._position;
	_transform = other._transform;
	_verts = other._verts;
	_convexVerts = other._convexVerts;
	_transformedConvexVerts = other._transformedConvexVerts;
	_transformedAxes = other._transformedAxes;
	_bbox = other._bbox;
	_transformedBBox = other._transformedBBox;
}

//#define ENSURE_POLYGON_CONVEX

void CollisionPoly::updatePointsIfNeeded()
{
	LogEntryPoint();
	if (_isDirty)
	{
		if (_isPointsDirty)
		{

#ifdef ENSURE_POLYGON_CONVEX
			int usedPoints = 0;
			int leftmostId = 0;
			float leftmostCoord = _verts[0].x;
			for (int i = 1; i < _verts.size(); i++)
			{
				if(_verts[i].x<leftmostCoord)
				{
					leftmostCoord=_verts[i].x;
					leftmostId = i;
				}
			}

			int count = _verts.size()-1;
			_convexVerts.reserve(_verts.size());
			_convexVerts.resize(0);
			_convexVerts.push_back(_verts[leftmostCoord]);

			int curIndex = leftmostId;
			do
			{

			}while(curIndex!=leftmostId);

#else
			_convexVerts = _verts;
			_transformedConvexVerts.resize(_convexVerts.size());
#endif
			CalculateBoundingBox(_convexVerts, _bbox);

			if (_transformedConvexVerts.size() > 1)
			{
				_transformedAxes.resize(_transformedConvexVerts.size());
			}
			else
			{
				_transformedAxes.resize(0);
			}
			_isPointsDirty = false;
		}

		_transform = _parentTransform;
		_transform.translate(_position.x, _position.y);
		_transform.rotate(_rotation);

		for (int i = 0; i < (int) _convexVerts.size(); i++)
		{
			_transformedConvexVerts[i] = _transform * _convexVerts[i];
		}

		CalculateTransformedBoundingBox(_transform, _bbox, _transformedBBox);
		for (int i = 0; i < (int) _transformedAxes.size(); i++)
		{
			int i2 = (i + 1) % _transformedConvexVerts.size();
			_transformedAxes[i] = -(_transformedConvexVerts[i2] - _transformedConvexVerts[i]).makeLeftPerpendicular().normalize();
		}

		_isDirty = false;
	}
}

void CollisionPoly::makeRectangle(float x1, float y1, float x2, float y2)
{
	LogEntryPoint();
	_isDirty = true;
	_isPointsDirty = true;
	resizeVertsVector(4);
	_verts[0].set(x1, y1);
	_verts[1].set(x1, y2);
	_verts[2].set(x2, y2);
	_verts[3].set(x2, y1);
}

void CollisionPoly::makeCircle(float x, float y, float radius, int vertNum)
{
	LogEntryPoint();
	_isDirty = true;
	_isPointsDirty = true;
	resizeVertsVector(vertNum);
	float invAngle = 1.0f / vertNum * 2.0f * M_PI;
	for (int i = 0; i < vertNum; i++)
	{
		float angle = (float) i * invAngle;
		float px = x + cos(angle) * radius;
		float py = y + sin(angle) * radius;
		_verts[i].set(px, py);
	}
}

void CollisionPoly::CalculateBoundingBox(const std::vector<lbVec2f>& convexVerts, lbRectF& retBBox)
{
	LogEntryPoint();
	if (!convexVerts.empty())
	{
		float minx, miny, maxx, maxy;
		minx = maxx = convexVerts[0].x;
		miny = maxy = convexVerts[0].y;

		for (int i = 1; i < (int) convexVerts.size(); i++)
		{
			float x = convexVerts[i].x;
			float y = convexVerts[i].y;

			if (minx > x)
				minx = x;
			else if (maxx < x)
				maxx = x;

			if (miny > y)
				miny = y;
			else if (maxy < y)
				maxy = y;
		}
		retBBox.set(minx, miny, maxx, maxy);
	}
	else
	{
		retBBox.set(0, 0, 0, 0);
	}
}

#define UPDATE_BOUNDS(vertx,verty,minx,miny,maxx,maxy)\
	if (minx > (vertx))\
		minx = (vertx);\
	else if (maxx < (vertx))\
		maxx = (vertx);\
	if (miny > (verty))\
		miny = (verty);\
	else if (maxy < (verty))\
		maxy = (verty);

void CollisionPoly::CalculateTransformedBoundingBox(const lbMatrix3x3f& mat, const lbRectF& bbox, lbRectF& retTransformedBBox)
{
	LogEntryPoint();
	float minx, miny, maxx, maxy;
	lbVec2f vec;

	vec = mat * lbVec2f(bbox.x1, bbox.y1);
	minx = maxx = vec.x;
	miny = maxy = vec.y;

	vec = mat * lbVec2f(bbox.x1, bbox.y2);
	UPDATE_BOUNDS(vec.x, vec.y, minx, miny, maxx, maxy);

	vec = mat * lbVec2f(bbox.x2, bbox.y1);
	UPDATE_BOUNDS(vec.x, vec.y, minx, miny, maxx, maxy);

	vec = mat * lbVec2f(bbox.x2, bbox.y2);
	UPDATE_BOUNDS(vec.x, vec.y, minx, miny, maxx, maxy);

	retTransformedBBox.set(minx, miny, maxx, maxy);
}

void CollisionPoly::CalculateStretchedBoundingBox(const lbVec2f& stretchVector, const lbRectF& bbox, lbRectF& retStretchedBBox)
{
	LogEntryPoint();
	lbRectF auxRect = bbox.moved(stretchVector.x, stretchVector.y);
	retStretchedBBox = lbRectF::getTotalRange(auxRect, bbox);
}
