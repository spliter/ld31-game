/*
 * DebugCameraEntity.h
 *
 *  Created on: 4 gru 2014
 *      Author: Miko Kuta
 */

#pragma once

#include <game/collisions/CollisionPoly.h>
#include <game/entities/SolidEntity.h>
#include <utils/Object.h>

class lbCameraSceneNode2D;

class SolidRectangleEntity: public SolidEntity
{
public:
	DEFINE_CLASS(SolidRectangleEntity,SolidEntity);

	SolidRectangleEntity(GameContext* context);
	virtual ~SolidRectangleEntity();
	virtual void onSetupFromXmlElement(TiXmlElement* element);
	virtual void onSpawned();
	virtual void onDestroy();
	virtual void draw(lbCameraSceneNode2D* camera);

	virtual CollisionPoly** getCollisionPolygons(){return _colliders;}
	virtual int getCollisionPolyNum(){return 1;}

	virtual void setPosition(float x, float y){super::setPosition(x,y);_collider.setPosition(getPosition());}
	virtual void setPosition(lbVec2f newPos){setPosition(newPos.x,newPos.y);}

	float getWidth(){return _width;}
	float getHeight(){return _height;}

	virtual void markTransformDirty();
private:
	float _width, _height;
	CollisionPoly _collider;
	CollisionPoly* _colliders[1];
};

