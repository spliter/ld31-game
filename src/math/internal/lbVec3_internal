/*
 * lbVec3_internal.h
 *
 *  Created on: 3 gru 2014
 *      Author: Miko Kuta
 */

#include "../lbBaseMath.h"
#include <ostream>

class LB_CLASS_NAME
{
public:
	union
	{
		LB_PARAM_TYPE me[3];
		struct
		{
			LB_PARAM_TYPE x;
			LB_PARAM_TYPE y;
			LB_PARAM_TYPE z;
		};
	};

	LB_CLASS_NAME();
	LB_CLASS_NAME(const LB_CLASS_NAME& mom);
	LB_CLASS_NAME(LB_PARAM_TYPE _x,LB_PARAM_TYPE _y,LB_PARAM_TYPE _z);
	LB_CLASS_NAME(LB_PARAM_TYPE _x);

	LB_CLASS_NAME& set(LB_PARAM_TYPE _x,LB_PARAM_TYPE _y,LB_PARAM_TYPE _z);
	LB_CLASS_NAME& set(const LB_CLASS_NAME& b);
	LB_CLASS_NAME& set(LB_PARAM_TYPE _x);

	LB_PARAM_TYPE length() const;
	LB_PARAM_TYPE squareLength() const;

	LB_CLASS_NAME& normalize();
	LB_CLASS_NAME normalized() const;

	LB_CLASS_NAME& perpendicular(const LB_CLASS_NAME& b);
	LB_CLASS_NAME perpendiculared(const LB_CLASS_NAME& b) const;

	LB_CLASS_NAME& scale(LB_PARAM_TYPE scale);
	LB_CLASS_NAME scaled(LB_PARAM_TYPE scale) const;

	LB_CLASS_NAME& stretch(LB_PARAM_TYPE newLength);
	LB_CLASS_NAME stretched(LB_PARAM_TYPE newLength) const;

	LB_PARAM_TYPE dot(const LB_CLASS_NAME& b) const;


	LB_CLASS_NAME& cross(const LB_CLASS_NAME& b);
	LB_CLASS_NAME crossed(const LB_CLASS_NAME& b) const;

	LB_CLASS_NAME& rotate(LB_PARAM_TYPE rad,LB_PARAM_TYPE ax,LB_PARAM_TYPE ay,LB_PARAM_TYPE az);
	LB_CLASS_NAME rotated(LB_PARAM_TYPE rad,LB_PARAM_TYPE ax,LB_PARAM_TYPE ay,LB_PARAM_TYPE az) const;
	LB_CLASS_NAME& rotate(LB_PARAM_TYPE rad,const LB_CLASS_NAME& axis);
	LB_CLASS_NAME rotated(LB_PARAM_TYPE rad,const LB_CLASS_NAME& axis) const;

	LB_CLASS_NAME& rotateDeg(LB_PARAM_TYPE deg,LB_PARAM_TYPE ax,LB_PARAM_TYPE ay,LB_PARAM_TYPE az);
	LB_CLASS_NAME rotatedDeg(LB_PARAM_TYPE deg,LB_PARAM_TYPE ax,LB_PARAM_TYPE ay,LB_PARAM_TYPE az) const;
	LB_CLASS_NAME& rotateDeg(LB_PARAM_TYPE deg,const LB_CLASS_NAME& axis);
	LB_CLASS_NAME rotatedDeg(LB_PARAM_TYPE deg,const LB_CLASS_NAME& axis) const;

	LB_CLASS_NAME& reflect(const LB_CLASS_NAME& n);
	LB_CLASS_NAME reflected(const LB_CLASS_NAME& n) const;

	// piecewise operators
	bool operator==(const LB_CLASS_NAME& rh) const;
	bool operator!=(const LB_CLASS_NAME& rh) const;

	LB_CLASS_NAME& operator=(const LB_CLASS_NAME& rh);
	LB_CLASS_NAME& operator*=(const LB_CLASS_NAME& rh);
	LB_CLASS_NAME& operator/=(const LB_CLASS_NAME& rh);
	LB_CLASS_NAME& operator+=(const LB_CLASS_NAME& rh);
	LB_CLASS_NAME& operator-=(const LB_CLASS_NAME& rh);

	LB_CLASS_NAME operator*(const LB_CLASS_NAME& rh) const;
	LB_CLASS_NAME operator/(const LB_CLASS_NAME& rh) const;
	LB_CLASS_NAME operator+(const LB_CLASS_NAME& rh) const;
	LB_CLASS_NAME operator-(const LB_CLASS_NAME& rh) const;

	LB_CLASS_NAME operator-() const;
	LB_CLASS_NAME operator+() const;

	//LB_PARAM_TYPE operators:

	LB_CLASS_NAME& operator=(const LB_PARAM_TYPE& rh);
	LB_CLASS_NAME& operator*=(const LB_PARAM_TYPE& rh);
	LB_CLASS_NAME& operator/=(const LB_PARAM_TYPE& rh);

	LB_CLASS_NAME operator*(const LB_PARAM_TYPE& rh) const;
	LB_CLASS_NAME operator/(const LB_PARAM_TYPE& rh) const;

	LB_PARAM_TYPE operator[](unsigned int index) const;

	friend std::ostream &operator<<(std::ostream &stream, LB_CLASS_NAME &vec);
};

std::ostream &operator<<(std::ostream &stream, LB_CLASS_NAME &vec);
LB_CLASS_NAME operator*(const LB_PARAM_TYPE& lh,const LB_CLASS_NAME& rh);
LB_CLASS_NAME operator/(const LB_PARAM_TYPE& lh,const LB_CLASS_NAME& rh);
LB_PARAM_TYPE getAngle(const LB_CLASS_NAME& from, const LB_CLASS_NAME& to);



