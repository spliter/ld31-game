/*
 * lbMatrix3x3.h
 *
 *  Created on: 13-04-2011
 *      Author: Spliter
 */

#pragma once

#include "lbVec2.h"

#define LB_CLASS_NAME __internal_lbMatrix3x3f_t
#define LB_VECTOR_CLASS_NAME lbVec2f
#define LB_PARAM_TYPE float
#include "internal/lbMatrix3x3_internal" // IWYU pragma: export
typedef __internal_lbMatrix3x3f_t lbMatrix3x3f;
#undef LB_CLASS_NAME
#undef LB_VECTOR_CLASS_NAME
#undef LB_PARAM_TYPE

#define LB_CLASS_NAME __internal_lbMatrix3x3d_t
#define LB_VECTOR_CLASS_NAME lbVec2d
#define LB_PARAM_TYPE double
#include "internal/lbMatrix3x3_internal" // IWYU pragma: export
typedef __internal_lbMatrix3x3d_t lbMatrix3x3d;
#undef LB_CLASS_NAME
#undef LB_VECTOR_CLASS_NAME
#undef LB_PARAM_TYPE
