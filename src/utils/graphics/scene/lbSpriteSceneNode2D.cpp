/*
 * lbSpriteSceneNode2D.cpp
 *
 *  Created on: 1 mar 2014
 *      Author: spliter
 */

//#include <Glee.h>
#include <Glee.h>
#include <math/util/lbRect.h>
#include <stddef.h>
#include <utils/graphics/scene/lbSpriteSceneNode2D.h>
#include <utils/debug/debug.h>

lbSpriteSceneNode2D::lbSpriteSceneNode2D():
	_x(0),
	_y(0),
	_r(1),_g(1),_b(1),_a(1),
	_curFrame(0),
	_sprite(NULL)
{

}

lbSpriteSceneNode2D::~lbSpriteSceneNode2D()
{
}



void lbSpriteSceneNode2D::setSprite(lbSprite* sprite)
{
	_sprite=sprite;
}

void lbSpriteSceneNode2D::setPosition(float x, float y)
{
	_x=x;
	_y=y;
}

void lbSpriteSceneNode2D::draw(lbCameraSceneNode2D* camera)
{
	if(_sprite)
	{
		glPushMatrix();
		glMultMatrixf(getGLWorldTransform().me);
		glColor4f(_r,_g,_b,_a);
		_sprite->renderFrame(_curFrame,_x,_y,1.0f,1.0f);
		glPopMatrix();
	}
}
