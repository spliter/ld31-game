/*
 * lbSpriteDrawable.h
 *
 *  Created on: 24-11-2012
 *      Author: Spliter
 */

#pragma once

#include "../lbDrawable.h"

struct lbRectI;
class lbSprite;

class lbSpriteDrawable: public lbDrawable
{
public:
	lbSpriteDrawable();
	virtual ~lbSpriteDrawable();

	void setSprite(lbSprite* sprite);
	lbSprite* getSprite(){return _sprite;}
	void setFrame(int frame){_curFrame=frame;}
	int getFrame(){return _curFrame;}

	void setPosition(float x, float y);
	void setColour(float r,float g,float b,float a){_r=r;_g=b;_b=b;_a=a;}
	void setColour(float r,float g,float b){_r=r;_g=b;_b=b;_a=1;}

	virtual void draw(lbRectF camera);
private:
	float _x,_y;
	float _r,_g,_b,_a;
	int _curFrame;
	lbSprite* _sprite;
};
