/*
 * lbSprite.cpp
 *
 *  Created on: 10-10-2012
 *      Author: Spliter
 */

#include <global.h>
#include <Glee.h>
#include <utils/debug/debug.h>
#include <utils/file/TinyXml/tinyxml.h>
#include <utils/graphics/lbSprite.h>
#include <utils/graphics/lbTexture.h>
#include <utils/resources/lbResourceManager.h>
#include <utils/resources/lbTextureResourceManager.h>
#include <cstdio>
#include <cstdlib>
#include <string>


/*
 * Necessary tests:
 * 	-check if auto spacing calculation is working:
 * 		o-load an image with known frame size but unknown spacing
 * 		o-load an image with unknown frame size and spacing
 * 	-check if auto frame size calculation is working
 * 		o-load image with unknown frame size but known spacing
 * 		o-load image with unknown frame size and spacing
 */
using std::string;

lbSprite* lbSprite::loadSprite(string fname, lbTextureResourceManager* textureManager)
{
	LogEntryPoint();
	int nColumns = 0;//columnNumber
	int nFrames = 0;//frameNumber
	int centerX = CENTER_MIDDLE;//centerX (middle,left,right)
	int centerY = CENTER_MIDDLE;//centerY (middle,top,bottom)
	int startXOffset=0;//frameStartX
	int startYOffset=0;//frameStartY
	int hSpacing=-1;//spacingHorizontal
	int vSpacing=-1;//spacingVertical
	int frameWidth=-1;//frameWidth
	int frameHeight=-1;//frameHeight
	string textureFname;//textureFile

	string textureDir = retrieveDirectory(fname);

	TiXmlDocument doc(fname.c_str());

	if(doc.LoadFile())
	{
		TiXmlElement* spriteElem = doc.FirstChildElement("sprite");
		if(spriteElem)
		{
			spriteElem->QueryIntAttribute("columnNumber",&nColumns);
			spriteElem->QueryIntAttribute("frameNumber",&nFrames);
			spriteElem->QueryIntAttribute("startOffsetX",&startXOffset);
			spriteElem->QueryIntAttribute("startOffsetY",&startYOffset);
			spriteElem->QueryIntAttribute("spacingHorizontal",&hSpacing);
			spriteElem->QueryIntAttribute("spacingVertical",&vSpacing);
			spriteElem->QueryIntAttribute("frameWidth",&frameWidth);
			spriteElem->QueryIntAttribute("frameHeight",&frameHeight);

			 spriteElem->QueryStringAttribute("textureFile",&textureFname);

			int resX = spriteElem->QueryIntAttribute("centerX",&centerX);
			if(resX!=TIXML_SUCCESS && resX!=TIXML_NO_ATTRIBUTE)
			{
				std::string val ="left";
				spriteElem->QueryStringAttribute("centerX",&val);
				if(val=="middle")
				{
					centerX = CENTER_MIDDLE;
				}
				else if(val=="left")
				{
					centerX = CENTER_LEFT;
				}
				else if(val=="right")
				{
					centerX = CENTER_RIGHT;
				}
			}

			int resY = spriteElem->QueryIntAttribute("centerY",&centerY);
			if(resY!=TIXML_SUCCESS && resY!=TIXML_NO_ATTRIBUTE)
			{
				std::string val ="top";
				spriteElem->QueryStringAttribute("centerY",&val);
				if(val=="middle")
				{
					centerY = CENTER_MIDDLE;
				}
				else if(val=="top")
				{
					centerY = CENTER_LEFT;
				}
				else if(val=="bottom")
				{
					centerY = CENTER_RIGHT;
				}
			}
		}

		if(nFrames<=0)
		{
			printf("PARSE ERROR: NO NUMBER OF FRAMES SPECIFIED\n");
			return NULL;
		}

		lbTexture* spriteTexture = textureManager->getResource(textureFname.c_str());
		if (spriteTexture == NULL)
		{
			printf("Failed to load spriteTexture file \"%s\" : %s\n",fname.c_str(),doc.ErrorDesc());
			return NULL;
		}

		return new lbSprite(spriteTexture,nFrames,nColumns,centerX,centerY,startXOffset,startYOffset,hSpacing,vSpacing,frameWidth,frameHeight);
	}
	else
	{
		printf("Error opening file \"%s\" : %s\n",fname.c_str(),doc.ErrorDesc());
	}
	return NULL;
}

lbSprite::lbSprite(lbTexture* texture, int nFrames, int nColumns, int centerX, int centerY, int startXOffset, int startYOffset, int hSpacing, int vSpacing, int frameWidth, int frameHeight)
{
	LogEntryPoint();
	lbAssert(texture!=NULL && nFrames>0);

	if(nColumns<=0)
	{
		nColumns = nFrames;
	}

	int nRows = (nFrames+nColumns-1)/nColumns;
	_boundTexture = texture;
	int fWidth=frameWidth;
	int fHeight=frameHeight;
	int fHOffset=0;
	int fVOffset=0;
	int startX=0;
	int startY=0;

	if(frameWidth<0)//size doesn't matter
	{
		if(hSpacing<0)
		{
			//size doesn't matter and spacing doesn't matter
			hSpacing=0;
		}
		fWidth=(texture->getWidth()-(nColumns+1)*hSpacing)/nColumns;
		fHOffset = fWidth+hSpacing;
		startX=hSpacing;
		if(fWidth<0)
		{
			fWidth=1;
			fHOffset = 1;
			startX=0;
		}
	}
	else
	{
		fWidth = frameWidth;
		if(hSpacing<0)//size matters but spacing doesn't
		{
			int surplusWidth = texture->getWidth()/frameWidth;
			hSpacing = surplusWidth/nColumns;
		}
		fHOffset = hSpacing+fWidth;
		startX=hSpacing;
	}



	if(frameHeight<0)//size doesn't matter
	{
		if(vSpacing<0)
		{
			//size doesn't matter and spacing doesn't matter
			vSpacing=0;
		}
		fHeight=(texture->getHeight()-(nRows+1)*vSpacing)/nRows;
		fVOffset = fHeight+vSpacing;
		startY=vSpacing;
		if(fHeight<0)
		{
			fHeight=1;
			fVOffset = 1;
			startY=0;
		}
	}
	else
	{
		fHeight = frameHeight;
		if(vSpacing<0)//size matters but spacing doesn't
		{
			int surplusHeight= texture->getHeight()/frameHeight;
			vSpacing = surplusHeight/nRows;
		}
		fVOffset = vSpacing+fHeight;
		startY=vSpacing;
	}

	switch(centerX)
	{
		case CENTER_MIDDLE:
			centerX = fWidth/2;
			break;
		case CENTER_LEFT:
			centerX = 0;
			break;

		case CENTER_RIGHT:
			centerX = fWidth;
			break;
		default:
			//keep the same
			break;
	};

	switch(centerY)
	{
		case CENTER_MIDDLE:
			centerY = fHeight/2;
			break;
		case CENTER_TOP:
			centerY = 0;
			break;

		case CENTER_BOTTOM:
			centerY = fHeight;
			break;
		default:
			//keep the same
			break;
	};

	_bbox.set(-centerX,-centerY,-centerX+fWidth,-centerY+fHeight);

	_frames.reserve(nFrames);


	startX+=startXOffset;
	startY+=startYOffset;

//	printf("nFrames = %d, nColumns=%d, nRows=%d, fVOffset=%d, startY=%d",nFrames,nColumns,nRows,fVOffset,startY);
	int left;
	int top = startY;
	int totalFrames = 0;
	for(int y=0;y<nRows && totalFrames<nFrames;y++)
	{
		left = startX;

		for(int x=0;x<nColumns && totalFrames<nFrames;x++)
		{
			_frames.push_back(lbRectI(left,top,left+fWidth,top+fHeight));
			left+=fHOffset;
			totalFrames++;
		}
		top+=fVOffset;
	}

}

lbSprite::~lbSprite()
{

}

lbTexture* lbSprite::getBoundTexture()
{
	return _boundTexture;
}

int lbSprite::getFrameNum()
{
	return _frames.size();
}

lbRectI lbSprite::getFrameRect(int frame)
{
	return _frames[frame];
}

void lbSprite::renderFrame(int frame, int x, int y, float xScale, float yScale)
{
	LogEntryPoint();
	_boundTexture->bind();

	lbRectF bbox = toRectF(getSpriteBbox());
	bbox.scale(xScale,yScale);
	bbox.move(x,y);

	lbRectF frameBbox = toRectF(getFrameRect(frame));

	frameBbox = _boundTexture->remapCoords(frameBbox);
	glBegin(GL_TRIANGLE_STRIP);
		glTexCoord2f(frameBbox.x1,frameBbox.y1);glVertex2f(bbox.x1,bbox.y1);
		glTexCoord2f(frameBbox.x1,frameBbox.y2);glVertex2f(bbox.x1,bbox.y2);
		glTexCoord2f(frameBbox.x2,frameBbox.y1);glVertex2f(bbox.x2,bbox.y1);
		glTexCoord2f(frameBbox.x2,frameBbox.y2);glVertex2f(bbox.x2,bbox.y2);
	glEnd();
//	_boundTexture->unbind();
}

void lbSprite::renderFrame(int frame, lbRectF rect)
{
	LogEntryPoint();
	_boundTexture->bind();
	lbRectF frameBbox = toRectF(getFrameRect(frame));
	frameBbox = _boundTexture->remapCoords(frameBbox);
	glBegin(GL_TRIANGLE_STRIP);
		glTexCoord2f(frameBbox.x1,frameBbox.y1);glVertex2f(rect.x1,rect.y1);
		glTexCoord2f(frameBbox.x1,frameBbox.y2);glVertex2f(rect.x1,rect.y2);
		glTexCoord2f(frameBbox.x2,frameBbox.y1);glVertex2f(rect.x2,rect.y1);
		glTexCoord2f(frameBbox.x2,frameBbox.y2);glVertex2f(rect.x2,rect.y2);
	glEnd();
}

