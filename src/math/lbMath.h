/*
 * lbMath.h
 *
 *  Created on: 11-04-2011
 *      Author: Spliter
 */

#ifndef LBMATH_H_
#define LBMATH_H_

#include "lbBaseMath.h"
#include "lbVec2.h"
#include "lbVec3.h"
#include "lbVec4.h"
#include "lbQuaternion.h"
#include "lbMatrix3x3.h"
#include "lbMatrix4x4.h"
#endif /* LBMATH_H_ */
