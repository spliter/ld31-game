/*
 * lbSceneNode2D.cpp
 *
 *  Created on: 1 mar 2014
 *      Author: spliter
 */

#include <stddef.h>
#include <utils/graphics/scene/lbSceneLayer2D.h>
#include <utils/graphics/scene/lbSceneNode2D.h>

lbSceneNode2D::lbSceneNode2D() :
		_parent(NULL), _childrenHead(NULL), _next(NULL), _prev(NULL), _layer(NULL), _visible(true), _zOrder(0), _position(0, 0), _scale(1, 1), _rotationDeg(0), _isDirty(false)
{

}

lbSceneNode2D::~lbSceneNode2D()
{
	removeFromLayer();
	if (_parent)
	{
		_parent->removeChild(this);
		_parent = NULL;
	}
}

void lbSceneNode2D::addToLayer(lbSceneLayer2D* layer)
{
	if (layer)
	{
		layer->addNode(this);
	}
}

void lbSceneNode2D::removeFromLayer()
{
	if (_layer)
	{
		_layer->removeNode(this);
	}
}

lbVec2f lbSceneNode2D::worldToLocalSpace(lbVec2f pos)
{
	updateTransformIfDirty();
	return _invWorldTransform * pos;
}

lbVec2f lbSceneNode2D::localToWorldSpace(lbVec2f pos)
{
	updateTransformIfDirty();
	return _worldTransform * pos;
}

void lbSceneNode2D::markTransformDirty()
{
	if (!_isDirty)
	{
		_isDirty = true;
		lbSceneNode2D* child = _childrenHead;
		while (child)
		{
			child->markTransformDirty();
			child = child->_next;
		}
	}
}

void lbSceneNode2D::updateTransformIfDirty()
{
	if (_isDirty)
	{
		_isDirty = false;
		_transform.makeIdentity();
		_transform.translate(_position.x, _position.y);
		_transform.scale(_scale.x, _scale.y);
		_transform.rotateDeg(_rotationDeg);
		_invTransform = _transform.inversed();
		if (_parent)
		{
			_parent->updateTransformIfDirty();
			_worldTransform = _parent->_worldTransform * _transform;
			_invWorldTransform = _invTransform * _parent->_invWorldTransform;
		}
		else
		{
			_worldTransform = _transform;
			_invWorldTransform = _invTransform;
		}
	}
}

void lbSceneNode2D::addChild(lbSceneNode2D* child)
{
	if (child)
	{
		if (child->_parent)
		{
			if (child->_parent == this)
			{
				return;
			}
			child->_parent->removeChild(child);
		}
		child->_next = _childrenHead;
		if (_childrenHead)
		{
			_childrenHead->_prev = child;
		}
		_childrenHead = child;
		_childrenHead->_parent = this;
		_childrenHead->markTransformDirty();
	}
}

void lbSceneNode2D::removeChild(lbSceneNode2D* child)
{
	if (child && child->_parent == this)
	{
		child->_parent = NULL;
		if (child->_next)
		{
			child->_next->_prev = child->_prev;
		}
		if (child == _childrenHead)
		{
			_childrenHead = child->_next;
		}
		else
		{
			child->_prev->_next = child->_next;
		}
		child->markTransformDirty();
	}
}
