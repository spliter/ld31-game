/*
 * BloodParticleSpawner.cpp
 *
 *  Created on: 7 gru 2014
 *      Author: Miko Kuta
 */

#include <game/entities/particles/BloodParticleSpawner.h>
#include <game/entities/particles/ParticleSpawner.h>
#include <game/GameEntity.h>
#include <game/GameEntityFactoryManager.h>
#include <Glee.h>
#include <math/lbBaseMath.h>
#include <utils/graphics/lbTexture.h>
#include <cfloat>

REGISTER_GAME_ENTITY(BloodParticleSpawner,"blood_spawner");

BloodParticleSpawner::BloodParticleSpawner(GameContext* context) :
		ParticleSpawner(context)
{
	_speed.set(0, 0);
	_drag = 0;
	_speedVariationRatio = 0;
	_dirVariationDeg = 0;
	_duration = 0;
	_bloodColor.set(1, 0, 0, 1);
	_particleLife = 0;
	_particleLifeVariation = 0;
	_particlesToFire = 0;
	_timeElapsed = 0;
	_particlesFired = 0;
	_killOnFinish = false;
	_active = false;
}

BloodParticleSpawner::~BloodParticleSpawner()
{
}

void BloodParticleSpawner::onCreated()
{
	super::onCreated();
	setParticleCount(512);
}

void BloodParticleSpawner::onSpawned()
{
	super::onSpawned();
}

void BloodParticleSpawner::onUpdate(float dt)
{
	if (_active)
	{
		_timeElapsed += dt;
		if (_timeElapsed > _duration)
		{
			_timeElapsed = _duration;
			_active = false;
		}
		int totalParticles;
		if (_duration > FLT_EPSILON)
		{
			totalParticles = (_timeElapsed / _duration) * _particlesToFire;
		}
		else
		{
			totalParticles = _particlesToFire;
		}
		int particlesToFireThisFrame = totalParticles - _particlesFired;
		_particlesFired = totalParticles;
		spawnParticles(particlesToFireThisFrame);
	}
	if (_aliveParticleCount)
	{
		for (int i = 0; i < (int) _particles.size(); i++)
		{
			if (_particles[i].isAlive)
			{
				BloodParticle &particle = _particles[i];

				if (particle.timeToLive < 0)
				{
					freeParticle(i);
					continue;
				}
				particle.speed -= particle.speed * particle.drag * dt;
				particle.timeToLive -= dt;
				particle.prevPos = particle.pos;
				particle.pos += particle.speed * dt;
			}
		}
	}

	if(_killOnFinish && !_active && _aliveParticleCount==0)
	{
		kill();
	}
}

void BloodParticleSpawner::draw(lbCameraSceneNode2D* camera)
{
	lbTexture::unbind();
	glLineWidth(3);
	glBegin(GL_LINES);
	for (int i = 0; i < (int)_particles.size(); i++)
	{
		if(_particles[i].isAlive)
		{
			glColor4f(_particles[i].color.r,_particles[i].color.g,_particles[i].color.b,_particles[i].color.a);
//			glColor4f(1.0f,0.0f,0.0f,1.0f);
			glVertex2fv(_particles[i].pos.me);
			glColor4f(_particles[i].color.r,_particles[i].color.g,_particles[i].color.b,0.5f);
//			glColor4f(1.0f,0.0f,0.0f,1.0f);
			glVertex2fv(_particles[i].prevPos.me);
		}
	}
	glEnd();
	glLineWidth(1);
}

void BloodParticleSpawner::spray(
									lbVec2f position,
									lbVec2f speed,
									float drag,
									float speedVariationRatio,
									float dirVariationDeg,
									lbColor bloodColor,
									float duration,
									float particleLife,
									float particleLifeVariation,
									int particlesToFire,
									bool killOnFinish)
{
	_active = true;
	setPosition(position);
	_speed = speed;
	_drag = drag;
	_speedVariationRatio = speedVariationRatio;
	_dirVariationDeg = dirVariationDeg;
	_bloodColor = bloodColor;
	_duration = duration;
	_particleLife = particleLife;
	_particleLifeVariation = particleLifeVariation;
	_particlesToFire = particlesToFire;
	_particlesFired = 0;
	_timeElapsed = 0;
	_killOnFinish = killOnFinish;
	if (_duration <= 0)
	{
		spawnParticles(_particlesToFire);
		_active = false;
	}
}

void BloodParticleSpawner::spawnParticles(int count)
{
	lbVec2f pos = getPosition();
	lbVec2f speed = _speed;

	for (int i = 0; i < count; i++)
	{
		int id = reserveParticle();
		if (id >= 0)
		{
			float speedMultiplier = random(1 - _speedVariationRatio, 1 + _speedVariationRatio);
			float dirDiff = random(-_dirVariationDeg * 0.5f, _dirVariationDeg * 0.5f);
			float timeToLive = _particleLife + random(-_particleLifeVariation * 0.5f, _particleLifeVariation * 0.5f);
			lbVec2f partSpeed = speed.rotatedDeg(dirDiff) * speedMultiplier;
			_particles[id].pos = pos;
			_particles[id].speed = partSpeed;
			_particles[id].prevPos = pos;
			_particles[id].timeToLive = timeToLive;
			_particles[id].color = _bloodColor;
			_particles[id].drag = _drag;
		}
	}
}

