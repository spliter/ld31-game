/*
 * InputEvents.h
 *
 *  Created on: 21-07-2012
 *      Author: Spliter
 */

#pragma once

#include <events/lbGameEvent.h>
#include <lbTypes.h>


typedef uint32 lbKey;
typedef uint32 lbKeyMod;


class lbKeyboardEvent;
class lbMouseMotionEvent;
class lbMouseButtonEvent;
class lbMouseWheelEvent;

class lbInputListener
{
public:
	virtual ~lbInputListener(){}
	virtual void onKeyboardEvent(lbKeyboardEvent* event)=0;
	virtual void onMouseMotionEvent(lbMouseMotionEvent* event)=0;
	virtual void onMouseButtonEvent(lbMouseButtonEvent* event)=0;
	virtual void onMouseWheelEvent(lbMouseWheelEvent* event)=0;
};

class lbKeyboardEvent: public lbGameEvent
{
	DEFINE_EVENT(lbKeyboardEvent)
public:
	enum Type
	{
		KeyPressed,KeyReleased
	};

	lbKey key;
	lbKeyMod keyMod;
	Type type;
};

class lbMouseButtonEvent: public lbGameEvent
{
	DEFINE_EVENT(lbMouseButtonEvent)
public:
	enum Type
	{
		MousePressed,MouseReleased
	};

	enum Button
	{
		ButtonUnknown = -1,
		ButtonLeft=0,
		ButtonRight,
		ButtonMiddle,
		ButtonExtra1,
		ButtonExtra2,
		ButtonNumber
	};

	int32 mouseId;
	float x,y;
	Button button;
	Type type;
};

class lbMouseWheelEvent: public lbGameEvent
{
	DEFINE_EVENT(lbMouseWheelEvent)
public:
	int32 mouseId;
	int32 x,y;
};

class lbMouseMotionEvent: public lbGameEvent
{
	DEFINE_EVENT(lbMouseMotionEvent)
public:
	int32 mouseId;
	float x,y;
	float relX,relY;
};

