/*
 * MonsterEntity.h
 *
 *  Created on: 4 gru 2014
 *      Author: Miko Kuta
 */

#pragma once

#include <game/collisions/CollisionPoly.h>
#include <game/entities/SolidEntity.h>
#include <math/lbVec2.h>
#include <utils/graphics/scene/lbSpriteSceneNode2D.h>
#include <utils/Object.h>

class PlayerEntity;

struct GameContext;
class lbCameraSceneNode2D;
class lbSprite;

class MonsterEntity: public SolidEntity
{
public:
	DEFINE_CLASS(MonsterEntity,SolidEntity);

	MonsterEntity(GameContext* context);
	virtual ~MonsterEntity();

	virtual void onCreated();
	virtual void onSpawned();
	virtual void onDestroy();
	virtual void onUpdate(float dt);
	virtual void draw(lbCameraSceneNode2D* camera);

	virtual void setPosition(float x, float y){super::setPosition(x,y);_collider.setPosition(getPosition());}
	virtual void setPosition(lbVec2f newPos){setPosition(newPos.x,newPos.y);}

	virtual CollisionPoly** getCollisionPolygons(){return _colliders;}
	virtual int getCollisionPolyNum(){return 1;}

	bool isAlive(){return _isAlive;}
	virtual void die();
	virtual void onDeath(){setZOrder(-3);}

	virtual void onHitByBullet(lbVec2f position, lbVec2f speed, int damage);

	void setBaseSprite(lbSprite* sprite);
	lbSprite* getBaseSprite(){return _monsterSprite;}
	void setShoveStunDuration(float shoveStunDuration);
	void setShoveDecay(float shoveDecay);


	virtual void shove(lbVec2f force){_shoveForce+=force;_timeSinceShove = 0;}
	bool isStunnedFromShove(){return _timeSinceShove<_shoveStunDuration;}

	virtual void setMaxHealth(int maxHealthAmount){_maxHealthPoints = maxHealthAmount;}
	virtual void setHealth(int healthAmount);
	virtual void applyDamage(int damagePoints);
	virtual void addHealth(int healthAmount);
	int getHealthAmount(){return _healthPoints;}
	int getMaxHealthAmount(){return _maxHealthPoints;}

	PlayerEntity* getPlayer();



protected:
	virtual void checkCollisions();
	lbSprite* _monsterSprite;
	lbSpriteSceneNode2D _spriteNode;

	CollisionPoly _collider;
	CollisionPoly* _colliders[1];
	lbVec2f _shoveForce;
	float _timeSinceShove;
	float _shoveForceDecay;
	float _shoveStunDuration;

	int _healthPoints;
	int _maxHealthPoints;

	int _playerId;
	int _playerUid;

	bool _isAlive;
};

