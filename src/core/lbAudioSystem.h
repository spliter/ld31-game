/*
 * lbAudioSystem.h
 *
 *  Created on: 21-09-2013
 *      Author: Spliter
 */

#pragma once
#include "fmod.h"
#include <string>

class lbAudioSystem
{
public:
	static std::string getErrorString(FMOD_RESULT result);
	static void FMODErrorCheck(FMOD_RESULT result);
	static lbAudioSystem& instance();

	bool init();
	bool deinit();

	void update();

	bool isInitialized(){return _isInitialized;}

	FMOD_SYSTEM *getSoundSystem(){return _system;}
private:

	lbAudioSystem();
	virtual ~lbAudioSystem();

	bool _isInitialized;
	FMOD_SYSTEM *_system;
};

