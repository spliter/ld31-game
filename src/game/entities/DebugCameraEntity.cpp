/*
 * DebugCameraEntity.cpp
 *
 *  Created on: 4 gru 2014
 *      Author: Miko Kuta
 */

#include <events/input/lbInputSystem.h>
#include <events/input/lbKeys.h>
#include <game/entities/DebugCameraEntity.h>
#include <game/GameEntityFactoryManager.h>
#include <game/level/GameLevel.h>
#include <game/states/GameState.h>
#include <math/lbVec2.h>
#include <utils/graphics/scene/lbCameraSceneNode2D.h>


static float movementSpeed = 300.0f;//units/second

REGISTER_GAME_ENTITY(DebugCameraEntity,"debug_camera");

DebugCameraEntity::DebugCameraEntity(GameContext* context):
	GameEntity(context)
{

}

DebugCameraEntity::~DebugCameraEntity()
{
}

void DebugCameraEntity::onUpdate(float dt)
{
	bool upPressed = lbInputSystem::getInstance().keyCheck(LBK_W) || lbInputSystem::getInstance().keyCheck(LBK_UP);
	bool downPressed = lbInputSystem::getInstance().keyCheck(LBK_S) || lbInputSystem::getInstance().keyCheck(LBK_DOWN);
	bool leftPressed = lbInputSystem::getInstance().keyCheck(LBK_A) || lbInputSystem::getInstance().keyCheck(LBK_LEFT);
	bool rightPressed = lbInputSystem::getInstance().keyCheck(LBK_D) || lbInputSystem::getInstance().keyCheck(LBK_RIGHT);
	lbVec2f movement;
	movement+=upPressed?lbVec2f(0,-movementSpeed):lbVec2f(0,0);
	movement+=downPressed?lbVec2f(0,movementSpeed):lbVec2f(0,0);
	movement+=leftPressed?lbVec2f(-movementSpeed,0):lbVec2f(0,0);
	movement+=rightPressed?lbVec2f(movementSpeed,0):lbVec2f(0,0);


	setPosition(getPosition()+movement*dt);
	getGameContext()->gameState->getCurrentLevel()->characterCamera.setPosition(getPosition());
	getGameContext()->gameState->getCurrentLevel()->backgroundCamera.setPosition(getPosition());
	getGameContext()->gameState->getCurrentLevel()->foregroundCamera.setPosition(getPosition());
}


