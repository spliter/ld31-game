/*
 * StateManager.h
 *
 *  Created on: 13-09-2012
 *      Author: Spliter
 */

#pragma once
#include <stack>
#include "../events/input/lbInputEvents.h"

class State;
class StateManager: public lbInputListener
{
public:
	static StateManager& getInstance();
	virtual ~StateManager();

	void update();
	void startFrame();
	void renderFrame();
	void endFrame();

	void cleanUp();

	void onKeyboardEvent(lbKeyboardEvent *event);
	void onMouseMotionEvent(lbMouseMotionEvent *event);
	void onMouseButtonEvent(lbMouseButtonEvent *event);
	void onMouseWheelEvent(lbMouseWheelEvent *event);

	void startState(State* state);//Starts the state and adds it as a child of existing state (if there is one)
	void bringToTop(State* state);//Finds the state and brings it to top if it isn't already on top, this is the same as calling startState() on an already started state
	//NOTE: StateManager will not delete states, this is left to the states themselves
private:
	typedef std::list<State*>::iterator StateIter;
	StateManager();
	void cleanFinishedStates();
	bool performStackCleanup();//we modify the stack in such a way that the current stack becomes identical to the desired stack
	static StateIter findState(std::list<State*> stateList,State* state);
	static bool hasState(std::list<State*> stateList,State* state);
	std::list<State*> _currentStack;
	std::list<State*> _desiredStack;
};

inline StateManager& getStateManager(){return StateManager::getInstance();}
