/*
 * GameLevel.h
 *
 *  Created on: 3 gru 2014
 *      Author: Miko Kuta
 */

#pragma once

#include <game/entities/SolidEntity.h>
#include <game/GameContext.h>
#include <game/GameEntity.h>
#include <game/level/Map.h>
#include <utils/entities/lbEntityManager.h>
#include <utils/graphics/lbTileMap.h>
#include <utils/graphics/scene/lbCameraSceneNode2D.h>
#include <utils/graphics/scene/lbSceneLayer2D.h>
#include <utils/graphics/scene/lbSceneTileMap.h>
#include <list>
#include <string>

class GameEntity;
typedef lbEntityManager<GameEntity> GameEntityManager;

class GameLevel
{
public:
	static GameLevel* loadLevel(GameContext context,std::string filename);

	GameLevel(GameContext context);
	virtual ~GameLevel();

	void onStart();
	void onUpdate(float frameDif);
	void onFrameRender();
	void onEnd();

	GameEntity* createEntity(const std::string& type);

	GameEntityManager entManager;

	GameContext gameContext;

	typedef std::list<lbTileMap*>::iterator MapIterator;
	typedef std::list<lbTileMap*>::iterator TileMapIterator;

	std::list<Map*> collisionMaps;
	std::list<lbSceneTileMap*> tileMaps;

	lbSceneLayer2D backgroundLayer;
	lbSceneLayer2D characterLayer;
	lbSceneLayer2D foregroundLayer;

	lbCameraSceneNode2D backgroundCamera;
	lbCameraSceneNode2D characterCamera;
	lbCameraSceneNode2D foregroundCamera;

	SolidWorld solidGeometryWorld;
	SolidWorld enemiesWorld;
	SolidWorld playerWorld;
};

