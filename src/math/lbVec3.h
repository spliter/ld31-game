/*
 * lbVec3f.h
 *
 *  Created on: 12-04-2011
 *      Author: Spliter
 */

#pragma once

#define LB_CLASS_NAME __internal_lbVec3f_t
#define LB_PARAM_TYPE float
#include "internal/lbVec3_internal" // IWYU pragma: export
typedef LB_CLASS_NAME lbVec3f;
#undef LB_CLASS_NAME
#undef LB_PARAM_TYPE

#define LB_CLASS_NAME __internal_lbVec3d_t
#define LB_PARAM_TYPE double
#include "internal/lbVec3_internal" // IWYU pragma: export
typedef LB_CLASS_NAME lbVec3d;
#undef LB_CLASS_NAME
#undef LB_PARAM_TYPE
