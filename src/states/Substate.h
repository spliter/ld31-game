/*
 * Substate.h
 *
 *  Created on: 15 lut 2014
 *      Author: spliter
 */

#pragma once
class lbKeyboardEvent;
class lbMouseButtonEvent;
class lbMouseMotionEvent;
class lbMouseWheelEvent;
class State;

class Substate
{
public:
	virtual ~Substate(){};

	virtual void onAdded(State* owner){}//Called once, after the state is created and added to be run
	virtual void onResume(){};//Called each time it resumes from a pause, but it will only be called in onUpdate
	virtual void onUpdate(){}
	virtual void onFrameBegin(){}
	virtual void onFrameRender(){}
	virtual void onFrameEnd(){}
	virtual void onPause(){};//called each time the state is paused/hidden
	virtual void onRemoved(State* owner){};//Called once, right before deletion;

	virtual void onKeyboardEvent(lbKeyboardEvent *event){}
	virtual void onMouseMotionEvent(lbMouseMotionEvent* event){}
	virtual void onMouseButtonEvent(lbMouseButtonEvent* event){}
	virtual void onMouseWheelEvent(lbMouseWheelEvent* event){}

	//this will take effect only after the substate has been added
	void start(){if(!_isResumed && _owner){_requestedState = Start;}}
	void pause(){if(_isResumed && _owner){_requestedState = Pause;}}

	State* getOwnerState(){return _owner;}

	bool isAttached(){return _owner;}
	State* getState(){return _owner;}
	bool isResumed(){return _isResumed;}
private:
	enum RequestedState
	{
		None,Start,Pause
	};

	friend class State;
	State *_owner;
	bool _isResumed;
	RequestedState _requestedState;
};

