/*
 * lbSpriteAnimator.h
 *
 *  Created on: 11-10-2012
 *      Author: Spliter
 */

#pragma once
#include <map>
#include <vector>

class lbSprite;
class lbSpriteAnimator;

struct lbSpriteAnimation
{
	lbSprite* sprite;
	int curFrame;
	bool resetOnSwitch;//should reset curFrame every time we switch to another animation?
	bool loop;
	int fps;

	void set(lbSprite* sprite,int curFrame,bool resetOnPause,bool loop,int fps)
	{
		this->sprite=sprite;
		this->curFrame=curFrame;
		this->resetOnSwitch=resetOnPause;
		this->loop=loop;
		this->fps=fps;
	}
};

class lbSpriteAnimationListener
{
public:
	virtual ~lbSpriteAnimationListener(){}
	virtual void onAnimationFinished(lbSpriteAnimator* animator, int refId)=0;
	virtual void onAnimationLooped(lbSpriteAnimator* animator, int refId)=0;
};

class lbSpriteAnimator
{
public:
	lbSpriteAnimator();
	~lbSpriteAnimator();

	void update(unsigned long frameMillisDiff);

	lbSpriteAnimation getCurrentAnimation();//will return current animation if any, null animation otherwise
	int getCurrentAnimationId();//will return current animation id if any, -1 animation otherwise

	void addAnimation(int refId, lbSprite* sprite, int fps=30, bool resetOnPause=true, bool loop=false);
	void startAnimation(int refId, bool forceRestart = false);//if forceRestart then it will restart the animation if it's already running
	void stopAnimation();//pauses and resets animation
	void pauseAnimation();

	void setAnimationListener(lbSpriteAnimationListener* listener);
private:
	typedef std::map<int,int>::iterator AnimIter;
	std::map<int,int> _animationMap;//first =  ref, second = index
	std::vector<lbSpriteAnimation> _animations;
	int _currentAnimationIndex;
	int _currentAnimationRef;
	unsigned long _currentAnimationTime;
	bool _isPlaying;
	lbSpriteAnimationListener* _listener;
};
