/*
 * lbTileMap.h
 *
 *  Created on: 06-11-2012
 *      Author: Spliter
 */

#pragma once

#include <Glee.h>
//#include <math/util/lbRect.h>
#include <utils/graphics/drawable/lbDrawable.h>

struct lbRectF;

class lbTileSet;

class lbTexture;
class TiXmlElement;


class lbTileMap: public lbDrawable
{
public:
	struct Tile
	{
		unsigned int renderInfo;
		unsigned int color;//argb
		Tile(){renderInfo=0;color=0xffffffff;}
	};

	static lbTileMap* createFromXmlElement(TiXmlElement* element);
	inline static int getTileTextureCoord(int x, int y){return (((x)<<8)|(y));}
	inline static int getTileTextureX(int textureCoord){return (((textureCoord)>>8)&0xff);}
	inline static int getTileTextureY(int textureCoord){return ((textureCoord)&0xff);}

	lbTileMap();
	lbTileMap(int width, int height, float tileWidth, float tileHeight);
	virtual ~lbTileMap();

	void setTexture(lbTexture* texture, int tileColumns, int tileRows);
	void setFromTileSet(lbTileSet *info);
	lbTexture* getTexture(){return _texture;}
	int getTextureTileColumns(){return _textureTileColumns;}
	int getTextureTileRows(){return _textureTileRows;}

	virtual void draw(lbRectF camera);


	Tile* getTiles(){return _tileMap;}
	void onTilesChanged(){_isDirty = true;}

	void resize(int width, int height);//note: it will lose all the information before this
	void setTileSize(float tileWidth, float tileHeight);
	void updateTiles();


	int getWidth(){return _width;}
	int getHeight(){return _height;}

	float getTileWidth(){return _tileWidth;}
	float getTileHeight(){return _tileHeight;}

private:
	int setupVertexBuffer(int startX, int startY, int visibleColumns, int visibleRows);

	struct VertexData
	{
		GLfloat x,y,z;
		GLfloat tx,ty;
		GLuint color;
	};

	Tile* _tileMap;
	lbTexture* _texture;
	VertexData* _vertexData;
	GLuint _vbo;

	int	_width;
	int _height;

	int _tileWidth;
	int _tileHeight;

	int _textureTileColumns;
	int _textureTileRows;

	int _numVertices;

	bool _isDirty;

	int _prevStartX;
	int _prevStartY;
	int _prevVisibleRows;
	int _prevVisibleColumns;
};
