/*
 * lbExternalDrawable.h
 *
 *  Created on: 24-11-2012
 *      Author: Spliter
 */

#pragma once
#include "../lbDrawable.h"

class lbExternalDrawableListener
{
public:
	virtual void onDraw(int drawableId, lbRectI camera)=0;
};

class lbExternalDrawable:public lbDrawable
{
public:
	lbExternalDrawable();
	virtual ~lbExternalDrawable();

	int getId(){return _id;}
	void setId(int id){_id=id;}

	void setListener(lbExternalDrawableListener* listener){_listener=listener;}

	virtual void draw(lbRectI camera);
private:
	lbExternalDrawableListener* _listener;
	int _id;
};
