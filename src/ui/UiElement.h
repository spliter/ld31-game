/*
 * UiElement.h
 *
 *  Created on: 24-09-2012
 *      Author: Spliter
 */

#pragma once
#include <math/lbMath.h>
#include <math/util/lbRect.h>
#include "../events/input/lbInputEvents.h"
#include <iostream>

class UiElementGroup;
class UiElement;


class UiListener
{
public:
	virtual void onPressed(UiElement* element, lbMouseButtonEvent* pressEvent){}
	virtual void onDragged(UiElement* element, lbMouseButtonEvent::Button button, lbMouseMotionEvent* moveEvent){}
	virtual void onReleased(UiElement* element, lbMouseButtonEvent* releaseEvent){}
	virtual void onCancelled(UiElement* element){}
	virtual void onClicked(UiElement* element, lbMouseButtonEvent* releaseEvent){}
};


class UiElement
{
public:
	UiElement();
	virtual ~UiElement();

	virtual void setup(){};
	virtual void update(unsigned long frameDiffMillis){};
	virtual void draw(){std::cout<<"Warning: using default draw\n";}

	virtual void setPosition(float x, float y);
	lbVec2f getPosition(){return _pos;}

	virtual void setGlobalPosition(float x, float y);
	lbVec2f getGlobalPosition();

	virtual void setSize(float width, float height);
	lbVec2f getSize(){return _size;}

	float getWidth(){return _size.x;}
	float getHeight(){return _size.y;}

	lbRectF getLocalBoundingRect();
	lbRectF getGlobalBoundingRect();

	virtual bool isInside(float x, float y);

	virtual bool handleMouseButtonEvent(lbMouseButtonEvent* event);//returns true if the event has been handled
	virtual bool handleMouseMotionEvent(lbMouseMotionEvent* event);//returns true if the event has been handled

	void setListener(UiListener* listener);
	UiListener* getListener(){return _listener;}

	UiElementGroup* getParent(){return _parent;}

	virtual void markTransformDirty(){_transformDirty=true;}
	virtual void updateTransform();//updates global position based on parent's global position

	virtual void setClickable(bool clickable);
	bool getClickable(){return _isClickable;}

	virtual void setEnabled(bool enabled);
	bool isEnabled(){return _enabled;}

	bool isPressed(){return _pressed;}
private:
	friend class UiElementGroup;
	void setParent(UiElementGroup* parent);

	bool _pressed;
	lbMouseButtonEvent::Button _pressButton;

	bool _enabled;

	UiListener* _listener;
	UiElementGroup* _parent;

	lbVec2f _pos;
	lbVec2f _globalPos;

	lbVec2f _size;

	lbRectF _boundingRect;
	lbRectF _globalBoundingRect;

	bool _isClickable;
	bool _transformDirty;
};
