/*
 * BloodParticleSpawner.h
 *
 *  Created on: 7 gru 2014
 *      Author: Miko Kuta
 */

#pragma once

#include <game/entities/particles/ParticleSpawner.h>
#include <math/lbVec2.h>
#include <utils/graphics/lbColor.h>
#include <utils/Object.h>

struct GameContext;

class lbCameraSceneNode2D;

struct BloodParticle
{
	union
	{
		int nextFreeId;
		float timeToLive;
	};
	bool isAlive;
	float drag;
	lbVec2f pos;
	lbVec2f prevPos;
	lbVec2f speed;
	lbColor color;
};

class BloodParticleSpawner: public ParticleSpawner<BloodParticle>
{
public:
	DEFINE_CLASS(BloodParticleSpawner,GameEntity);

	BloodParticleSpawner(GameContext* context);
	virtual ~BloodParticleSpawner();
	virtual void onCreated();
	virtual void onSpawned();
	virtual void onUpdate(float dt);

	virtual void draw(lbCameraSceneNode2D* camera);

	void spray(lbVec2f position, lbVec2f speed, float drag, float speedVariationRatio, float dirVariationDeg, lbColor bloodColor, float duration, float particleLife, float particleLifeVariation, int particlesToFire, bool killOnFinish);
	void deactivate(){_active = false;}
private:
	void spawnParticles(int count);
	lbVec2f _position;
	lbVec2f _speed;
	float _drag;
	float _speedVariationRatio;
	float _dirVariationDeg;
	float _duration;
	lbColor _bloodColor;
	float _particleLife;
	float _particleLifeVariation;
	int _particlesToFire;
	int _timeElapsed;
	int _particlesFired;
	bool _killOnFinish;
	bool _active;
};
