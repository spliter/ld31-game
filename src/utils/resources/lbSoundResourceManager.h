/*
 * lbSoundResourceManager.h
 *
 *  Created on: 21-09-2013
 *      Author: Spliter
 */

#pragma once

#include <fmod.h>
#include <utils/resources/lbResourceManager.h>
#include <string>

class lbSoundResourceManager: public lbResourceManager<FMOD_SOUND>
{
public:
	lbSoundResourceManager();
	virtual ~lbSoundResourceManager();

	virtual FMOD_SOUND* getDefaultResource(){return _defaultSound;}
	virtual FMOD_SOUND* loadResourceFromFile(const std::string & fname);
	virtual void freeResource(FMOD_SOUND* resource){FMOD_Sound_Release(resource);}
private:
	void createDefaultSound();
	FMOD_SOUND* _defaultSound;
};
