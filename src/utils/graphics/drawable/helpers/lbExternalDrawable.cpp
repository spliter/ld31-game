/*
 * lbExternalDrawable.cpp
 *
 *  Created on: 24-11-2012
 *      Author: Spliter
 */

#include "lbExternalDrawable.h"

lbExternalDrawable::lbExternalDrawable():
	_listener(NULL),
	_id(0)
{

}

lbExternalDrawable::~lbExternalDrawable()
{
}


void lbExternalDrawable::draw(lbRectI camera)
{
	if(_listener)
	{
		_listener->onDraw(_id,camera);
	}
}
